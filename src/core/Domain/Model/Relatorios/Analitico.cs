﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Relatorios
{
    public class Analitico : EntityBase, IAggregateRoot
    {
        [JsonProperty("counting_id")]
        public int IdApuracao { get; set; }

        [JsonProperty("cnpj")]
        public string CNPJ { get; set; }

        [JsonProperty("fantasyName")]
        public string NomeFantasia { get; set; }

        [JsonProperty("group")]
        public string Grupo { get; set; }

        [JsonProperty("segment_id")]
        public int IdSegmento { get; set; }

        [JsonProperty("segment")]
        public int Segmento { get; set; }

        [JsonProperty("gc_id")]
        public int IdGC { get; set; }

        [JsonProperty("gc_name")]
        public string NomeGC { get; set; }

        [JsonProperty("gr_id")] 
        public int IdGR { get; set; }

        [JsonProperty("gr_name")]
        public string NomeGR { get; set; }

        [JsonProperty("production")]
        public decimal Producao { get; set; }

        [JsonProperty("marketShare")]
        public string MarketShare { get; set; }

        [JsonProperty("ips")]
        public string IPS { get; set; }

        [JsonProperty("contracts_numbers")]
        public int QtdContratos { get; set; }

        [JsonProperty("converted_numbers")]
        public int QtdConvertidos { get; set; }

        [JsonProperty("prestamista_numbers")]
        public int QtdPrestamistas { get; set; }

        [JsonProperty("auto_numbers")]
        public int QtdAuto { get; set; }

        [JsonProperty("potential_numbers")]
        public int QtdPotencial { get; set; }
    }
}
