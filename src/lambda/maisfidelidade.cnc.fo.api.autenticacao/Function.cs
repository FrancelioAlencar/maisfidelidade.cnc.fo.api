using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using maisfidelidade.cnc.fo.api.core.Common.AWS;
using maisfidelidade.cnc.fo.api.core.Common.DependencyInjection;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Usuarios;
using System;
using Newtonsoft.Json;
using maisfidelidade.cnc.fo.api.core.Domain.Model;
using Microsoft.AspNetCore.Identity;
using System.Text;
using System.Security.Cryptography;
using System.Linq;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Empresas;
using maisfidelidade.cnc.fo.api.core.Log;
using System.Collections.Generic;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Gerentes;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace maisfidelidade.cnc.fo.api.autenticacao
{
    public class Function : FunctionBase
    {
        private readonly IUsuarioRepositorio usuarioRepositorio;
        private readonly ILogAcessoRepositorio logAcessoRepositorio;
        private readonly IEmpresaRepositorio empresaRepositorio;
        private readonly IGerenteRepositorio gerenteRepositorio;

        public Function()
        {
            Logger.Log($"Resolve Inje��o de Dependencia");

            var dependencyResolver = new DependencyResolver();

            usuarioRepositorio = dependencyResolver.ObterServico<IUsuarioRepositorio>();
            logAcessoRepositorio = dependencyResolver.ObterServico<ILogAcessoRepositorio>();
            empresaRepositorio = dependencyResolver.ObterServico<IEmpresaRepositorio>();
            gerenteRepositorio = dependencyResolver.ObterServico<IGerenteRepositorio>();
        }

        public APIGatewayProxyResponse FunctionHandler(APIGatewayProxyRequest request, ILambdaContext context)
        {
            try
            {
                Logger.Log($"Deserializa body {request.Body} para Usuario");
                var requestBody = JsonConvert.DeserializeObject<Usuario>(request.Body);

                if (string.IsNullOrEmpty(requestBody.Email))
                {
                    Logger.Log($"Os dados informados n�o s�o v�lidos. Email: {requestBody.Email}");
                    return CreateResponse(new
                    {
                        success = false,
                        message = $"O endere�o de e-mail deve ser preenchido."
                    });
                }

                if (string.IsNullOrEmpty(requestBody.Senha))
                {
                    Logger.Log($"Os dados informados n�o s�o v�lidos. Senha esta nula ou � vazia");
                    return CreateResponse(new
                    {
                        success = false,
                        message = $"A senha deve ser preenchida."
                    });
                }

                Logger.Log($"Busca informa��es do usu�rio");
                Usuario usuario = usuarioRepositorio.ObterPorEmail(requestBody.Email);

                //usuario.Empresas = new List<EmpresaResumido>();

                if (usuario == null)
                {
                    Logger.Log($"Informa��es do usu�rio vieram nula");
                    throw new Exception($"Usu�rio {requestBody.Email} n�o localizado");
                }

                Logger.Log($"Seta id_usuario: {usuario.Id}");

                if (!usuario.Ativo)
                {
                    Logger.Log($"Usu�rio inativo na base de dados");
                    throw new Exception($"Usu�rio {usuario.Id} inativo");
                }

                Logger.Log($"Valida senha");

                if (!usuario.ValidaSenha(requestBody.Senha))
                {
                    Logger.Log($"Senha incorreta");
                    return CreateResponse(new
                    {
                        success = false,
                        message = $"Email e/ou Senha inv�lidos."
                    });
                }

                Logger.Log($"Verifica perfil usu�rio: {usuario.Perfil.Id}");

                if (usuario.Perfil.Id.Equals((int)PerfilEnum.Lojista))
                {
                    Logger.Log($"Busca informa��es da empresa: {usuario.IdEmpresa}");
                    Empresa empresa = empresaRepositorio.ObterPorId(usuario.IdEmpresa);

                    if (empresa == null)
                    {
                        Logger.Log($"Informa��es da empresa vieram nula");
                        throw new Exception($"Empresa {usuario.IdEmpresa} n�o localizado");
                    }

                    if (!empresa.Ativo)
                    {
                        Logger.Log($"Empresa inativa");
                        throw new Exception($"Empresa {empresa.Id} inativa");
                    }
                }
                else if (usuario.Perfil.Id == (int)PerfilEnum.GC || usuario.Perfil.Id == (int)PerfilEnum.GR)
                {
                    Logger.Log($"Busca informa��es do gerente: {usuario.Email}");
                    var gerente = gerenteRepositorio.ObterPorEmail(usuario.Email);

                    if (gerente == null)
                    {
                        Logger.Log($"Gerente {gerente.Email} veio nulo");
                        throw new Exception($"Gerente {gerente.Email} n�o localizado!");
                    }

                    if (!gerente.Ativo)
                    {
                        Logger.Log($"Gerente {gerente.Email} inativo");
                        throw new Exception($"Gerente {gerente.Id} inativo.");
                    }

                    if (usuario.Perfil.Id == (int)PerfilEnum.GC)
                    {
                        Logger.Log($"Busca informa��es da empresa de acordo com o GC: {gerente.Matricula}");
                        usuario.Empresas = empresaRepositorio.ObterPorGC(gerente.Matricula);
                    }
                    else
                    {
                        Logger.Log($"Busca informa��es da empresa de acordo com o GR: {gerente.Matricula}");
                        usuario.Empresas = empresaRepositorio.ObterPorGR(gerente.Matricula);
                    }

                    if (!usuario.Empresas.Any())
                    {
                        Logger.Log($"Gerente sem empresas");
                        throw new Exception($"Gerente {gerente.Id} sem empresas.");
                    }

                    if (usuario.IdEmpresa == 0 || !usuario.Empresas.Any(x => x.Id.Equals(usuario.IdEmpresa)))
                    {
                        Logger.Log($"Seta id_empresa: {usuario.Empresas[0].Id}");
                        usuario.IdEmpresa = usuario.Empresas[0].Id;
                        usuario.Empresas[0].EmpresaPadrao = true;

                        Logger.Log($"Atualizando id_empresa do gerente para {usuario.IdEmpresa}");
                        usuarioRepositorio.AtualizaIdEmpresa(usuario.Id, usuario.IdEmpresa);
                    }
                }

                LogMessage(context, "Solicita��o de processamento bem-sucedida.");

                Logger.Log($"Gera token de login");

                var token = usuario.GerarToken();

                Logger.Log($"Salva registro de log");

                logAcessoRepositorio.SalvaRegistro(usuario.Id, usuario.Perfil.Id);

                return CreateResponse(new { success = true, Token = token, change_password = usuario.TrocarSenha });
            }
            catch (Exception _exception)
            {
                LogMessage(context, string.Format("Falha na solicita��o de processamento - {0}", _exception.Message));
                Logger.Log($"Error ao validar o token (ClaimToEntity): error ::{_exception.Message} - StackTrace:: {_exception.StackTrace} ");
                _exception.Data.Add("LambdaContext", JsonConvert.SerializeObject(context));
                return CreateResponse(new
                {
                    success = false,
                    message = $"Ocorreu um erro ao tentar processar sua solicita��o.<br/>Tente novamente em instantes. Se o erro persistir, fale com {PessoaContato()}."
                });
            }
        }
    }
}
