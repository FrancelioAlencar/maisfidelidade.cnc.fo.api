﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace maisfidelidade.cnc.fo.api.core.View.Model.FAQsModel
{
    public class QuestionarioFAQModel
    {
        [JsonProperty("title")] 
        public string Titulo { get; set; }
        [JsonProperty("content")] 
        public string Conteudo { get; set; }
    }
}