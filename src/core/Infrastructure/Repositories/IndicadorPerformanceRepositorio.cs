﻿using maisfidelidade.cnc.fo.api.core.Domain.Model.Indicadores.Performance;
using maisfidelidade.cnc.fo.api.core.Log;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Infrastructure.Repositories
{
    public class IndicadorPerformanceRepositorio : MySqlRepositorioBase<IndicadorPerformance>, IIndicadorPerformanceRepositorio
    {
        public IndicadorPerformanceRepositorio()
        {

        }

        public IList<IndicadorPerformance> ObterPorEmpresa(int idEmpresa)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("SELECT D.id_entidade AS IdEntidade, D.nome AS Indicador, D.descricao AS Descricao, D.tooltip AS Tooltip, B.valor AS Valor, E.nome AS Dado, C.valor AS ValorDado");
                query.Append(" FROM tb_apuracao AS A");
                query.Append(" LEFT JOIN tb_apuracao_indicador_performance AS B ON B.id_apuracao = A.id_apuracao");
                query.Append(" LEFT JOIN tb_apuracao_indicador_performance_dado AS C ON C.id_apuracao_indicador_performance = B.id_apuracao_indicador_performance");
                query.Append(" LEFT JOIN tb_indicador_performance AS D ON D.id_indicador_performance = B.id_indicador_performance");
                query.Append(" LEFT JOIN tb_indicador_performance_dados AS E ON E.id_indicador_performance_dado = C.id_indicador_performance_dado");
                query.Append(" LEFT JOIN tb_arquivo AS F ON F.id_arquivo = A.id_arquivo");
                query.AppendFormat(" WHERE A.id_empresa = {0} AND F.ultimo_processado = 1;", idEmpresa);

                Logger.Log($"IndicadorPerformanceRepositorio > ObterPorEmpresa Query MySql: {query.ToString()}");

                return this.BuildEntitiesFromSql(query.ToString());
            }
            catch (Exception error)
            {
                throw new Exception($"IndicadorPerformanceRepositorio > ObterPorEmpresa: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        protected override void BuildChildCallbacks()
        {

        }

        protected override void PersistDeletedItem(IndicadorPerformance item)
        {
            throw new NotImplementedException();
        }

        protected override void PersistNewItem(IndicadorPerformance item)
        {
            throw new NotImplementedException();
        }

        protected override void PersistUpdatedItem(IndicadorPerformance item)
        {
            throw new NotImplementedException();
        }
    }
}
