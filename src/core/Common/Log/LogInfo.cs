﻿//using maisfidelidade.cnc.fo.api.importacao.core.Domain.Model.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Log
{
    public class LogInfo
    {
        public LogInfo()
        {
            Detalhes = new List<LogDetalhe>();
        }

        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("id_arquivo")]
        public ObjectId? IdArquivo { get; set; }

       // [BsonElement("id_status_processamento")]
       // public StatusAndamentoProcesso StatusAndamentoProcesso { get; set; }

        [BsonElement("data_inicio")]
        public DateTime? DataInicio { get; set; }

        [BsonElement("data_fim")]
        public DateTime? DataFim { get; set; }

        [BsonElement("tempo_decorrido")]
        public TimeSpan TempoDecorrido { get; set; }

        [BsonElement("tipo")]
        public TipoLog Tipo { get; set; }
        
        [BsonElement("mensagem")]
        public string Mensagem { get; set; }

        [BsonElement("stack_trace")]
        public string StackTrace { get; set; }

        [BsonElement("detalhes")]
        public IList<LogDetalhe> Detalhes { get; set; }
    }
}
