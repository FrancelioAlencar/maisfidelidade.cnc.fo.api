﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Infrastructure.RepositoryFramework
{
    public abstract class RepositorioBase<TAggregateRoot> : IRepositorio<TAggregateRoot>
        where TAggregateRoot : IAggregateRoot
    {
        protected RepositorioBase()
        {

        }

        protected virtual void PersistNewItem(IEntity item)
        {
            this.PersistNewItem((TAggregateRoot)item);
        }

        protected virtual void PersistUpdatedItem(IEntity item)
        {
            this.PersistUpdatedItem((TAggregateRoot)item);
        }

        protected virtual void PersistDeletedItem(IEntity item)
        {
            this.PersistDeletedItem((TAggregateRoot)item);
        }

        protected abstract void PersistNewItem(TAggregateRoot item);
        protected abstract void PersistUpdatedItem(TAggregateRoot item);
        protected abstract void PersistDeletedItem(TAggregateRoot item);
    }
}
