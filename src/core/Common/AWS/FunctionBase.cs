﻿using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using maisfidelidade.cnc.fo.api.core.Common.Configuration;
using maisfidelidade.cnc.fo.api.core.Common.DependencyInjection;
using maisfidelidade.cnc.fo.api.core.Configuration;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Empresas;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Gerentes;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Usuarios;
using maisfidelidade.cnc.fo.api.core.Infrastructure.Data;
using maisfidelidade.cnc.fo.api.core.Infrastructure.Data.MySql;
using maisfidelidade.cnc.fo.api.core.Log;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Common.AWS
{
    public abstract class FunctionBase
    {
        static IUsuarioRepositorio usuarioRepositorio;
        static IEmpresaRepositorio empresaRepositorio;
        static IGerenteRepositorio gerenteRepositorio;
        static int perfil;

        protected FunctionBase()
        {
            DatabaseFactory.SetDatabases(GetDatabase);
        }

        protected static APIGatewayProxyResponse CreateResponse(object result)
        {
            var statusCode = (result != null) ?
                (int)HttpStatusCode.OK :
                (int)HttpStatusCode.InternalServerError;

            var body = (result != null) ?
                JsonConvert.SerializeObject(result) : null;

            return new APIGatewayProxyResponse
            {
                StatusCode = statusCode,
                Body = body,
                Headers = new Dictionary<string, string>
                {
                    { "Content-Type", "application/json" },
                    { "Access-Control-Allow-Origin", "*" },
                    { "Access-Control-Allow-Headers", "*" }
                }
            };
        }

        public static void LogMessage(ILambdaContext context, string message)
        {
            context.Logger.LogLine($"{context.AwsRequestId}:{context.FunctionName} - {message}");
        }

        private static bool ValidateToken(string authToken)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var validationParameters = GetValidationParameters();

                SecurityToken validatedToken;
                IPrincipal principal = tokenHandler.ValidateToken(authToken, validationParameters, out validatedToken);
                return true;
            }
            catch (Exception error)
            {
                throw new Exception($"FunctionBase > ValidadeToken: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        private static TokenValidationParameters GetValidationParameters()
        {
            return new TokenValidationParameters()
            {
                ValidateLifetime = false,
                ValidateAudience = false,
                ValidateIssuer = false,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(ConfigurationManager.AppSettings["JWT:Key"]))
            };
        }

        protected static string PessoaContato()
        {
            switch (perfil)
            {
                case (int)PerfilEnum.Lojista:
                    return "seu GR!";
                case (int)PerfilEnum.GR:
                    return "seu GC!";
                default:
                    return "o administrador do sistema!";
            }
        }

        protected static Token ValidaToken(APIGatewayProxyRequest request)
        {
            try
            {
                if (usuarioRepositorio == null && empresaRepositorio == null)
                {
                    Logger.Log($"Resolve Injeção de Dependencia");

                    var dependencyResolver = new DependencyResolver();
                    usuarioRepositorio = dependencyResolver.ObterServico<IUsuarioRepositorio>();
                    empresaRepositorio = dependencyResolver.ObterServico<IEmpresaRepositorio>();
                }

                Logger.Log($"Valida Token {request.Headers["Authorization"]}");

                var token = request.Headers["Authorization"].ToString();

                if (!ValidateToken(token))
                {
                    throw new Exception($"Token inválido {request.Headers["Authorization"]}");
                }

                return ClaimToEntity(ObterToken(token));
            }
            catch (Exception error)
            {
                throw new Exception($"FunctionBase > ValidaToken: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        public int ObterIdUsuario(string token)
        {
            try
            {
                var stream = token;
                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadToken(stream);
                var tokenS = handler.ReadToken(stream) as JwtSecurityToken;

                return int.Parse(tokenS.Claims.First(claim => claim.Type == "IdUsuario").Value);
            }
            catch (Exception error)
            {
                throw new Exception($"FunctionBase > ObterIdUsuario: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        protected static IEnumerable<Claim> ObterToken(string token)
        {
            Logger.Log($"Obtem o Claim do Token {token}");

            var stream = token;
            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadToken(stream);

            Logger.Log($"Verifica validade do token {jsonToken.ValidTo}");

            if (jsonToken.ValidTo < DateTime.Now)
                throw new Exception("Token expirou!");

            var tokenS = handler.ReadToken(stream) as JwtSecurityToken;

            return tokenS.Claims;
        }

        protected static Token ClaimToEntity(IEnumerable<Claim> claim)
        {
            try
            {
                Logger.Log($"Converte o token para objeto");

                var token = new Token
                {
                    Email = claim.First(x => x.Type == "Email").Value,
                    IdUsuario = int.Parse(claim.First(x => x.Type == "IdUsuario").Value),
                    Nome = claim.First(x => x.Type == "Nome").Value,
                    Jti = claim.First(x => x.Type == "jti").Value,
                    TrocaSenha = bool.Parse(claim.First(x => x.Type == "TrocarSenha").Value),
                    UniqueName = claim.First(x => x.Type == "unique_name").Value,
                    Perfil = ObterPerfil(claim.First(x => x.Type == "Perfil").Value)
                    //Empresas = ObterEmpresas(claim.First(x => x.Type == "Empresas").Value)
                };

                Logger.Log($"Verifica usuário na base. Consulta por id_usuario {token.IdUsuario}");

                var usuario = usuarioRepositorio.ObterPorId(token.IdUsuario);

                if (usuario == null)
                {
                    throw new Exception("Usuário não localizado!");
                }

                if (!usuario.Ativo)
                {
                    throw new Exception("Usuário inativo!");
                }

                perfil = token.Perfil.Id;

                if (token.Perfil.Id.Equals((int)PerfilEnum.Lojista))
                {
                    Logger.Log($"Verifica loja do usuário");

                    var empresa = empresaRepositorio.ObterPorId(usuario.IdEmpresa);

                    if (empresa == null)
                    {
                        throw new Exception("Empresa não localizada!");
                    }

                    if (!empresa.Ativo)
                    {
                        throw new Exception("Empresa inativa!");
                    }

                    token.IdEmpresa = empresa.Id;

                    if (!empresa.Email.Equals(token.Email))
                    {
                        Logger.Log($"Endereço de email ({token.Email}) não é o mesmo que esta cadastrado na tb_empresa {empresa.Email}");
                        throw new Exception("Endereço de email do usuário não é o mesmo do lojista");
                    }
                }
                else if (!token.Perfil.Id.Equals((int)PerfilEnum.Administrador))
                {
                    Logger.Log($"Verifica cadastro do gerente");
                    // SE NÃO É ADMIN, VERIFICA SE GERENTE ESTA ATIVO
                    if (gerenteRepositorio == null)
                    {
                        Logger.Log($"Resolve Injeção de Dependencia do Gerente");
                        var dependencyResolver = new DependencyResolver();
                        gerenteRepositorio = dependencyResolver.ObterServico<IGerenteRepositorio>();
                    }

                    Logger.Log($"Busca o gerente por email {token.Email}");
                    var gerente = gerenteRepositorio.ObterPorEmail(token.Email);

                    if (gerente == null)
                    {
                        throw new Exception("Gerente não localizado!");
                    }

                    if (!gerente.Ativo)
                    {
                        throw new Exception("Gerente inativo!");
                    }

                    if (usuario.Perfil.Id == (int)PerfilEnum.GC)
                    {
                        Logger.Log($"Busca informações da empresa de acordo com o GC: {gerente.Matricula}");
                        token.Empresas = empresaRepositorio.ObterPorGC(gerente.Matricula);
                    }
                    else
                    {
                        Logger.Log($"Busca informações da empresa de acordo com o GR: {gerente.Matricula}");
                        token.Empresas = empresaRepositorio.ObterPorGR(gerente.Matricula);
                    }

                    if (!token.Empresas.Any())
                    {
                        Logger.Log($"Gerente sem empresas");
                        token.Empresas = null;
                    }

                    if (!token.Empresas.Any(x => x.Id.Equals(usuario.IdEmpresa)))
                    {
                        Logger.Log($"Seta id_empresa: {token.Empresas[0].Id}");
                        token.IdEmpresa = token.Empresas[0].Id;
                        token.Empresas[0].EmpresaPadrao = true;

                        Logger.Log($"Atualizando id_empresa do gerente para {token.IdEmpresa}");
                        usuarioRepositorio.AtualizaIdEmpresa(token.IdUsuario, token.IdEmpresa);
                    }
                    else
                    {
                        token.Empresas.Single(x => x.Id.Equals(usuario.IdEmpresa)).EmpresaPadrao = true;
                        token.IdEmpresa = usuario.IdEmpresa;
                    }
                }

                return token;
            }
            catch (Exception _exception)
            {
                Logger.Log($"Error ao validar o token (ClaimToEntity): error ::{_exception.Message} - StackTrace:: {_exception.StackTrace} ");
                throw new Exception($"FunctionBase > ClaimToEntity: {_exception.Message}\n\nStack Trace: {_exception.StackTrace}", _exception);
            }
        }

        protected static Perfil ObterPerfil(string json)
        {
            try
            {
                var perfil = new Perfil();

                Logger.Log($"Converte para JObject {json}");
                var item = JObject.Parse(json);

                perfil.Id = int.Parse(item["Id"].ToString());
                perfil.Nome = item["Nome"].ToString();
                perfil.Permissoes = new List<Permissao>();

                foreach (var permissao in JArray.Parse(item["Permissoes"].ToString()))
                {
                    perfil.Permissoes.Add(new Permissao
                    {
                        Id = int.Parse(permissao["Id"].ToString()),
                        Nome = permissao["Nome"].ToString()
                    });
                }

                return perfil;
            }
            catch (Exception error)
            {
                throw new Exception($"FunctionBase > ObterPerfil: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        protected static IList<EmpresaResumido> ObterEmpresas(string json)
        {
            try
            {
                var empresas = new List<EmpresaResumido>();

                Logger.Log($"Converte para JArray {json}");
                foreach (var empresa in JArray.Parse(json))
                {
                    empresas.Add(new EmpresaResumido
                    {
                        Id = int.Parse(empresa["Id"].ToString()),
                        NomeFantasia = empresa["NomeFantasia"].ToString(),
                        CNPJ = empresa["CNPJ"].ToString(),
                        Ativo = bool.Parse(empresa["Ativo"].ToString()),
                        DataInclusao = Convert.ToDateTime(empresa["DataInclusao"].ToString()),
                        DataAlteracao = Convert.ToDateTime(empresa["DataAlteracao"].ToString()),
                        EmpresaPadrao = bool.Parse(empresa["EmpresaPadrao"].ToString())
                    });
                }

                return empresas;
            }
            catch (Exception error)
            {
                throw new Exception($"FunctionBase > ObterEmpresas: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        #region Métodos Privados
        private static Database GetDatabase(string name) //, DatabaseType databaseType)
        {
            try
            {
                Database database = null;
                switch (name)
                {
                    case "read":
                        database = new MySqlDatabase(new DatabaseSettings("connection_maisfidelidade_mysql_read").ConnectionString);
                        break;
                    case "write":
                        database = new MySqlDatabase(new DatabaseSettings("connection_maisfidelidade_mysql_write").ConnectionString);
                        break;
                    /*
                    case "webmotors":
                        database = new SqlServerDatabase(new DatabaseSettings("connection_maisfidelidade_sqlserver").ConnectionString);
                        break;
                    case "stage":
                        database = new MongoDBDatabase(new DatabaseSettings("connection_maisfidelidade_mongodb").ConnectionString);
                        break;*/
                    default:
                        throw new Exception("Unexpected Case");
                }
                if (database == null)
                {
                    throw new ArgumentNullException("");
                }
                return database;
            }
            catch (Exception exception)
            {
                throw new Exception($"FunctionBase > GetDatabase (Erro ao instanciar a conexão): {exception.Message}\n\nStack Trace: {exception.StackTrace}", exception);
            }
        }
        #endregion
    }
}
