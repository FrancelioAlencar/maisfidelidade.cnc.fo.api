﻿using maisfidelidade.cnc.fo.api.core.Infrastructure;
using maisfidelidade.cnc.fo.api.core.Infrastructure.RepositoryFramework;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain
{
    public interface IConfiguracaoRepositorio : IRepositorio<Configuracao>
    {
        IList<Configuracao> ObterHubConfig();
    }
}
