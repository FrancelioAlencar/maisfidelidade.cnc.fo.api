﻿using maisfidelidade.cnc.fo.api.core.Domain.Model.Matrizes;
using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace maisfidelidade.cnc.fo.api.core.View.Model.SimuladoresModel
{
    public class SimuladorModel
    {
        public SimuladorModel()
        {
            Dados = new DadosSimuladorModel();
            Matriz = new List<Matriz>();
        }

        [JsonProperty("data")]
        public DadosSimuladorModel Dados { get; set; }

        [JsonProperty("matrix")]
        public IList<Matriz> Matriz { get; set; }
    }
}