﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace maisfidelidade.cnc.fo.api.core.Configuration
{
	public static class ConfigurationManager
	{
		private static ChavesAPP appSettings;
		public static ChavesAPP AppSettings
		{
			get
			{
				if (appSettings == null)
					appSettings = new ChavesAPP();

				return appSettings;
			}
		}

		public class ChavesAPP
		{
			private IConfigurationRoot _configuration;

			public IConfigurationRoot Configuration
			{
				get
				{
                    if (_configuration == null)
                    {
						return new ConfigurationBuilder()
									.SetBasePath(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location))
									.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
									.AddJsonFile($"appsettings.dev.json", optional: true)
									.AddEnvironmentVariables()
									.Build();

						//return new ConfigurationBuilder()
						//		.SetBasePath(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location))
						//		.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
						//		.AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json", optional: true)
						//		.AddEnvironmentVariables()
						//		.Build();
					}
                    return _configuration;
				}
			}

			public string this[string key]
			{
				get
				{
					try
					{
						return Configuration[key];
					}
					catch { }
					return string.Empty;
				}
			}

			public string Decrypt(string key, bool encoded = false)
			{
                if (!string.IsNullOrWhiteSpace(AppSettings["encrypted"]) && !AppSettings["encrypted"].To<bool>())
					return AppSettings[key];
				return KmsHelper.DecodeEnvVar(key, encoded).Result;
			}

			public void AddRange(IDictionary<string, string> items)
			{
				var builder = new ConfigurationBuilder()
					.SetBasePath(Directory.GetCurrentDirectory())
					.AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json", optional: true)
					.AddJsonFile("appsettings.json", optional: true)
					.AddEnvironmentVariables()
					.AddInMemoryCollection(items);
				_configuration = builder.Build();
			}
		}

		public static string GetEnvironment
		{
			get
			{
				try
				{
					var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
					if (!string.IsNullOrWhiteSpace(env))
						return env;
				}
				catch { }
				return string.Empty;
			}
		}
	}
}
