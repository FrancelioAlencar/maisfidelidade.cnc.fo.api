﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Empresas
{
    public class Empresa : EntityBase, IAggregateRoot
    {
        public string CNPJ { get; set; }
        public string NomeFantasia { get; set; }
        public string Email { get; set; }
        public int MatriculaGR { get; set; }
        public int MatriculaGC { get; set; }
	}
}
