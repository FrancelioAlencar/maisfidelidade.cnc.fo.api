using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using maisfidelidade.cnc.fo.api.core.Common.AWS;
using maisfidelidade.cnc.fo.api.core.Common.DependencyInjection;
using maisfidelidade.cnc.fo.api.core.Domain.Model;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Empresas;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Relatorios;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Usuarios;
using maisfidelidade.cnc.fo.api.core.Log;
using Newtonsoft.Json;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace maisfidelidade.cnc.fo.api.rel.filtros
{
    public class Function : FunctionBase
    {
        private readonly IAnaliticoRepositorio analiticoRepositorio;

        public Function()
        {
            Logger.Log($"Resolve Injeção de Dependencia");

            var dependencyResolver = new DependencyResolver();

            analiticoRepositorio = dependencyResolver.ObterServico<IAnaliticoRepositorio>();
        }

        public APIGatewayProxyResponse FunctionHandler(APIGatewayProxyRequest request, ILambdaContext context)
        {
            try
            {
                Logger.Log($"Valida token");
                var token = ValidaToken(request);                

                Logger.Log($"Obtem filtros");
                var filtros = analiticoRepositorio.ObterFiltros(token.Perfil.Id);

                LogMessage(context, "Solicitação de processamento bem-sucedida.");

                return CreateResponse(new
                {
                    success = true,
                    data = filtros
                });
            }
            catch (Exception _exception)
            {
                LogMessage(context, string.Format("Falha na solicitação de processamento - {0}", _exception.Message));
                Logger.Log($"Error ao buscar filtros do relatório analítico: error ::{_exception.Message} - StackTrace:: {_exception.StackTrace} ");
                _exception.Data.Add("LambdaContext", JsonConvert.SerializeObject(context));
                return CreateResponse(new
                {
                    success = false,
                    message = $"Ocorreu um erro ao tentar processar sua solicitação.<br/>Tente novamente em instantes. Se o erro persistir, fale com {PessoaContato()}."
                });
            }
        }
    }
}