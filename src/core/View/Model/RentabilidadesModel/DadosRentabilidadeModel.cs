﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace maisfidelidade.cnc.fo.api.core.View.Model.RentabilidadesModel
{
    public class DadosRentabilidadeModel
    {
        public DadosRentabilidadeModel()
        {
            Extrato = new ExtratoRentabilidadeModel();
            Cartoes = new List<CartaoRentabilidadeModel>();
        }

        [JsonProperty("extract")]
        public ExtratoRentabilidadeModel Extrato { get; set; }
     
        [JsonProperty("cards")]
        public List<CartaoRentabilidadeModel> Cartoes { get; set; }
    }
}