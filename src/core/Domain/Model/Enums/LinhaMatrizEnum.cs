﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Enums
{
	public enum LinhaMatrizEnum : int
	{
		Vertical = 1,
		Horizontal = 2
	}
}
