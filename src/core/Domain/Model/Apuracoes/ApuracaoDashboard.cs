﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Apuracoes
{
    public class ApuracaoDashboard
    {
        public int Id { get; set; }
        public decimal PlusAtual { get; set; }
        public decimal PlusAnterior { get; set; }
        public decimal PlusFuturo { get; set; }
        public decimal AutoAtual { get; set; }
        public decimal AutoAnterior { get; set; }
        public decimal AutoFuturo { get; set; }
        public decimal PrestamistaAtual{ get; set; }
        public decimal PrestamistaAnterior { get; set; }
        public decimal PrestamistaFuturo { get; set; }
        public DateTime DataAtual { get; set; }
        public DateTime DataAnterior { get; set; }
        public DateTime DataFuturo { get; set; }
    }
}
