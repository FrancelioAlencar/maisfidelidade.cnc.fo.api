﻿using maisfidelidade.cnc.fo.api.core.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Apuracoes;
using Amazon.SQS.Model;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Enums;

namespace maisfidelidade.cnc.fo.api.core.Infrastructure.Repositories
{
	public class ApuracaoRepositorio : MySqlRepositorioBase<Apuracao>, IApuracaoRepositorio
	{
		public ApuracaoRepositorio()
		{

		}

		public Apuracao BuscarApuracaoSemanal(int idUsuario)
		{
			var builder = new StringBuilder();

			builder.Append("SELECT ap.financiado_santander AS FinanciadoSantander, ");
			builder.Append("ap.financiado_loja AS FinanciadoLoja, ");
			builder.Append("ap.qtd_contratos AS QtdContrato, ");
			builder.Append("ap.qtd_prestamista AS QtdPrestamista, ");
			builder.Append("ap.qtd_auto AS QtdAuto, ");
			builder.Append("ap.qtd_ambos AS QtdAmbos, ");
			builder.Append("e.id_segmento AS IdSegmento ");
			builder.Append("FROM tb_apuracao ap ");
			builder.Append("INNER JOIN tb_categoria c ");
			builder.Append("ON ap.id_categoria = c.id_categoria ");
			builder.Append("INNER JOIN tb_arquivo arq ");
			builder.Append("ON ap.id_arquivo = arq.id_arquivo ");
			builder.Append("INNER JOIN tb_empresa e ");
			builder.Append("ON e.id_empresa = ap.id_empresa ");
			builder.Append("INNER JOIN tb_usuario u ");
			builder.Append("ON u.id_empresa = ap.id_empresa ");
			builder.Append("WHERE u.id_usuario = " + idUsuario + " ");
			builder.Append("AND arq.id_tipo_arquivo =  " + (int)TipoArquivoEnum.Semanal + " ");
			builder.Append("AND arq.ultimo_processado = 1 ");

			return this.ExecuteQueryFirst<Apuracao>(builder.ToString());
	}

		protected override void BuildChildCallbacks()
		{

		}

		protected override void PersistDeletedItem(Apuracao item)
		{
			throw new NotImplementedException();
		}

		protected override void PersistNewItem(Apuracao item)
		{
			throw new NotImplementedException();
		}

		protected override void PersistUpdatedItem(Apuracao item)
		{
			throw new NotImplementedException();
		}
	}
}