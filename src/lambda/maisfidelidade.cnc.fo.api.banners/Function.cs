using System;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using maisfidelidade.cnc.fo.api.core.Common.AWS;
using maisfidelidade.cnc.fo.api.core.Common.DependencyInjection;
using maisfidelidade.cnc.fo.api.core.Domain.Model;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Banners;
using maisfidelidade.cnc.fo.api.core.Log;
using Newtonsoft.Json;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace maisfidelidade.cnc.fo.api.banners
{
    public class Function : FunctionBase
    {
        private readonly IBannerRepositorio bannerRepositorio;

        public Function()
        {
            Logger.Log($"Resolve Inje��o de Dependencia");

            var dependencyResolver = new DependencyResolver();

            bannerRepositorio = dependencyResolver.ObterServico<IBannerRepositorio>();
        }

        public APIGatewayProxyResponse FunctionHandler(APIGatewayProxyRequest request, ILambdaContext context)
        {
            try
            {
                Logger.Log("Valida token");
                var token = ValidaToken(request);

                Logger.Log($"Obtem banners para o usu�rio {token.IdUsuario}");
                var lista = bannerRepositorio.Obter(token.IdEmpresa);

                LogMessage(context, "Solicita��o de processamento bem-sucedida.");

                return CreateResponse(new
                {
                    success = true,
                    data = lista
                });
            }
            catch (Exception _exception)
            {
                LogMessage(context, string.Format("Falha na solicita��o de processamento - {0}", _exception.Message));
                Logger.Log($"Error ao carregar banners: error ::{_exception.Message} - StackTrace:: {_exception.StackTrace} ");
                _exception.Data.Add("LambdaContext", JsonConvert.SerializeObject(context));
                return CreateResponse(new
                {
                    success = false,
                    message = $"Ocorreu um erro ao tentar processar sua solicita��o.<br/>Tente novamente em instantes. Se o erro persistir, fale com {PessoaContato()}."
                });
            }
        }
    }
}