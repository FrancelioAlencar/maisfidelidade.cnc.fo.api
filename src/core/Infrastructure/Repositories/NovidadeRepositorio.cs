﻿using maisfidelidade.cnc.fo.api.core.Domain.Model.Mensagens;
using maisfidelidade.cnc.fo.api.core.Log;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Infrastructure.Repositories
{
    public class NovidadeRepositorio : MySqlRepositorioBase<Novidade>, INovidadeRepositorio
    {
        public NovidadeRepositorio()
        {

        }

        public void Lida(int idNovidadeUsuario, bool lida)
        {
            try
            {
                var query = new StringBuilder();

                if (lida)
                    query.Append("UPDATE tb_novidade_usuario SET dt_leitura = NOW() ");
                else
                    query.Append("UPDATE tb_novidade_usuario SET dt_leitura = NULL ");

                query.AppendFormat(" WHERE id_novidade_usuario = {0}", idNovidadeUsuario);

                Logger.Log($"NovidadeRepositorio > Lida Query MySql: {query.ToString()}");

                this.ExecuteQuery(query.ToString());
            }
            catch (Exception error)
            {
                throw new Exception($"NovidadeRepositorio > Lida: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        public IList<Novidade> Obter(int idUsuario)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("SELECT id_novidade_usuario AS Id, titulo AS Titulo, descricao AS Descricao, dt_inicio AS DataInicio, dt_fim AS DataFim, dt_inclusao AS DataInclusao, dt_alteracao AS DataAlteracao, id_usuario_envio AS IdUsuarioEnvio, dt_leitura AS DataLeitura, ordem_exibicao AS OrdemExibicao, CASE WHEN ordem_exibicao IS NOT NULL THEN TRUE ELSE FALSE END AS Pin, CASE WHEN dt_leitura IS NOT NULL THEN TRUE ELSE FALSE END AS Lida ");
                query.Append(" FROM tb_novidades AS A ");
                query.Append(" LEFT JOIN tb_novidade_usuario AS B ON B.id_novidade = A.id_novidade ");
                query.AppendFormat(" WHERE (dt_inicio IS NULL AND dt_fim IS NULL) OR (NOW() BETWEEN dt_inicio AND dt_fim) AND id_usuario = {0} ", idUsuario);
                query.Append(" ORDER BY dt_inclusao DESC");

                Logger.Log($"NovidadeRepositorio > Obter Query MySql: {query.ToString()}");

                return this.BuildEntitiesFromSql(query.ToString());
            }
            catch (Exception error)
            {
                throw new Exception($"NovidadeRepositorio > Obter: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        public void Pin(int idNovidadeUsuario, bool check, int idUsuario)
        {
            try
            {
                var query = new StringBuilder();

                if (!check)
                    query.Append("UPDATE tb_novidade_usuario SET ordem_exibicao = NULL ");
                else
                {
                    var ordem = this.ExecuteQueryFirst<int>(string.Format("SELECT CASE WHEN MAX(ordem_exibicao) IS NULL THEN 1 ELSE MAX(ordem_exibicao) +1 END FROM tb_novidade_usuario WHERE id_usuario = {0}", idUsuario));

                    query.AppendFormat("UPDATE tb_novidade_usuario SET ordem_exibicao = {0} ", ordem);
                }

                query.AppendFormat(" WHERE id_novidade_usuario = {0}", idNovidadeUsuario);
                
                Logger.Log($"NovidadeRepositorio > Pin Query MySql: {query.ToString()}");

                this.ExecuteQuery(query.ToString());
            }
            catch (Exception error)
            {
                throw new Exception($"NovidadeRepositorio > Pin: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        protected override void BuildChildCallbacks()
        {

        }

        protected override void PersistDeletedItem(Novidade item)
        {
            throw new NotImplementedException();
        }

        protected override void PersistNewItem(Novidade item)
        {
            throw new NotImplementedException();
        }

        protected override void PersistUpdatedItem(Novidade item)
        {
            throw new NotImplementedException();
        }
    }
}
