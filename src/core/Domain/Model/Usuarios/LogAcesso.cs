﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Usuarios
{
    public class LogAcesso : EntityBase, IAggregateRoot
    {
        public int IdUsuario { get; set; }
        public int IdPerfil { get; set; }
    }
}
