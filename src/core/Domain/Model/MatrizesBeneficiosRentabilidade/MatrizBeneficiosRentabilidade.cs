﻿using maisfidelidade.cnc.fo.api.core.Common.AWS;
using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using Newtonsoft.Json;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.MatrizesBeneficiosRentabilidade

{
    /// <summary>
    ///
    /// </summary>
    public class MatrizBeneficioRentabilidade : EntityBase
    {
        [JsonProperty("matrixId")]
        public int IdMatriz { get; set; }

        [JsonProperty("matrix")]
        public string Matriz { get; set; }

        [JsonProperty("benefit")]
        public string BeneficioRentabilidade { get; set; }

        [JsonProperty("value")]
        public string Valor { get; set; }
    }
}