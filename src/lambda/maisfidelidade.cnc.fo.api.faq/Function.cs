using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using maisfidelidade.cnc.fo.api.core.Common.AWS;
using maisfidelidade.cnc.fo.api.core.Common.DependencyInjection;
using maisfidelidade.cnc.fo.api.core.Domain;
using maisfidelidade.cnc.fo.api.core.Domain.Model.FAQs;
using maisfidelidade.cnc.fo.api.core.View.Model.FAQsModel;
using System;
using System.Collections.Generic;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace maisfidelidade.cnc.fo.api.faq
{
	public class Function : FunctionBase
    {
		private readonly IFAQRepositorio faqRepositorio;

		public Function()
		{
			var dependencyResolver = new DependencyResolver();

			faqRepositorio = dependencyResolver.ObterServico<IFAQRepositorio>();
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="context"></param>
		/// <returns></returns>
		public APIGatewayProxyResponse FunctionHandler(ILambdaContext context)
		{	
			APIGatewayProxyResponse response;

			try
			{
				IList<FAQ> faq = faqRepositorio.BuscarFAQ();
				Template template = faqRepositorio.BuscarTemplate();
				FAQModel retorno = new FAQModel();

				if (faq != null && template != null)
				{
				     for (int i = 0; i < faq.Count; i++)
				     {
						QuestionarioFAQModel questionario = new QuestionarioFAQModel();

						questionario.Titulo = faq[i].Pergunta;
						questionario.Conteudo = template.Conteudo;
						questionario.Conteudo = questionario.Conteudo.Replace("{resposta}", faq[i].Resposta);
						retorno.Questionario.Add(questionario);
					}
				}

				response = CreateResponse(retorno);

				LogMessage(context, "Solicitação de processamento bem-sucedida.");
			}
			catch (Exception exception)
			{
				LogMessage(context, string.Format("Falha na solicitação de processamento - {0}", exception.Message));

				response = CreateResponse(null);
			}

			return response;
		}
	}
}
