﻿using maisfidelidade.cnc.fo.api.core.Domain.Model.Usuarios;
using maisfidelidade.cnc.fo.api.core.Log;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Infrastructure.Repositories
{
    public class LogSenhaRepositorio : MySqlRepositorioBase<LogSenha>, ILogSenhaRepositorio
    {
        public LogSenhaRepositorio()
        {

        }

        public void SalvaRegistro(LogSenha log)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("INSERT INTO tb_log_senha (id_usuario, hash_senha, fl_recuperar_senha) VALUES (");
                query.AppendFormat("{0}, '{1}', {2});", log.IdUsuario, log.HashSenha, log.EmailRecuperarSenha);

                Logger.Log($"LogSenhaRepositorio > SalvaRegistro Query MySql: {query.ToString()}");

                this.ExecuteQuery(query.ToString());
            }
            catch (Exception error)
            {
                throw new Exception($"LogSenhaRepositorio > SalvaRegistro: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        protected override void BuildChildCallbacks()
        {

        }

        protected override void PersistDeletedItem(LogSenha item)
        {
            throw new NotImplementedException();
        }

        protected override void PersistNewItem(LogSenha item)
        {
            throw new NotImplementedException();
        }

        protected override void PersistUpdatedItem(LogSenha item)
        {
            throw new NotImplementedException();
        }
    }
}
