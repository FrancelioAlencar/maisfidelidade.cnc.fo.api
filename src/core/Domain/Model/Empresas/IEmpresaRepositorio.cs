﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.RepositoryFramework;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Empresas
{
    public interface IEmpresaRepositorio : IRepositorio<Empresa>
    {
        Empresa ObterPorCNPJ(string cnpj);
        Empresa ObterPorId(int idEmpresa);
        IList<EmpresaResumido> ObterPorGC(int matricula);
        IList<EmpresaResumido> ObterPorGR(int matricula);
    }
}
