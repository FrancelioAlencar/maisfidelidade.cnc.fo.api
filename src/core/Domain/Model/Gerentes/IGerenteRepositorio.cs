﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.RepositoryFramework;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Gerentes
{
    public interface IGerenteRepositorio : IRepositorio<Gerente>
    {
        Gerente ObterPorEmail(string email);
    }
}