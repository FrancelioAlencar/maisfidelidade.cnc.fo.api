﻿using maisfidelidade.cnc.fo.api.core.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using maisfidelidade.cnc.fo.api.core.Log;
using Amazon.SQS.Model;
using maisfidelidade.cnc.fo.api.core.Domain.Model.ApuracoesIndicadorClassificacao;

namespace maisfidelidade.cnc.fo.api.core.Infrastructure.Repositories
{
	public class ApuracaoIndicadorClassificacaoRepositorio : MySqlRepositorioBase<ApuracaoIndicadorClassificacao>, IApuracaoIndicadorClassificacaoRepositorio
	{
		public ApuracaoIndicadorClassificacaoRepositorio()
		{

		}
		public IList<ApuracaoIndicadorClassificacao> BuscarApuracaoIndicadorClassificacao(int idUsuario)
		{
			var builder = new StringBuilder();

			builder.Append("SELECT arq. id_tipo_arquivo AS TipoArquivo, ");
			builder.Append("ic.id_indicador_classificacao AS IdIndicadorClassificacao, ");
			builder.Append("fx.id_faixa AS IdFaixa ");
			builder.Append("FROM tb_apuracao_indicador_classificacao aic ");
			builder.Append("INNER JOIN tb_indicador_classificacao ic ");
			builder.Append("ON aic.id_indicador_classificacao = ic.id_indicador_classificacao ");
			builder.Append("INNER JOIN tb_apuracao ap ");
			builder.Append("ON aic.id_apuracao = ap.id_apuracao ");
			builder.Append("INNER JOIN tb_arquivo arq ");
			builder.Append("ON ap.id_arquivo = arq.id_arquivo ");
			builder.Append("INNER JOIN tb_empresa e ");
			builder.Append("ON e.id_empresa = ap.id_empresa ");
			builder.Append("INNER JOIN tb_usuario u ");
			builder.Append("ON u.id_empresa = ap.id_empresa ");
			builder.Append("INNER JOIN tb_faixa fx ");
			builder.Append("ON fx.id_indicador_classificacao = ic.id_indicador_classificacao ");
			builder.Append("AND fx.id_segmento = e.id_segmento ");
			builder.Append("WHERE u.id_usuario = " + idUsuario + " ");
			builder.Append("AND arq.ultimo_processado = 1 ");
			builder.Append("AND(aic.valor >= fx.percentual_de AND aic.valor <= fx.percentual_ate) ");
			builder.Append("ORDER BY id_tipo_arquivo ASC ");

			Logger.Log($"Busca Apuração Indicador Classificação: {builder.ToString()}");

			return this.BuildEntitiesFromSql(builder.ToString());
		}
		protected override void BuildChildCallbacks()
		{

		}

		protected override void PersistDeletedItem(ApuracaoIndicadorClassificacao item)
		{
			throw new NotImplementedException();
		}

		protected override void PersistNewItem(ApuracaoIndicadorClassificacao item)
		{
			throw new NotImplementedException();
		}

		protected override void PersistUpdatedItem(ApuracaoIndicadorClassificacao item)
		{
			throw new NotImplementedException();
		}
	}
}
