﻿using Amazon;
using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using Amazon.SQS;
using Amazon.SQS.Model;
using maisfidelidade.cnc.fo.api.core.Log;
using maisfidelidade.cnc.fo.api.core.Configuration;

namespace maisfidelidade.cnc.fo.api.core.Helpers
{
	public static class AWSHelper
	{
		public static void EnviarSNS(string idArquivo, string topic)
		{
			Logger.Log("topic:   " + ConfigurationManager.AppSettings[$"{topic}"] + " Mensagem:" + idArquivo);

			if (string.IsNullOrWhiteSpace(idArquivo)) return;
			using (var snsClient = new AmazonSimpleNotificationServiceClient(RegionEndpoint.SAEast1))
			{
				var request = new PublishRequest
				{
					Message = idArquivo,
					TopicArn = ConfigurationManager.AppSettings[$"{topic}"],
				};
				var result = snsClient.PublishAsync(request).Result;
			}
		}

		public static void EnviarSQS(string messageBody, string fila)
		{
			var teste = "Enviando mensagem para à fila:   " + ConfigurationManager.AppSettings[$"{fila}"] + " Mensagem:" + messageBody;
			Logger.Log(teste);

			if (string.IsNullOrWhiteSpace(messageBody)) return;

			using (var sqsClient = new AmazonSQSClient(RegionEndpoint.SAEast1))
			{
				var request = new SendMessageRequest
				{
					MessageBody = messageBody,
					QueueUrl = ConfigurationManager.AppSettings[$"{fila}"],
				};
				var result = sqsClient.SendMessageAsync(request).Result;
			}
		}
	}
}
