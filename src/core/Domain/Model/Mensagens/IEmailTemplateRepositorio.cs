﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.RepositoryFramework;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Mensagens
{
    public interface IEmailTemplateRepositorio : IRepositorio<EmailTemplate>
    {
        EmailTemplate ObterPorId(int id);
    }
}
