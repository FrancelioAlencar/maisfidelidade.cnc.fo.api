﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Enums
{
	public enum PosicaoMatrizEnum : int
	{
		Nenhuma = 0,
		Atual = 1,
		Anterior = 2
	}
}
