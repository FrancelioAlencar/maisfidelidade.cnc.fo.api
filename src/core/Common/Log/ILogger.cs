﻿//using maisfidelidade.cnc.fo.api.importacao.core.Domain.Model.Enums;
using MongoDB.Bson;
using System;
using System.Collections.Generic;

namespace maisfidelidade.cnc.fo.api.core.Log
{
    public interface ILogger
    {
       // void Log(ObjectId? idArquivo, StatusAndamentoProcesso statusAndamentoProcesso, LogWatcher logWatcher, TipoLog tipoLog, string mensagem);
     //   void Log(ObjectId? idArquivo, StatusAndamentoProcesso statusAndamentoProcesso, LogWatcher logWatcher, TipoLog tipoLog, string mensagem, IList<LogDetalhe> detalhes);
        void Log(LogInfo logInfo);
        void LogException(Exception exception, LogWatcher logWatcher);
    }
}
