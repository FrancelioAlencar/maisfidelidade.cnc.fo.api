﻿using maisfidelidade.cnc.fo.api.core.Domain.Model.Usuarios;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Usuarios
{
    public class Perfil
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public IList<Permissao> Permissoes { get; set; }
    }
}
