﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Enums
{
	public enum TipoArquivoEnum : int
	{
		Semanal = 1,
		Mensal = 2
	}
}
