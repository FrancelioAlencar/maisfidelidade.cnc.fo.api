﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using Newtonsoft.Json;

namespace maisfidelidade.cnc.fo.api.core.Domain
{
    /// <summary>
    ///
    /// </summary>
    public class Template : EntityBase, IAggregateRoot
    {
        [JsonProperty("content")]
        public string Conteudo { get; set; }
    }
}