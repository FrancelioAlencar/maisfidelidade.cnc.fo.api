﻿using Amazon.KeyManagementService;
using Amazon.KeyManagementService.Model;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace maisfidelidade.cnc.fo.api.core.Configuration
{
	public class KmsHelper
	{
		public static async Task<string> DecodeEnvVar(string envVarName, bool encoded = false)
		{
			var encryptedBase64Text = encoded ? Encoding.ASCII.GetString(Convert.FromBase64String(ConfigurationManager.AppSettings[envVarName].ToString())) : ConfigurationManager.AppSettings[envVarName];
			var encryptedBytes = Convert.FromBase64String(encryptedBase64Text);

			using (var client = new AmazonKeyManagementServiceClient(Amazon.RegionEndpoint.USEast1))
			{
				var decryptRequest = new DecryptRequest
				{
					CiphertextBlob = new MemoryStream(encryptedBytes),
				};

				var response = await client.DecryptAsync(decryptRequest);
				using (var plaintextStream = response.Plaintext)
				{
					var plaintextBytes = plaintextStream.ToArray();
					var plaintext = Encoding.UTF8.GetString(plaintextBytes);
					return plaintext;
				}
			}
		}
	}
}
