﻿using maisfidelidade.cnc.fo.api.core.Domain.Model.Empresas;
using maisfidelidade.cnc.fo.api.core.Log;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Infrastructure.Repositories
{
    public class EmpresaRepositorio : MySqlRepositorioBase<Empresa>, IEmpresaRepositorio
    {
        public EmpresaRepositorio()
        {

        }

        public Empresa ObterPorCNPJ(string cnpj)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("SELECT id_empresa AS Id, cnpj AS CNPJ, nome_fantasia AS NomeFantasia, matricula_gr AS MatriculaGR, matricula_gc AS MatriculaGC, ativo AS Ativo, dt_inclusao AS DataInclusao, dt_alteracao AS DataAlteracao, email AS Email ");
                query.Append(" FROM tb_empresa ");
                query.AppendFormat(" WHERE cnpj = '{0}';", cnpj);

                Logger.Log($"EmpresaRepositorio > ObterPorCNPJ Query MySql: {query.ToString()}");

                return this.BuildEntityFromSql(query.ToString());
            }
            catch (Exception error)
            {
                throw new Exception($"EmpresaRepositorio > ObterPorCNPJ: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        public Empresa ObterPorId(int idEmpresa)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("SELECT id_empresa AS Id, cnpj AS CNPJ, nome_fantasia AS NomeFantasia, matricula_gr AS MatriculaGR, matricula_gc AS MatriculaGC, ativo AS Ativo, dt_inclusao AS DataInclusao, dt_alteracao AS DataAlteracao, email AS Email ");
                query.Append(" FROM tb_empresa ");
                query.AppendFormat(" WHERE id_empresa = {0};", idEmpresa);

                Logger.Log($"EmpresaRepositorio > ObterPorId Query MySql: {query.ToString()}");

                return this.BuildEntityFromSql(query.ToString());
            }
            catch (Exception error)
            {
                throw new Exception($"EmpresaRepositorio > ObterPorId: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        public IList<EmpresaResumido> ObterPorGC(int matricula)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("SELECT id_empresa AS Id, cnpj AS CNPJ, nome_fantasia AS NomeFantasia, ativo AS Ativo, dt_inclusao AS DataInclusao, dt_alteracao AS DataAlteracao, 0 AS EmpresaPadrao ");
                query.Append(" FROM tb_empresa ");
                query.AppendFormat(" WHERE matricula_gc = {0} AND ativo = 1", matricula);

                Logger.Log($"EmpresaRepositorio > ObterPorGC Query MySql: {query.ToString()}");

                return this.ExecuteQuery<EmpresaResumido>(query.ToString());
            }
            catch (Exception error)
            {
                throw new Exception($"EmpresaRepositorio > ObterPorGC: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        public IList<EmpresaResumido> ObterPorGR(int matricula)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("SELECT id_empresa AS Id, cnpj AS CNPJ, nome_fantasia AS NomeFantasia, ativo AS Ativo, dt_inclusao AS DataInclusao, dt_alteracao AS DataAlteracao, 0 AS EmpresaPadrao ");
                query.Append(" FROM tb_empresa ");
                query.AppendFormat(" WHERE matricula_gr = {0} AND ativo = 1", matricula);

                Logger.Log($"EmpresaRepositorio > ObterPorGR Query MySql: {query.ToString()}");

                return this.ExecuteQuery<EmpresaResumido>(query.ToString());
            }
            catch (Exception error)
            {
                throw new Exception($"EmpresaRepositorio > ObterPorGR: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        protected override void BuildChildCallbacks()
        {

        }

        protected override void PersistDeletedItem(Empresa item)
        {
            throw new NotImplementedException();
        }

        protected override void PersistNewItem(Empresa item)
        {
            throw new NotImplementedException();
        }

        protected override void PersistUpdatedItem(Empresa item)
        {
            throw new NotImplementedException();
        }
    }
}
