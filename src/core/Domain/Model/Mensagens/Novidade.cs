﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Mensagens
{
    public class Novidade : EntityBase, IAggregateRoot
    {
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        //public int IdUsuarioEnvio { get; set; }
        //public string NomeUsuarioEnvio { get; set; }
        public DateTime DataLeitura { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
        public bool Lida { get; set; }
        public int OrdemExibicao { get; set; }
        public bool Pin { get; set; }
    }
}
