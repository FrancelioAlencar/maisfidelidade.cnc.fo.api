﻿namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Classificacao
{
    public class Classificacao
    {
        public int IdApuracao { get; set; }
        public int IdCategoria { get; set; }
        public string Categoria { get; set; }
        public string NomeFantasia { get; set; }
        public string Grupo { get; set; }
        public int IdSegmento { get; set; }
        public string Segmento { get; set; }
        public int IdEntidade { get; set; }
        public string Indicador { get; set; }
        public string Valor { get; set; }
        public string MarketShare { get; set; }
        public string IPS { get; set; }
    }
}
