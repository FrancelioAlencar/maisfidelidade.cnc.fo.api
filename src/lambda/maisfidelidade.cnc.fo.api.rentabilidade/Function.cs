using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using maisfidelidade.cnc.fo.api.core.Common.AWS;
using maisfidelidade.cnc.fo.api.core.Common.DependencyInjection;
using maisfidelidade.cnc.fo.api.core.Domain;
using maisfidelidade.cnc.fo.api.core.Domain.Model.ApuracoesIndicadorRentabilidade;
using maisfidelidade.cnc.fo.api.core.Domain.Model.ApuracoesBeneficioRentabilidade;
using maisfidelidade.cnc.fo.api.core.View.Model.RentabilidadesModel;
using maisfidelidade.cnc.fo.api.core.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using ChoETL;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace maisfidelidade.cnc.fo.api.rentabilidade
{
    public class Function : FunctionBase
    {
        private readonly IApuracaoIndicadorRentabilidadeRepositorio apuracaIndicadorRentabilidadeRepositorio; 
        private readonly IApuracaoBeneficioRentabilidadeRepositorio apuracaoBeneficioRentabilidadeRepositorio;

        public Function()
        {
            var dependencyResolver = new DependencyResolver();

            Logger.Log($"Resolve Inje��o de Dependencia");

            apuracaIndicadorRentabilidadeRepositorio = dependencyResolver.ObterServico<IApuracaoIndicadorRentabilidadeRepositorio>();
            apuracaoBeneficioRentabilidadeRepositorio = dependencyResolver.ObterServico<IApuracaoBeneficioRentabilidadeRepositorio>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public APIGatewayProxyResponse FunctionHandler(APIGatewayProxyRequest request, ILambdaContext context)
        {
            APIGatewayProxyResponse response;

            try
            {
                Logger.Log($"Valida token");
                var token = ValidaToken(request);

                RentabilidadeModel retorno = new RentabilidadeModel();

                Logger.Log($"Busca apura��o de indicadores de rentabilidade");
                IList<ApuracaoIndicadorRentabilidade> apuracoesIndicacao = apuracaIndicadorRentabilidadeRepositorio.BuscarApuracaoIndicadorRentabilidade(token.IdEmpresa);

                if (apuracoesIndicacao == null)
                {
                    Logger.Log($"Apura��o de indicadores de rentabilidade n�o encontrada");
                    throw new ArgumentException($"Apura��o de indicadores de rentabilidade n�o encontrada");
                }

                if (apuracoesIndicacao != null)
                {
                    ApuracaoIndicadorRentabilidade idTotal = apuracoesIndicacao.Where(c => c.IdIndicadorRentabilidadePai == null).FirstOrDefault();

                    if (idTotal == null)
                    {
                        Logger.Log($"Id do indicador Consolidado n�o encontrado");
                        throw new ArgumentException($"Id do indicador Consolidado n�o encontrado");
                    }

                    Logger.Log($"Preenche id do indicador Consolidado: {idTotal.IdIndicadorRentabilidade}");

                    var total = (from a in apuracoesIndicacao
                                 where a.IdIndicadorRentabilidadePai == null
                                 select new TotalRentabilidadeModel
                                 {
                                     Maximo = a.Maximo,
                                     Realizado = a.Realizado
                                 }).FirstOrDefault();

                    if (total == null)
                    {
                        Logger.Log($"Objeto total vazio");
                        throw new ArgumentException($"Objeto total vazio");
                    }

                    Logger.Log($"Preenche objeto total para retorno JSON: {retorno.Dados.Extrato.Total}");

                    retorno.Dados.Extrato.Total = total;

                    var idCards = (from a in apuracoesIndicacao
                                   where a.IdIndicadorRentabilidadePai == idTotal.IdIndicadorRentabilidade
                                   select new
                                   {
                                       Id = a.IdIndicadorRentabilidade
                                   }).ToList();

                    if (!idCards.Any())
                    {
                        Logger.Log($"Id dos indicadores dos cards, para filtros de valores, n�o encontrados");
                        throw new ArgumentException($"Id dos indicadores dos cards, para filtros de valores, n�o encontrados");
                    }

                    Logger.Log($"Preenche ids dos indicadores dos cards, para filtros de valores");

                    var cards = (from a in apuracoesIndicacao
                                 where a.IdIndicadorRentabilidadePai == idTotal.IdIndicadorRentabilidade
                                 select new CartaoRentabilidadeModel
                                 {
                                     Nome = a.Nome,
                                     Valor = a.Realizado,
                                     Tipo = (int)a.IdIndicadorRentabilidadePai
                                 }).ToList();

                    if (!cards.Any())
                    {
                        Logger.Log($"Objeto cards vazio");
                        throw new ArgumentException($"Objeto cards vazio");
                    }

                    Logger.Log($"Preenche objeto cards para retorno JSON: {retorno.Dados.Cartoes}");

                    retorno.Dados.Cartoes = cards;

                    var valor = (from a in apuracoesIndicacao
                                 join c in idCards
                                 on a.IdIndicadorRentabilidadePai equals c.Id
                                 select new ValorRentabilidadeModel
                                 {
                                     Nome = a.Nome,
                                     Maximo = a.Maximo,
                                     Realizado = a.Realizado,
                                     Tipo = a.Tipo
                                 }).ToList();

                    if (!valor.Any())
                    {
                        Logger.Log($"Objeto valor vazio");
                        throw new ArgumentException($"Objeto valor vazio");
                    }

                    retorno.Dados.Extrato.Valores = valor;

                    Logger.Log($"Preenche objeto valores para retorno JSON: {retorno.Dados.Extrato.Valores}");
                }

                Logger.Log($"Busca apura��o de benef�cios de rentabilidade");
                IList<ApuracaoBeneficioRentabilidade> apuracaoBeneficio = apuracaoBeneficioRentabilidadeRepositorio.BuscarApuracaoBeneficioRentabilidade(token.IdEmpresa);

                if (apuracaoBeneficio == null)
                {
                    Logger.Log($"Apura��o de benef�cios de rentabilidade n�o encontrada");
                    throw new ArgumentException($"Apura��o de benef�cios de rentabilidade n�o encontrada");
                }

                if (apuracaoBeneficio != null)
                {
                    for (int i = 0; i < apuracaoBeneficio.Count; i++)
                    {
                        BeneficioRentabilidadeModel beneficio = new BeneficioRentabilidadeModel();

                        beneficio.Nome = apuracaoBeneficio[i].Nome;
                        beneficio.Maximo = apuracaoBeneficio[i].Valor;

                        retorno.Dados.Extrato.Beneficios.Add(beneficio);
                    }
                }
                Logger.Log($"Preenche objeto benef�cios para retorno JSON: {retorno.Dados.Extrato.Beneficios}");

                response = CreateResponse(retorno);

                LogMessage(context, "Solicita��o de processamento bem-sucedida.");

            }
            catch (Exception exception)
            {
                LogMessage(context, string.Format("Falha na solicita��o de processamento - {0}", exception.Message));

                response = CreateResponse(null);
            }

            return response;
        }
    }
}
