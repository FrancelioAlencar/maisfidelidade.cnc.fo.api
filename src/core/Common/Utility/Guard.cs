﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Common.Utility
{
    public static class Guard
    {
        public static void ArgumentNotNull(object argumentValue, string argumentName)
        {
            if (argumentValue == null)
            {
                throw new ArgumentNullException(argumentName);
            }
        }

    }
}
