﻿using System.Collections;
using System.Collections.Generic;
using maisfidelidade.cnc.fo.api.core.Domain.Model.ApuracoesBeneficioRentabilidade;
using maisfidelidade.cnc.fo.api.core.Infrastructure;
using maisfidelidade.cnc.fo.api.core.Infrastructure.RepositoryFramework;

namespace maisfidelidade.cnc.fo.api.core.Domain
{
    public interface IApuracaoBeneficioRentabilidadeRepositorio : IRepositorio<ApuracaoBeneficioRentabilidade>
    {
        IList<ApuracaoBeneficioRentabilidade> BuscarApuracaoBeneficioRentabilidade(int idEmpresa);
    }
}