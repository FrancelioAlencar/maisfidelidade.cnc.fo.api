ALTER TABLE db_maisfidelidade.tb_beneficio
ADD como_funciona varchar(1000);

SET SQL_SAFE_UPDATES = 0;

UPDATE db_maisfidelidade.tb_beneficio 
SET como_funciona = 'Solução perfeita para bombar suas vendas. Verba disponível para investimento em mídias.'
WHERE nome = 'Feirão' AND ativo = 1;

UPDATE db_maisfidelidade.tb_beneficio 
SET como_funciona = ''
WHERE nome = 'Material PDV' AND ativo = 1;

UPDATE db_maisfidelidade.tb_beneficio 
SET como_funciona = 'Desconto na fatura do plano contratado.'
WHERE nome = 'Desconto Plano + Lead' AND ativo = 1;

UPDATE db_maisfidelidade.tb_beneficio 
SET como_funciona = 'Possibilidade de formalizar o pagamento de uma proposta mesmo que o veículo esteja alienado à alguma instituição financeira, sem a necessidade de realizar a baixa.'
WHERE nome = 'Gravame' AND ativo = 1;

UPDATE db_maisfidelidade.tb_beneficio 
SET como_funciona = 'Disponível para as melhores proposta a partir do dia 10 do mês subsequente, através do botão "Negociação de taxa".'
WHERE nome = 'Negociação de Taxa' AND ativo = 1;

UPDATE db_maisfidelidade.tb_beneficio 
SET como_funciona = ''
WHERE nome = 'Feirão +Fidelidade – Extensão Mega' AND ativo = 1;

UPDATE db_maisfidelidade.tb_beneficio 
SET como_funciona = ''
WHERE nome = 'Prorrogação de Documentos' AND ativo = 1;

UPDATE db_maisfidelidade.tb_beneficio 
SET como_funciona = 'Plataforma que permite gerenciar leads de forma simples e prática, através de ferramentas automatizadas, que focam na negociação e aproximação entre vendedor e comprador.'
WHERE nome = 'CRM Smart' AND ativo = 1;

UPDATE db_maisfidelidade.tb_beneficio 
SET como_funciona = 'Para cada indicação de clientes não correntistas que dentro de 30 dias abrirem conta corrente com pacote de tarifas no Santander, receba remuneração proporcional a classificação no +fidelidade e + 5 Pontos no programa por conta corrente convertida.
O pagamento será feito via emissão de Nota Fiscal os valores são:
R$ 20,00 - BRONZE/SILVER
R$ 30,00 - GOLD
R$ 50,00 - PLATINUM'
WHERE nome = 'Bônus indicação CC PF' AND ativo = 1;

UPDATE db_maisfidelidade.tb_beneficio 
SET como_funciona = 'Termômetro de indicações dos clientes (leads) com propensão de financiamento no Banco Santander, de acordo com análise de comportamento e histórico de cada cliente.A indicação varia em 3 “temperaturas”: Quente, Morno ou Frio e a visualização é através do CRM Webmotors'
WHERE nome = 'Esquenta Lead' AND ativo = 1;

UPDATE db_maisfidelidade.tb_beneficio 
SET como_funciona = 'Pagamento das propostas livre de pendência de Gravame aos finais de semana, com este, o lojista poderá consultar a disponibilidade do valor no extrato bancário da conta corrente. Apenas para lojistas correntistas Santander.'
WHERE nome = 'Pagamento aos finais de semana' AND ativo = 1;

UPDATE db_maisfidelidade.tb_beneficio 
SET como_funciona = 'Se elegível, o pagamento de comissionamento referente aos contratos originados pelo ponto de venda, será efetuado via conta corrente do lojista.'
WHERE nome = 'Plus' AND ativo = 1;

UPDATE db_maisfidelidade.tb_beneficio 
SET como_funciona = 'Crédito do bônus em Conta Corrente do cliente PF que realize o pagamento do financiamento via Débito em Conta.'
WHERE nome = 'Max Bônus PF' AND ativo = 1;

UPDATE db_maisfidelidade.tb_beneficio 
SET como_funciona = 'Ofertas proativas de créditos pré-aprovados a toda a carteira de clientes correntistas Santander direcionando o estoque da loja na Webmotors para atrair novos consumidores propensos a compra de Veículos.'
WHERE nome = 'Pré Aprovado' AND ativo = 1;

UPDATE db_maisfidelidade.tb_beneficio 
SET como_funciona = 'Ofertas ativa e personalizadas a clientes da carteira da loja no momento da quitação do contrato ou quitado para incentivo a retornar a mesma loja e efetuar nova compra ou troca do veículo.'
WHERE nome = 'Troque Já' AND ativo = 1;

UPDATE db_maisfidelidade.tb_beneficio 
SET como_funciona = ''
WHERE nome = 'Combo (CRM + Auto Guru)' AND ativo = 1;

UPDATE db_maisfidelidade.tb_beneficio 
SET como_funciona = 'Vendedores que indicarem Conta Corrente recebem Pontos Esfera por Cada Conta. Não é preciso ter cartão de crédito Santander para acumular e trocar os pontos Esfera.
O Vendedor se cadastra em Landing Page da Campanha no Esfera > Realiza Indicações em Plataforma do +Contas incluindo seu CPF > Por cada conta aberta recebe pontuação que pode ser trocada por diversos produtos/serviços> Recebe mensagem do Esfera com crédito de pontos > Visualiza na plataforma quantidade de pontos e opções de troca.'
WHERE nome = 'Pontos esfera - Vendedor' AND ativo = 1;

UPDATE db_maisfidelidade.tb_beneficio 
SET como_funciona = 'O percentual de comissão será aplicado sobre prêmio de seguro conforme classificação.'
WHERE nome = 'Seguro auto' AND ativo = 1;

UPDATE db_maisfidelidade.tb_beneficio 
SET como_funciona = 'O percentual de comissão será aplicado sobre prêmio de seguro conforme classificação.'
WHERE nome = 'Seguro prestamista' AND ativo = 1;

UPDATE db_maisfidelidade.tb_beneficio 
SET como_funciona = ''
WHERE nome = 'Negociação de crédito a prazo' AND ativo = 1;

SET SQL_SAFE_UPDATES = 1;
