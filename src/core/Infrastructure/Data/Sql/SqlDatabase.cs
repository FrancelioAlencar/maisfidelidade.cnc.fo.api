﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Infrastructure.Data.Sql
{
    public class SqlDatabase : Database
    {
        public SqlDatabase(string connectionString)
            : base(connectionString, SqlClientFactory.Instance)
        {

        }
    }
}
