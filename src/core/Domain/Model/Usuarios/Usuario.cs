﻿using maisfidelidade.cnc.fo.api.core.Configuration;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Empresas;
using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Usuarios
{
    public class Usuario : EntityBase, IAggregateRoot
    {
        private string email = String.Empty;
        public string Nome { get; set; }
        public string Email { get => email.ToLower(); set => email = value; }
        public string Senha { get; set; }
        public bool TrocarSenha { get; set; }
        public Perfil Perfil { get; set; }
        public int IdEmpresa { get; set; }
        public IList<EmpresaResumido> Empresas { get; set; }

        public bool ValidaSenha(string senha)
        {
            if (this.Senha.Equals(Encrypt(senha)))
                return true;

            return false;
        }

        public bool ValidaToken(string token)
        {
            return ValidateToken(token);
        }

        public int ObterIdUsuario(string token)
        {
            var stream = token;
            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadToken(stream);
            var tokenS = handler.ReadToken(stream) as JwtSecurityToken;

            return int.Parse(tokenS.Claims.First(claim => claim.Type == "IdUsuario").Value);
        }

        private static bool ValidateToken(string authToken)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var validationParameters = GetValidationParameters();

            SecurityToken validatedToken;
            IPrincipal principal = tokenHandler.ValidateToken(authToken, validationParameters, out validatedToken);
            return true;
        }

        private static TokenValidationParameters GetValidationParameters()
        {
            return new TokenValidationParameters()
            {
                ValidateLifetime = false,
                ValidateAudience = false,
                ValidateIssuer = false,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(ConfigurationManager.AppSettings["JWT:Key"]))
            };
        }

        public string GerarNovaSenha()
        {
            this.Senha = String.Empty;
            this.TrocarSenha = true;
            var random = new Random();
            return random.Next(999999).ToString();
        }

        public string Encrypt(string strData)
        {
            return Convert.ToBase64String(Encrypt(Encoding.UTF8.GetBytes(strData)));
        }

        public static byte[] Encrypt(byte[] strData)
        {
            using (var passbytes =
                new PasswordDeriveBytes(ConfigurationManager.AppSettings["JWT:Key"],
                    new byte[] { 0x19, 0x59, 0x17, 0x41 }))
            {
                var memstream = new MemoryStream();
                using (Aes aes = new AesManaged())
                {
                    aes.Key = passbytes.GetBytes(aes.KeySize / 8);
                    aes.IV = passbytes.GetBytes(aes.BlockSize / 8);

                    using (var cryptostream = new CryptoStream(memstream, aes.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cryptostream.Write(strData, 0, strData.Length);
                        cryptostream.Close();
                        return memstream.ToArray();
                    }
                }
            }
        }

        public string GerarToken()
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(ConfigurationManager.AppSettings["JWT:Key"]);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                        {
                        new Claim(JwtRegisteredClaimNames.UniqueName, this.Email),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        new Claim("IdUsuario", this.Id.ToString()),
                        new Claim(nameof(this.Nome), this.Nome),
                        new Claim(nameof(this.Email), this.Email),
                        new Claim(nameof(this.TrocarSenha), this.TrocarSenha.ToString()),
                        new Claim(nameof(this.Perfil), JsonConvert.SerializeObject(this.Perfil))
                        //new Claim(nameof(this.Empresas), JsonConvert.SerializeObject(this.Empresas))
                        }),
                    Expires = DateTime.UtcNow.AddDays(7),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256)
                };

                var token = tokenHandler.CreateToken(tokenDescriptor);

                return tokenHandler.WriteToken(token);
            }
            catch (Exception error)
            {
                throw new Exception($"Usuario > GeraToken: {error.Message}");
            }
        }
    }
}
