﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Banners
{
    public class Banner : EntityBase, IAggregateRoot
    {
        [JsonProperty("name")]
        public string Nome { get; set; }

        [JsonProperty("type")]
        public int Tipo { get; set; }

        [JsonProperty("banner_type")]
        public string TipoBanner { get; set; }

        [JsonProperty("file")]
        public string Link { get; set; }

        [JsonProperty("src")]
        public string Imagem { get; set; }

        [JsonProperty("html")]
        public string HTML { get; set; }

        [JsonProperty("initial_date")]
        public DateTime DataInicio { get; set; }
        
        [JsonProperty("finish_date")]
        public DateTime DataFim { get; set; }
    }
}
