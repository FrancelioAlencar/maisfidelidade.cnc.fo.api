﻿using maisfidelidade.cnc.fo.api.core.Domain.Model.Banners;
using maisfidelidade.cnc.fo.api.core.Log;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Infrastructure.Repositories
{
    public class BannerRepositorio : MySqlRepositorioBase<Banner>, IBannerRepositorio
    {
        public BannerRepositorio()
        {

        }

        public IList<Banner> Obter(int idEmpresa)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("SELECT C.id_banner AS Id, C.nome AS Nome, id_tipo AS Tipo, D.nome AS TipoBanner, B.link AS Link, imagem AS Imagem, html AS HTML, dt_inicio AS DataInicio, dt_fim AS DataFim, C.ativo AS Ativo, C.dt_inclusao AS DataInclusao, C.dt_alteracao AS DataAlteracao ");
                query.Append(" FROM tb_apuracao AS A ");
                query.Append(" LEFT JOIN tb_apuracao_banner AS B ON B.id_apuracao = A.id_apuracao ");
                query.Append(" LEFT JOIN tb_banner AS C ON C.id_banner = B.id_banner ");
                query.Append(" LEFT JOIN tb_tipo_banner AS D ON D.id_tipo_banner = C.id_tipo ");
                query.Append(" WHERE A.id_arquivo = (SELECT id_arquivo FROM tb_arquivo WHERE ultimo_processado = 1 LIMIT 1) ");
                query.AppendFormat(" AND A.id_empresa = {0} AND C.ativo = 1 AND (dt_inicio IS NULL AND dt_fim IS NULL) OR(NOW() BETWEEN dt_inicio AND dt_fim) ", idEmpresa);
                query.Append(" UNION ");
                query.Append(" SELECT A.id_banner AS Id, A.nome AS Nome, id_tipo AS Tipo, B.nome AS TipoBanner, link AS Link, imagem AS Imagem, html AS HTML, dt_inicio AS DataInicio, dt_fim AS DataFim, A.ativo AS Ativo, A.dt_inclusao AS DataInclusao, A.dt_alteracao AS DataAlteracao ");
                query.Append(" FROM tb_banner AS A ");
                query.Append(" LEFT JOIN tb_tipo_banner AS B ON B.id_tipo_banner = A.id_tipo ");
                query.Append(" WHERE A.ativo = 1 AND (dt_inicio IS NULL AND dt_fim IS NULL) OR(NOW() BETWEEN dt_inicio AND dt_fim) ");

                Logger.Log($"BannerRepositorio > Obter Query MySql: {query.ToString()}");

                return this.BuildEntitiesFromSql(query.ToString());
            }
            catch (Exception error)
            {
                throw new Exception($"BannerRepositorio > Obter: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        protected override void BuildChildCallbacks()
        {
            
        }

        protected override void PersistDeletedItem(Banner item)
        {
            throw new NotImplementedException();
        }

        protected override void PersistNewItem(Banner item)
        {
            throw new NotImplementedException();
        }

        protected override void PersistUpdatedItem(Banner item)
        {
            throw new NotImplementedException();
        }
    }
}
