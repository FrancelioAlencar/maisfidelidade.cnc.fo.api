using System;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using maisfidelidade.cnc.fo.api.core.Common.AWS;
using maisfidelidade.cnc.fo.api.core.Common.DependencyInjection;
using maisfidelidade.cnc.fo.api.core.Domain.Model;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Usuarios;
using maisfidelidade.cnc.fo.api.core.Log;
using Newtonsoft.Json.Linq;
using System.Linq;
using Newtonsoft.Json;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace maisfidelidade.cnc.fo.api.empresa.troca
{
    public class Function : FunctionBase
    {
        private readonly IUsuarioRepositorio usuarioRepositorio;

        public Function()
        {
            Logger.Log($"Resolve Inje��o de Dependencia");

            var dependencyResolver = new DependencyResolver();

            usuarioRepositorio = dependencyResolver.ObterServico<IUsuarioRepositorio>();
        }

        public APIGatewayProxyResponse FunctionHandler(APIGatewayProxyRequest request, ILambdaContext context)
        {
            try
            {
                Logger.Log("Valida token");
                var token = ValidaToken(request);

                Logger.Log($"Deserializa body {request.Body}");
                var requestBody = JObject.Parse(request.Body.ToLower());

                Logger.Log($"Seta IdEmpresa");
                var idEmpresa = int.Parse(requestBody["idempresa"].ToString());

                Logger.Log($"Verifica se o IdEmpresa {idEmpresa} esta na lista de suas empresas");
                if (!token.Empresas.Any(x => x.Id.Equals(idEmpresa)))
                {
                    Logger.Log($"Esta empresa n�o pertence a este usu�rio");
                    throw new Exception($"IdEmpresa {idEmpresa} informado n�o faz parte deste usu�rio {token.IdUsuario}");
                }

                Logger.Log($"Atualiza id_empresa do usu�rio {token.IdUsuario}, da empresa {token.IdEmpresa} para {idEmpresa}");
                usuarioRepositorio.AtualizaIdEmpresa(token.IdUsuario, idEmpresa);

                token.Empresas = token.Empresas.Select(x => { x.EmpresaPadrao = false; return x; }).ToList();
                token.Empresas.Single(x => x.Id.Equals(idEmpresa)).EmpresaPadrao = true;

                LogMessage(context, "Solicita��o de processamento bem-sucedida.");

                Usuario usuario = MontaUsuario(token);

                Logger.Log($"Gera token de login");

                return CreateResponse(new
                {
                    success = true,
                    Token = usuario.GerarToken()
                });
            }
            catch (Exception _exception)
            {
                LogMessage(context, string.Format("Falha na solicita��o de processamento - {0}", _exception.Message));
                Logger.Log($"Error ao trocar empresa: error ::{_exception.Message} - StackTrace:: {_exception.StackTrace} ");
                _exception.Data.Add("LambdaContext", JsonConvert.SerializeObject(context));
                return CreateResponse(new
                {
                    success = false,
                    message = $"Ocorreu um erro ao tentar processar sua solicita��o.<br/>Tente novamente em instantes. Se o erro persistir, fale com {PessoaContato()}."
                });
            }
        }

        public Usuario MontaUsuario(Token token)
        {
            Usuario usuario = new Usuario();

            usuario.Email = token.Email;
            usuario.Empresas = token.Empresas;
            usuario.Id = token.IdUsuario;
            usuario.IdEmpresa = token.IdEmpresa;
            usuario.Nome = token.Nome;
            usuario.Perfil = token.Perfil;
            usuario.TrocarSenha = token.TrocaSenha;

            return usuario;
        }
    }
}
