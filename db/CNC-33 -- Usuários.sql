-- --------------------------------------------------------
-- Servidor:                     rds-aurora-hk-vmotors-2018.cuqb1lmjd24e.us-east-1.rds.amazonaws.com
-- Versão do servidor:           5.6.10-log - MySQL Community Server (GPL)
-- OS do Servidor:               Linux
-- HeidiSQL Versão:              11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura para tabela db_concessionaria.tb_perfil
CREATE TABLE IF NOT EXISTS `tb_perfil` (
  `id_perfil` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `fl_ativo` bit(1) NOT NULL DEFAULT b'0',
  `dt_alteracao` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_perfil`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_perfil: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_perfil` DISABLE KEYS */;
INSERT INTO `tb_perfil` (`id_perfil`, `nome`, `fl_ativo`, `dt_alteracao`) VALUES
	(1, 'Administrador', b'1', '2020-07-06 11:50:00'),
	(2, 'GC', b'1', '2020-07-10 10:10:01'),
	(3, 'GR', b'1', '2020-07-10 10:10:01');
/*!40000 ALTER TABLE `tb_perfil` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_perfil_permissao
CREATE TABLE IF NOT EXISTS `tb_perfil_permissao` (
  `id_perfil_permissao` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_perfil` int(11) NOT NULL DEFAULT '0',
  `id_permissao` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_perfil_permissao`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_perfil_permissao: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_perfil_permissao` DISABLE KEYS */;
INSERT INTO `tb_perfil_permissao` (`id_perfil_permissao`, `id_perfil`, `id_permissao`) VALUES
	(1, 1, 1),
	(2, 1, 2);
/*!40000 ALTER TABLE `tb_perfil_permissao` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_permissao
CREATE TABLE IF NOT EXISTS `tb_permissao` (
  `id_permissao` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `dt_alteracao` datetime NOT NULL,
  `fl_ativo` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id_permissao`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_permissao: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_permissao` DISABLE KEYS */;
INSERT INTO `tb_permissao` (`id_permissao`, `nome`, `dt_alteracao`, `fl_ativo`) VALUES
	(1, 'Dashboard', '2020-07-07 14:55:45', b'1'),
	(2, 'Simulador', '2020-07-07 14:56:24', b'1');
/*!40000 ALTER TABLE `tb_permissao` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_usuario
CREATE TABLE IF NOT EXISTS `tb_usuario` (
  `id_usuario` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) NOT NULL,
  `email` varchar(250) NOT NULL,
  `hash_senha` varchar(250) NOT NULL,
  `dt_cadastro` datetime NOT NULL,
  `dt_alteracao` datetime NOT NULL,
  `fl_trocar_senha` bit(1) NOT NULL DEFAULT b'0',
  `fl_ativo` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_usuario: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_usuario` DISABLE KEYS */;
INSERT INTO `tb_usuario` (`id_usuario`, `nome`, `email`, `hash_senha`, `dt_cadastro`, `dt_alteracao`, `fl_trocar_senha`, `fl_ativo`) VALUES
	(1, 'Davi Caetano', 'davi.caetano@opah.com.br', 'AQAAAAEAACcQAAAAEJ7ueaYQKlkgz2f1BaVM6TIsl0Eh4VbzeWql7bWwfK1aOJw2Sqqs1pWvbberOKpd7A==', '2020-07-06 12:00:56', '2020-07-07 10:55:11', b'0', b'1');
/*!40000 ALTER TABLE `tb_usuario` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_usuario_perfil
CREATE TABLE IF NOT EXISTS `tb_usuario_perfil` (
  `id_usuario_perfil` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `id_perfil` int(11) NOT NULL,
  PRIMARY KEY (`id_usuario_perfil`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_usuario_perfil: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_usuario_perfil` DISABLE KEYS */;
INSERT INTO `tb_usuario_perfil` (`id_usuario_perfil`, `id_usuario`, `id_perfil`) VALUES
	(1, 1, 1);
/*!40000 ALTER TABLE `tb_usuario_perfil` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
