﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace maisfidelidade.cnc.fo.api.core.View.Model.SimuladoresModel
{
    public class DadosSimuladorModel
    {
        public DadosSimuladorModel()
        {
            ParticipacaoMercado = new ParticipacaoMercadoSimuladorModel();
            Ip = new IpSimuladorModel();
            Extrato = new ExtratoSimuladorModel();
        }

        [JsonProperty("marketShare")]
        public ParticipacaoMercadoSimuladorModel ParticipacaoMercado { get; set; }

        [JsonProperty("ip")]
        public IpSimuladorModel Ip { get; set; }

        [JsonProperty("extract")]
        public ExtratoSimuladorModel Extrato { get; set; }
    }
}