﻿namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Indicadores.Performance
{
    public enum IndicadorPerformanceEnum
    {
        Financiamento = 10,
        SeguroAuto = 20,
        SeguroPrestamista = 30
    }
}
