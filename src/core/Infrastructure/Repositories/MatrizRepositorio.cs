﻿using maisfidelidade.cnc.fo.api.core.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using maisfidelidade.cnc.fo.api.core.Log;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Matrizes;
using Amazon.SQS.Model;
using maisfidelidade.cnc.fo.api.core.Domain.Model.MatrizesIndicadoresRentabilidade;
using maisfidelidade.cnc.fo.api.core.Domain.Model.MatrizesBeneficiosRentabilidade;

namespace maisfidelidade.cnc.fo.api.core.Infrastructure.Repositories
{
	public class MatrizRepositorio : MySqlRepositorioBase<Matriz>, IMatrizRepositorio
	{
		public MatrizRepositorio()
		{

		}
		public IList<Matriz> BuscarMatriz(int segmento)
		{
			var builder = new StringBuilder();
			builder.Append("SELECT ");
			builder.Append("m.id_matriz AS IdMatriz, ");
			builder.Append("fv.id_faixa AS IdFaixaVertical, ");
			builder.Append("fv.nome AS FaixaVertical, ");
			builder.Append("fv.percentual_de AS PorcentagemDeVertical, ");
			builder.Append("fv.percentual_ate AS PorcentagemAteVertical, ");
			builder.Append("fh.id_faixa AS IdFaixaHorizontal, ");
			builder.Append("fh.nome AS FaixaHorizontal, ");
			builder.Append("fh.percentual_de AS PorcentagemDeHorizontal, ");
			builder.Append("fh.percentual_ate AS PorcentagemAteHorizontal, ");
			builder.Append("c.nome AS Categoria, ");
			builder.Append("m.valor AS Valor ");
			builder.Append("FROM ");
			builder.Append("tb_matriz m ");
			builder.Append("INNER JOIN tb_faixa fv ");
			builder.Append("ON m.id_faixa_vertical = fv.id_faixa ");
			builder.Append("INNER JOIN tb_faixa fh ");
			builder.Append("ON m.id_faixa_horizontal = fh.id_faixa ");
			builder.Append("INNER JOIN tb_categoria c ");
			builder.Append("ON m.id_categoria = c.id_categoria ");
			builder.Append("WHERE ");
			builder.Append("fv.id_segmento = " + segmento + " ");
			builder.Append("AND fh.id_segmento = " + segmento + " ");
			builder.Append("ORDER BY ");
			builder.Append("m.id_matriz ASC ");

			Logger.Log($"Busca Matriz: {builder.ToString()}");

			return this.BuildEntitiesFromSql(builder.ToString());
		}

		protected override void BuildChildCallbacks()
		{
			ChildCallbacks.Add("beneficios", delegate (IList<Matriz> matriz, object childKeyName)
			{
				this.AppendMatrizIndicadorRentabilidade(matriz);
				this.AppendMatrizBeneficioRentabilidade(matriz);
			}
			);
		}

		private void AppendMatrizIndicadorRentabilidade(IList<Matriz> matrizes)
		{
			if (!matrizes.Any())
				return;

			StringBuilder builder = new StringBuilder();

			builder.Append("SELECT ");
			builder.Append("mir.id_matriz as IdMatriz, ");
			builder.Append("CONCAT(fh.nome, ' - ', fv.nome) as Matriz, ");
			builder.Append("ir.nome as IndicadorRentabilidade, ");
			builder.Append("mir.valor as Valor ");
			builder.Append("FROM tb_matriz_indicador_rentabilidade mir ");
			builder.Append("INNER JOIN tb_matriz m ");
			builder.Append("on mir.id_matriz = m.id_matriz ");
			builder.Append("INNER JOIN tb_faixa fh ");
			builder.Append("on m.id_faixa_horizontal = fh.id_faixa ");
			builder.Append("INNER JOIN tb_faixa fv ");
			builder.Append("ON m.id_faixa_vertical = fv.id_faixa ");
			builder.Append("INNER JOIN tb_indicador_rentabilidade ir ");
			builder.Append("ON mir.id_indicador_rentabilidade = ir.id_indicador_rentabilidade ");
			builder.Append("ORDER BY mir.id_matriz ASC ");

			Logger.Log($"Busca Matriz Indicador Rentabilidade: {builder.ToString()}");
			
			var matrizesIndicadoresRentabilidade = this.ExecuteQuery<MatrizIndicadorRentabilidade>(builder.ToString());

			foreach (var matriz in matrizes)
			{
				matriz.MatrizIndicadorRentabilidade = new List<MatrizIndicadorRentabilidade>(matrizesIndicadoresRentabilidade.Where(c => c.IdMatriz == matriz.IdMatriz));
			}
		}

		private void AppendMatrizBeneficioRentabilidade(IList<Matriz> matrizes)
		{
			if (!matrizes.Any())
				return;

			StringBuilder builder = new StringBuilder();

			builder.Append("SELECT ");
			builder.Append("mbr.id_matriz as IdMatriz, ");
			builder.Append("CONCAT(fh.nome, ' - ', fv.nome) as Matriz, ");
			builder.Append("br.nome as BeneficioRentabilidade, ");
			builder.Append("mbr.valor as Valor ");
			builder.Append("FROM tb_matriz_beneficio_rentabilidade mbr ");
			builder.Append("INNER JOIN tb_matriz m ");
			builder.Append("on mbr.id_matriz = m.id_matriz ");
			builder.Append("INNER JOIN tb_faixa fh ");
			builder.Append("on m.id_faixa_horizontal = fh.id_faixa ");
			builder.Append("INNER JOIN tb_faixa fv ");
			builder.Append("ON m.id_faixa_vertical = fv.id_faixa ");
			builder.Append("INNER JOIN tb_beneficio_rentabilidade br ");
			builder.Append("ON mbr.id_beneficio_rentabilidade = br.id_beneficio_rentabilidade ");
			builder.Append("ORDER BY mbr.id_matriz ASC ");

			Logger.Log($"Busca Matriz Benefício Rentabilidade: {builder.ToString()}");

			var matrizesBeneficiosRentabilidade = this.ExecuteQuery<MatrizBeneficioRentabilidade>(builder.ToString());

			foreach (var matriz in matrizes)
			{
				matriz.MatrizBeneficioRentabilidade = new List<MatrizBeneficioRentabilidade>(matrizesBeneficiosRentabilidade.Where(c => c.IdMatriz == matriz.IdMatriz));
			}
		}

		protected override void PersistDeletedItem(Matriz item)
		{
			throw new NotImplementedException();
		}

		protected override void PersistNewItem(Matriz item)
		{
			throw new NotImplementedException();
		}

		protected override void PersistUpdatedItem(Matriz item)
		{
			throw new NotImplementedException();
		}
	}
}
