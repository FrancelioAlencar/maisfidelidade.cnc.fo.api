﻿using maisfidelidade.cnc.fo.api.core.Common.AWS;
using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using Newtonsoft.Json;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.ApuracoesIndicadorClassificacao
{
    /// <summary>
    ///
    /// </summary>
    public class ApuracaoIndicadorClassificacao : EntityBase, IAggregateRoot
    {
        [JsonProperty("fileType")]
        public int TipoArquivo { get; set; }

        [JsonProperty("ratingIndexId")]
        public int IdIndicadorClassificacao { get; set; }

        [JsonProperty("zoneId")]
        public int IdFaixa { get; set; }
    }
}