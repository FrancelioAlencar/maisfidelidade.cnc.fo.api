﻿namespace maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase
{
    /// <summary>
    ///
    /// </summary>
    public interface IEntity
    {
        int Id { get; set; }
    }
}
