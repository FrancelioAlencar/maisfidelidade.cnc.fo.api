﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Infrastructure.Data
{
    public class ConnectionString
    {
        private string connectionString;

        public ConnectionString(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentException("Resources.ExceptionNullOrEmptyString", nameof(connectionString));
            }

            this.connectionString = connectionString;
        }
    }
}
