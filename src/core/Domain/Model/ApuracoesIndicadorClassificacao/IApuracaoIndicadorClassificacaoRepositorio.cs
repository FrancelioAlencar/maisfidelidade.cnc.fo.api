﻿using System.Collections;
using System.Collections.Generic;
using maisfidelidade.cnc.fo.api.core.Domain.Model.ApuracoesIndicadorClassificacao;
using maisfidelidade.cnc.fo.api.core.Infrastructure;
using maisfidelidade.cnc.fo.api.core.Infrastructure.RepositoryFramework;

namespace maisfidelidade.cnc.fo.api.core.Domain
{
    public interface IApuracaoIndicadorClassificacaoRepositorio : IRepositorio<ApuracaoIndicadorClassificacao>
    {
        IList<ApuracaoIndicadorClassificacao> BuscarApuracaoIndicadorClassificacao(int idUsuario);
    }
}

