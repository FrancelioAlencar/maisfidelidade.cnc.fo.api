﻿using System;
using System.Data.Common;
using System.Threading;

namespace maisfidelidade.cnc.fo.api.core.Infrastructure.Data
{
    /// <summary>
    /// Esta é uma pequena classe auxiliar usada para gerenciar o fechamento de uma conexão na presença de pool de transações. 
    /// Na verdade, não podemos fechar a conexão até que todos os usem estejam concluídos; portanto, precisamos da contagem de referências.
    /// </summary>
    public class DatabaseConnectionWrapper : IDisposable
    {
        private int refCount;

        public DbConnection Conexao { get; private set; }

        public bool IsDisposed
        {
            get { return (this.refCount == 0); }
        }

        public DatabaseConnectionWrapper(DbConnection connection)
        {
            Conexao = connection;
            this.refCount = 1;
        }

        public DatabaseConnectionWrapper AddRef()
        {
            Interlocked.Increment(ref this.refCount);
            return this;
        }

        public void Dispose()
        {
            this.Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing && (Interlocked.Decrement(ref refCount) == 0))
            {
                this.Conexao.Dispose();
                this.Conexao = null;
                GC.SuppressFinalize(this);
            }
        }
    }
}
