﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Empresas
{
    public class EmpresaResumido : EntityBase, IAggregateRoot
    {
        public string CNPJ { get; set; }
        public string NomeFantasia { get; set; }
        public bool EmpresaPadrao { get; set; }
    }
}
