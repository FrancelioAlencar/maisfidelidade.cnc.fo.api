using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using maisfidelidade.cnc.fo.api.core.Common.AWS;
using maisfidelidade.cnc.fo.api.core.Common.DependencyInjection;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Usuarios;
using System;
using Newtonsoft.Json;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Empresas;
using maisfidelidade.cnc.fo.api.core.Log;
using maisfidelidade.cnc.fo.api.core.Infrastructure.Repositories;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Apuracoes;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Classificacao;
using System.Collections;
using System.Collections.Generic;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Gerentes;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace maisfidelidade.cnc.fo.api.cliente.classificacao
{
    public class Function : FunctionBase
    {
        private readonly ILogAcessoRepositorio logAcessoRepositorio;
        private readonly IApuracaoRepositorio apuracaoRepositorio;
        private IEmpresaRepositorio empresaRepositorio;
        private IGerenteRepositorio gerenteRepositorio;

        DependencyResolver dependencyResolver;

        public Function()
        {
            Logger.Log($"Resolve Injeção de Dependencia");

            dependencyResolver = new DependencyResolver();

            logAcessoRepositorio = dependencyResolver.ObterServico<ILogAcessoRepositorio>();
            apuracaoRepositorio = dependencyResolver.ObterServico<IApuracaoRepositorio>();
        }

        public APIGatewayProxyResponse FunctionHandler(APIGatewayProxyRequest request, ILambdaContext context)
        {
            try
            {
                Logger.Log("Valida token");
                var token = ValidaToken(request);

                Classificacao classificacao = apuracaoRepositorio.ObterClassificacao(token.IdEmpresa);

                if (classificacao == null)
                {
                    return CreateResponse(new
                    {
                        success = false,
                        message = $"Empresa sem apuração!"
                    });
                }

                DateTime ultimoAcesso = logAcessoRepositorio.ObterUltimoAcesso(token.IdUsuario);

                if (ultimoAcesso == null)
                    ultimoAcesso = DateTime.Now;

                if (!token.Perfil.Id.Equals((int)PerfilEnum.Lojista))
                {
                    if (empresaRepositorio == null)
                        empresaRepositorio = dependencyResolver.ObterServico<IEmpresaRepositorio>();

                    if (gerenteRepositorio == null)
                        gerenteRepositorio = dependencyResolver.ObterServico<IGerenteRepositorio>();

                    if (token.Perfil.Id == (int)PerfilEnum.GC || token.Perfil.Id == (int)PerfilEnum.GR)
                    {
                        Logger.Log($"Busca informações do gerente: {token.Email}");
                        var gerente = gerenteRepositorio.ObterPorEmail(token.Email);

                        if (gerente == null)
                        {
                            Logger.Log($"Gerente {gerente.Email} veio nulo");
                            throw new Exception($"Gerente {gerente.Email} não localizado!");
                        }

                        if (!gerente.Ativo)
                        {
                            Logger.Log($"Gerente {gerente.Email} inativo");
                            throw new Exception($"Gerente {gerente.Id} inativo.");
                        }

                        if (token.Perfil.Id == (int)PerfilEnum.GC)
                        {
                            Logger.Log($"Busca informações da empresa de acordo com o GC: {gerente.Matricula}");
                            token.Empresas = empresaRepositorio.ObterPorGC(gerente.Matricula);
                        }
                        else
                        {
                            Logger.Log($"Busca informações da empresa de acordo com o GR: {gerente.Matricula}");
                            token.Empresas = empresaRepositorio.ObterPorGR(gerente.Matricula);
                        }
                    }
                }

                IList<object> empresas = new List<object>();

                if (token.Empresas != null)
                {
                    foreach (EmpresaResumido empresa in token.Empresas)
                    {
                        empresas.Add(new
                        {
                            id = empresa.Id,
                            name = empresa.NomeFantasia,
                            company_default = empresa.EmpresaPadrao
                        });
                    }
                }

                return CreateResponse(new
                {
                    success = true,
                    data = new
                    {
                        lastAtt = ultimoAcesso,
                        marketShare = classificacao.MarketShare,
                        ips = classificacao.IPS,
                        store = new
                        {
                            id = token.IdEmpresa,
                            name = classificacao.NomeFantasia,
                            segment = classificacao.Segmento,
                            segment_id = classificacao.IdSegmento,
                            group = classificacao.Grupo,
                        },
                        stores = empresas,
                        category = new
                        {
                            id = classificacao.IdCategoria,
                            name = classificacao.Categoria
                        }
                    }
                });
            }
            catch (Exception _exception)
            {
                LogMessage(context, string.Format("Falha na solicitação de processamento - {0}", _exception.Message));
                Logger.Log($"Error no classificação: error ::{_exception.Message} - StackTrace:: {_exception.StackTrace} ");
                _exception.Data.Add("LambdaContext", JsonConvert.SerializeObject(context));
                return CreateResponse(new
                {
                    success = false,
                    message = $"Ocorreu um erro ao tentar processar sua solicitação.<br/>Tente novamente em instantes. Se o erro persistir, fale com {PessoaContato()}."
                });
            }
        }
    }
}