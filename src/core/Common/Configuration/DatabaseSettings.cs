using maisfidelidade.cnc.fo.api.core.Configuration;

namespace maisfidelidade.cnc.fo.api.core.Common.Configuration
{
	public class DatabaseSettings
	{
		public DatabaseSettings(string prefix)
		{
			Prefix = prefix;

			if (Prefix.Contains("mongodb"))
			{
				Scheme = ConfigurationManager.AppSettings[$"{Prefix}_scheme"];
				Username = ConfigurationManager.AppSettings[$"{Prefix}_username"];
				Password = ConfigurationManager.AppSettings[$"{Prefix}_password"];
				Host = ConfigurationManager.AppSettings[$"{Prefix}_host"];
				Database = ConfigurationManager.AppSettings[$"{Prefix}_database"];
				Options = ConfigurationManager.AppSettings[$"{Prefix}_options"];
			}
			else
			{
				Server = ConfigurationManager.AppSettings[$"{Prefix}_server"];
				Database = ConfigurationManager.AppSettings[$"{Prefix}_database"];
				Username = ConfigurationManager.AppSettings[$"{Prefix}_username"];
				Password = ConfigurationManager.AppSettings[$"{Prefix}_password"];
				Port = ConfigurationManager.AppSettings[$"{Prefix}_port"];
			}
		}

		public string ConnectionString
		{
			get
			{
				if (Prefix.Contains("mongodb"))
					return $"{Scheme}://{Username}:{Password}@{Host}/{Database}{Options}";
				else
					return $"Server={Server};Database={Database};Uid={Username};Pwd={Password};Convert Zero Datetime=True;Allow Zero Datetime=true;";
			}
		}

		protected string Prefix { get; }
		protected string Server { get; }
		protected string Database { get; }
		protected string Username { get; }
		protected string Password { get; }
		protected string Port { get; }
		protected string Scheme { get; }
		protected string Host { get; }
		protected string Options { get; }
	}
}
