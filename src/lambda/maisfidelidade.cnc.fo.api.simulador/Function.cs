using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using maisfidelidade.cnc.fo.api.core.Common.AWS;
using maisfidelidade.cnc.fo.api.core.Common.DependencyInjection;
using maisfidelidade.cnc.fo.api.core.Domain;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Matrizes;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Enums;
using maisfidelidade.cnc.fo.api.core.View.Model.SimuladoresModel;
using maisfidelidade.cnc.fo.api.core.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Apuracoes;
using maisfidelidade.cnc.fo.api.core.Domain.Model.ApuracoesIndicadorClassificacao;
using System.Numerics;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace maisfidelidade.cnc.fo.api.simulador
{
	public class Function : FunctionBase
	{
		private readonly IApuracaoRepositorio apuracaoRepositorio;
		private readonly IApuracaoIndicadorClassificacaoRepositorio apuracaoIndicadorClassificacaoRepositorio;
		private readonly IMatrizRepositorio matrizRepositorio;

		public Function()
		{
			var dependencyResolver = new DependencyResolver();
			
			Logger.Log($"Resolve Inje��o de Dependencia");

			apuracaoRepositorio = dependencyResolver.ObterServico<IApuracaoRepositorio>();
			apuracaoIndicadorClassificacaoRepositorio = dependencyResolver.ObterServico<IApuracaoIndicadorClassificacaoRepositorio>();
			matrizRepositorio = dependencyResolver.ObterServico<IMatrizRepositorio>();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="context"></param>
		/// <returns></returns>
		public APIGatewayProxyResponse FunctionHandler(APIGatewayProxyRequest request, ILambdaContext context)
		{
			APIGatewayProxyResponse response;

			try
			{
				int? idCliente = ObterIdUsuario(request.Headers["Authorization"].ToString());

				Logger.Log($"Seta variavel do Cliente: {idCliente}");

                if (idCliente == null)
                {
					Logger.Log($"Id do Cliente inv�lido: Cliente: {nameof(idCliente)}");
					idCliente = 0;
					throw new ArgumentException("Id do Cliente inv�lido", nameof(idCliente));
                }

				SimuladorModel retorno = new SimuladorModel();

				Logger.Log($"Busca informa��es de apura��o, para a empresa vinculada ao cliente {idCliente}");
				Apuracao apuracaoSemanal = apuracaoRepositorio.BuscarApuracaoSemanal((int)idCliente);

				if (apuracaoSemanal == null)
				{
					Logger.Log($"Apura��o semanal n�o encontrada, para a empresa vinculada ao cliente {idCliente}");
					idCliente = 0;
					throw new ArgumentException($"Apura��o semanal n�o encontrada, para a empresa vinculada ao cliente {idCliente}");
				}

				ParticipacaoMercadoSimuladorModel marketshare = new ParticipacaoMercadoSimuladorModel();
				marketshare.Santander = apuracaoSemanal.FinanciadoSantander;
				marketshare.Loja = apuracaoSemanal.FinanciadoLoja;
				marketshare.Posicao = null;
				Logger.Log($"Preenche objeto marketshare para retorno JSON: {marketshare}");

				IpSimuladorModel ip = new IpSimuladorModel();
				ip.QuantidadeContratos = apuracaoSemanal.QtdContrato;
				ip.QuantidadePrestamista = apuracaoSemanal.QtdPrestamista;
				ip.QuantidadeAuto = apuracaoSemanal.QtdAuto;
				ip.QuantidadeAmbos = apuracaoSemanal.QtdAmbos;
				ip.Posicao = null;
				Logger.Log($"Preenche objeto ip para retorno JSON: {ip}");

				retorno.Dados.ParticipacaoMercado = marketshare;
				retorno.Dados.Ip = ip;
				Logger.Log($"Preenche objeto retorno para retorno JSON: {retorno}");

				Logger.Log($"Busca informa��es de matriz, para o segmento {apuracaoSemanal.IdSegmento}");
				IList<Matriz> matriz = matrizRepositorio.BuscarMatriz(apuracaoSemanal.IdSegmento);

				if (!matriz.Any())
				{
					Logger.Log($"Matriz n�o encontrada, para o segmento {apuracaoSemanal.IdSegmento}");
					idCliente = 0;
					throw new ArgumentException($"Matriz n�o encontrada, para o segmento {apuracaoSemanal.IdSegmento}");
				}

				Logger.Log($"Busca a Apuracao do Indicador de Classificacao");

				IList<ApuracaoIndicadorClassificacao> apuracoesIndicadorClassificacao = apuracaoIndicadorClassificacaoRepositorio.BuscarApuracaoIndicadorClassificacao((int)idCliente);
				if (!apuracoesIndicadorClassificacao.Any())
				{
					Logger.Log($"Apura��es de indicadores de classifica��o n�o encontradas, para o cliente {idCliente}");
					idCliente = 0;
					throw new ArgumentException($"Apura��es de indicadores de classifica��o n�o encontradas, para o cliente {idCliente}");
				}

				Logger.Log($"Seleciona Market Share Atual");

				ApuracaoIndicadorClassificacao apuracaoAtualVertical = apuracoesIndicadorClassificacao.Where(x =>
				x.IdIndicadorClassificacao.Equals((int)LinhaMatrizEnum.Vertical) 
				&& x.TipoArquivo.Equals((int)TipoArquivoEnum.Semanal)).FirstOrDefault();

				if (apuracaoAtualVertical == null)
				{
					Logger.Log($"Apura��o atual market share n�o encontrada, para a empresa vinculada ao cliente {idCliente}");
					idCliente = 0;
					throw new ArgumentException($"Apura��o atual market share n�o encontrada, para a empresa vinculada ao cliente {idCliente}");
				}

				Logger.Log($"Seleciona IP Seguros Atual");

				ApuracaoIndicadorClassificacao apuracaoAtualHorizontal = apuracoesIndicadorClassificacao.Where(x =>
				x.IdIndicadorClassificacao.Equals((int)LinhaMatrizEnum.Horizontal)
				&& x.TipoArquivo.Equals((int)TipoArquivoEnum.Semanal)).FirstOrDefault();

				if (apuracaoAtualHorizontal == null)
				{
					Logger.Log($"Apura��o atual ip seguros n�o encontrada, para a empresa vinculada ao cliente {idCliente}");
					idCliente = 0;
					throw new ArgumentException($"Apura��o atual ip seguros n�o encontrada, para a empresa vinculada ao cliente {idCliente}");
				}

				Logger.Log($"Seleciona Market Share M�s Anterior");

				ApuracaoIndicadorClassificacao apuracaoAnteriorVertical = apuracoesIndicadorClassificacao.Where(x =>
				x.IdIndicadorClassificacao.Equals((int)LinhaMatrizEnum.Vertical)
				&& x.TipoArquivo.Equals((int)TipoArquivoEnum.Mensal)).FirstOrDefault();

				if (apuracaoAnteriorVertical == null)
				{
					Logger.Log($"Apura��o anterior market share n�o encontrada, para a empresa vinculada ao cliente {idCliente}");
					idCliente = 0;
					throw new ArgumentException($"Apura��o anterior market share n�o encontrada, para a empresa vinculada ao cliente {idCliente}");
				}

				Logger.Log($"Seleciona IP Seguros M�s Anterior");

				ApuracaoIndicadorClassificacao apuracaoAnteriorHorizontal = apuracoesIndicadorClassificacao.Where(x =>
				x.IdIndicadorClassificacao.Equals((int)LinhaMatrizEnum.Horizontal)
				&& x.TipoArquivo.Equals((int)TipoArquivoEnum.Mensal)).FirstOrDefault();

				if (apuracaoAnteriorVertical == null)
				{
					Logger.Log($"Apura��o anterior ip seguros n�o encontrada, para a empresa vinculada ao cliente {idCliente}");
					idCliente = 0;
					throw new ArgumentException($"Apura��o anterior ip seguros n�o encontrada, para a empresa vinculada ao cliente {idCliente}");
				}

				Logger.Log($"Marca as c�lulas da matriz - posi��o atual e posi��o m�s anterior");

				for (int i = 0; i < matriz.Count; i++)
				{
					//c�lula valor atual
					if (matriz[i].IdFaixaHorizontal == apuracaoAtualHorizontal.IdFaixa && matriz[i].IdFaixaVertical == apuracaoAtualVertical.IdFaixa)
					{
						matriz[i].Posicao = (int)PosicaoMatrizEnum.Atual;
					}
					//c�lula valor anterior
					if (matriz[i].IdFaixaHorizontal == apuracaoAnteriorHorizontal.IdFaixa && matriz[i].IdFaixaVertical == apuracaoAnteriorVertical.IdFaixa)
					{
						matriz[i].Posicao = (int)PosicaoMatrizEnum.Anterior;
					}
					else
					{
						matriz[i].Posicao = (int)PosicaoMatrizEnum.Nenhuma;
					}
				}

				retorno.Matriz = matriz;
				
				response = CreateResponse(retorno);

				LogMessage(context, "Solicita��o de processamento bem-sucedida.");

			}
			catch (Exception exception)
			{
				LogMessage(context, string.Format("Falha na solicita��o de processamento - {0}", exception.Message));

				response = CreateResponse(null);
			}

			return response;
		}
	}
}
