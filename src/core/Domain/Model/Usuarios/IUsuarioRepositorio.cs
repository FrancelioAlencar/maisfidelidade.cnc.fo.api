﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.RepositoryFramework;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Usuarios
{
    public interface IUsuarioRepositorio : IRepositorio<Usuario>
    {
        Usuario ObterPorEmail(string email);
        Usuario ObterPorId(int id);
        Usuario ObterPorIdEmpresa(int id);
        void TrocaSenha(Usuario usuario);
        Usuario Cadastro(Usuario usuariol);
        void AssociaPerfil(Usuario usuario, PerfilEnum perfil);
        void AtualizaIdEmpresa(int idUsuario, int idEmpresa);
    }
}