CREATE TABLE tb_faixa (
  id_faixa int unsigned not null auto_increment,
  id_indicador_classificacao int unsigned not null,
  id_segmento int unsigned not null,
  nome varchar(45) NOT NULL,
  percentual_de decimal(10,2) not null,
  percentual_ate decimal(10,2) not null,
  dt_inclusao timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  dt_alteracao timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id_faixa),
  CONSTRAINT fk_indicadorclassificacao__faixa FOREIGN KEY (id_indicador_classificacao) REFERENCES tb_indicador_classificacao (id_indicador_classificacao) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT fk_segmento__faixa FOREIGN KEY (id_segmento) REFERENCES tb_segmento (id_segmento) ON DELETE NO ACTION ON UPDATE NO ACTION
);

insert into tb_faixa
(id_indicador_classificacao, id_segmento, nome, percentual_de, percentual_ate)
values
(1, 2, 'FXS1-BD', 0, 5), 
(1, 2, 'FXS2-BD', 6, 10), 
(1, 2, 'FXS3-BD', 11, 35), 
(1, 2, 'FXS4-BD', 36, 100), 
(1, 3, 'FXS1-ID', 0, 10), 
(1, 3, 'FXS2-ID', 11, 20), 
(1, 3, 'FXS3-ID', 21, 50),
(1, 3, 'FXS4-ID', 51, 100),
(1, 4, 'FXS1-RV', 0, 10),
(1, 4, 'FXS2-RV', 11, 20),
(1, 4, 'FXS3-RV', 21, 50),
(1, 4, 'FXS4-RV', 51, 100),
(2, 2, 'FXI1-BD', 0, 20),
(2, 2, 'FXI2-BD', 21, 35), 
(2, 2, 'FXI3-BD', 36, 50), 
(2, 2, 'FXI4-BD', 51, 100), 
(2, 3, 'FXI1-ID', 0, 20), 
(2, 3, 'FXI2-ID', 21, 35), 
(2, 3, 'FXI3-ID', 36, 50), 
(2, 3, 'FXI4-ID', 51, 100), 
(2, 4, 'FXI1-RV', 0, 25), 
(2, 4, 'FXI2-RV', 26, 50),
(2, 4, 'FXI3-RV', 51, 75),
(2, 4, 'FXI4-RV', 76, 100);

CREATE TABLE tb_matriz (
	id_matriz int unsigned not null auto_increment,
	id_faixa_vertical int unsigned not null,
	id_faixa_horizontal int unsigned not null,
	id_categoria int unsigned not null,
	valor decimal(10,2) unsigned not null,
	dt_inclusao timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	dt_alteracao timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (id_matriz),
	CONSTRAINT fk_faixahorizontal__matriz FOREIGN KEY (id_faixa_horizontal) REFERENCES tb_faixa (id_faixa) ON DELETE NO ACTION ON UPDATE NO ACTION,
	CONSTRAINT fk_faixavertical__matriz FOREIGN KEY (id_faixa_vertical) REFERENCES tb_faixa (id_faixa) ON DELETE NO ACTION ON UPDATE NO ACTION,
	CONSTRAINT fk_categoria__matriz FOREIGN KEY (id_categoria) REFERENCES tb_categoria (id_categoria) ON DELETE NO ACTION ON UPDATE NO ACTION
);

insert into tb_matriz
(id_faixa_vertical, id_faixa_horizontal, id_categoria, valor)
values
(1, 13, 1, 5.8),
(2, 13, 2, 6.4),
(3, 13, 2, 6.6),
(4, 13, 2, 6.6),
(1, 14, 1, 6),
(2, 14, 2, 6.6),
(3, 14, 3, 7.8),
(4, 14, 3, 7.9),
(1, 15, 1, 5.9),
(2, 15, 2, 6.8),
(3, 15, 3, 8),
(4, 15, 4, 9.9),
(1, 16, 1, 6.1),
(2, 16, 2, 7),
(3, 16, 4, 9.8),
(4, 16, 4, 10.3),
(5, 17, 1, 5.8),
(6, 17, 2, 6.4),
(7, 17, 2, 6.6),
(8, 17, 2, 6.6),
(5, 18, 1, 6),
(6, 18, 2, 6.6),
(7, 18, 3, 7.8),
(8, 18, 3, 7.9),
(5, 19, 1, 5.9),
(6, 19, 2, 6.8),
(7, 19, 3, 8),
(8, 19, 4, 9.9),
(5, 20, 1, 6.1),
(6, 20, 2, 7),
(7, 20, 3, 9.8),
(8, 20, 4, 10.3),
(9, 21, 1, 5.8),
(10, 21, 2, 6.4),
(11, 21, 2, 6.6),
(12, 21, 2, 6.6),
(9, 22, 1, 6),
(10, 22, 2, 6.6),
(11, 22, 3, 7.8),
(12, 22, 3, 7.9),
(9, 23, 1, 5.9),
(10, 23, 2, 6.8),
(11, 23, 3, 8),
(12, 23, 4, 9.9),
(9, 24, 1, 6.1),
(10, 24, 2, 7),
(11, 24, 4, 9.8),
(12, 24, 4, 10.3);

CREATE TABLE tb_matriz_indicador_rentabilidade (
	id_matriz_indicador_rentabilidade int unsigned not null auto_increment,
	id_matriz int unsigned not null,
	id_indicador_rentabilidade int unsigned not null,
	valor varchar(50) NOT NULL,
	dt_inclusao timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	dt_alteracao timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	constraint pk_matriz_indicador_rentabilidade primary key (id_matriz_indicador_rentabilidade),
	constraint fk_matrizindicadorrentabilidade__matriz foreign key (id_matriz) references tb_matriz (id_matriz),
	constraint fk_matrizindicadorrentabilidade__indicadorrentabilidade foreign key (id_indicador_rentabilidade) references tb_indicador_rentabilidade (id_indicador_rentabilidade)
);

INSERT INTO tb_matriz_indicador_rentabilidade
(id_matriz, id_indicador_rentabilidade, valor)
VALUES
(1, 4, '0,017'),
(17, 4, '0,017'),
(33, 4, '0,017'),
(1, 6, '0,04'),
(17, 6, '0,04'),
(33, 6, '0,04'),
(1, 7, '0,04'),
(17, 7, '0,04'),
(33, 7, '0,04'),
(2, 4, '0,017'),
(18, 4, '0,017'),
(34, 4, '0,017'),
(2, 6, '0,05'),
(18, 6, '0,05'),
(34, 6, '0,05'),
(2, 7, '0,05'),
(18, 7, '0,05'),
(34, 7, '0,05'),
(3, 4, '0,017'),
(19, 4, '0,017'),
(35, 4, '0,017'),
(3, 6, '0,05'),
(19, 6, '0,05'),
(35, 6, '0,05'),
(3, 7, '0,05'),
(19, 7, '0,05'),
(35, 7, '0,05'),
(4, 4, '0,017'),
(20, 4, '0,017'),
(36, 4, '0,017'),
(4, 6, '0,06'),
(20, 6, '0,06'),
(36, 6, '0,06'),
(4, 7, '0,06'),
(20, 7, '0,06'),
(36, 7, '0,06'),
(5, 4, '0,019'),
(21, 4, '0,019'),
(37, 4, '0,019'),
(5, 6, '0,07'),
(21, 6, '0,07'),
(37, 6, '0,07'),
(5, 7, '0,07'),
(21, 7, '0,07'),
(37, 7, '0,07'),
(6, 4, '0,019'),
(22, 4, '0,019'),
(38, 4, '0,019'),
(6, 6, '0,08'),
(22, 6, '0,08'),
(38, 6, '0,08'),
(6, 7, '0,07'),
(22, 7, '0,07'),
(38, 7, '0,07'),
(7, 4, '0,020'),
(23, 4, '0,020'),
(39, 4, '0,020'),
(7, 6, '0,09'),
(23, 6, '0,09'),
(39, 6, '0,09'),
(7, 7, '0,08'),
(23, 7, '0,08'),
(39, 7, '0,08'),
(8, 4, '0,020'),
(24, 4, '0,020'),
(40, 4, '0,020'),
(8, 6, '0,1'),
(24, 6, '0,1'),
(40, 6, '0,1'),
(8, 7, '0,08'),
(24, 7, '0,08'),
(40, 7, '0,08'),
(9, 4, '0,021'),
(25, 4, '0,021'),
(41, 4, '0,021'),
(9, 6, '0,09'),
(25, 6, '0,09'),
(41, 6, '0,09'),
(9, 7, '0,08'),
(25, 7, '0,08'),
(41, 7, '0,08'),
(10, 4, '0,024'),
(26, 4, '0,024'),
(42, 4, '0,024'),
(10, 6, '0,14'),
(26, 6, '0,14'),
(42, 6, '0,14'),
(10, 7, '0,12'),
(26, 7, '0,12'),
(42, 7, '0,12'),
(11, 4, '0,024'),
(27, 4, '0,024'),
(43, 4, '0,024'),
(11, 6, '0,15'),
(27, 6, '0,15'),
(43, 6, '0,15'),
(11, 7, '0,13'),
(27, 7, '0,13'),
(43, 7, '0,13'),
(12, 4, '0,024'),
(28, 4, '0,024'),
(44, 4, '0,024'),
(12, 6, '0,28'),
(28, 6, '0,28'),
(44, 6, '0,28'),
(12, 7, '0,19'),
(28, 7, '0,19'),
(44, 7, '0,19'),
(13, 4, '0,020'),
(29, 4, '0,020'),
(45, 4, '0,020'),
(13, 6, '0,09'),
(29, 6, '0,09'),
(45, 6, '0,09'),
(13, 7, '0,09'),
(29, 7, '0,09'),
(45, 7, '0,09'),
(14, 4, '0,024'),
(30, 4, '0,024'),
(46, 4, '0,024'),
(14, 6, '0,16'),
(30, 6, '0,16'),
(46, 6, '0,16'),
(14, 7, '0,13'),
(30, 7, '0,13'),
(46, 7, '0,13'),
(15, 4, '0,024'),
(31, 4, '0,024'),
(47, 4, '0,024'),
(15, 6, '0,3'),
(31, 6, '0,3'),
(47, 6, '0,3'),
(15, 7, '0,19'),
(31, 7, '0,19'),
(47, 7, '0,19'),
(16, 4, '0,024'),
(32, 4, '0,024'),
(48, 4, '0,024'),
(16, 6, '0,35'),
(32, 6, '0,35'),
(48, 6, '0,35'),
(16, 7, '0,19'),
(32, 7, '0,19'),
(48, 7, '0,19');

CREATE TABLE tb_matriz_beneficio_rentabilidade (
	id_matriz_beneficio_rentabilidade int unsigned not null auto_increment,
	id_matriz int unsigned not null,
	id_beneficio_rentabilidade int unsigned not null,
	valor BIT NOT NULL,
	dt_inclusao timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	dt_alteracao timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	constraint pk_matriz_beneficio_rentabilidade primary key (id_matriz_beneficio_rentabilidade),
	constraint fk_matrizbeneficiorentabilidade__matriz foreign key (id_matriz) references tb_matriz (id_matriz),
	constraint fk_matrizbeneficiorentabilidade__beneficiorentabilidade foreign key (id_beneficio_rentabilidade) references tb_beneficio_rentabilidade (id_beneficio_rentabilidade)
);

INSERT INTO tb_matriz_beneficio_rentabilidade
(id_matriz, id_beneficio_rentabilidade, valor)
VALUES
(1, 1, 1),
(17, 1, 1),
(33, 1, 1),
(1, 2, 1),
(17, 2, 1),
(33, 2, 1),
(1, 3, 1),
(17, 3, 1),
(33, 3, 1),
(2, 1, 0),
(18, 1, 0),
(34, 1, 0),
(2, 2, 0),
(18, 2, 0),
(34, 2, 0),
(2, 3, 0),
(18, 3, 0),
(34, 3, 0),
(3, 1, 0),
(19, 1, 0),
(35, 1, 0),
(3, 2, 0),
(19, 2, 0),
(35, 2, 0),
(3, 3, 0),
(19, 3, 0),
(35, 3, 0),
(4, 1, 1),
(20, 1, 1),
(36, 1, 1),
(4, 2, 1),
(20, 2, 1),
(36, 2, 1),
(4, 3, 1),
(20, 3, 1),
(36, 3, 1),
(5, 1, 1),
(21, 1, 1),
(37, 1, 1),
(5, 2, 1),
(21, 2, 1),
(37, 2, 1),
(5, 3, 1),
(21, 3, 1),
(37, 3, 1),
(6, 1, 1),
(22, 1, 1),
(38, 1, 1),
(6, 2, 1),
(22, 2, 0),
(38, 2, 0),
(6, 3, 0),
(22, 3, 0),
(38, 3, 0),
(7, 1, 0),
(23, 1, 0),
(39, 1, 0),
(7, 2, 0),
(23, 2, 0),
(39, 2, 0),
(7, 3, 0),
(23, 3, 0),
(39, 3, 1),
(8, 1, 1),
(24, 1, 1),
(40, 1, 1),
(8, 2, 1),
(24, 2, 1),
(40, 2, 0),
(8, 3, 0),
(24, 3, 0),
(40, 3, 0),
(9, 1, 0),
(25, 1, 0),
(41, 1, 0),
(9, 2, 0),
(25, 2, 0),
(41, 2, 0),
(9, 3, 0),
(25, 3, 0),
(41, 3, 0),
(10, 1, 0),
(26, 1, 0),
(42, 1, 0),
(10, 2, 0),
(26, 2, 0),
(42, 2, 0),
(10, 3, 0),
(26, 3, 0),
(42, 3, 0),
(11, 1, 0),
(27, 1, 0),
(43, 1, 0),
(11, 2, 1),
(27, 2, 1),
(43, 2, 1),
(11, 3, 1),
(27, 3, 1),
(43, 3, 1),
(12, 1, 1),
(28, 1, 1),
(44, 1, 1),
(12, 2, 1),
(28, 2, 1),
(44, 2, 1),
(12, 3, 1),
(28, 3, 1),
(44, 3, 1),
(13, 1, 1),
(29, 1, 1),
(45, 1, 1),
(13, 2, 1),
(29, 2, 1),
(45, 2, 1),
(13, 3, 1),
(29, 3, 1),
(45, 3, 1),
(14, 1, 1),
(30, 1, 1),
(46, 1, 1),
(14, 2, 1),
(30, 2, 1),
(46, 2, 1),
(14, 3, 1),
(30, 3, 1),
(46, 3, 1),
(15, 1, 1),
(31, 1, 1),
(47, 1, 1),
(15, 2, 1),
(31, 2, 1),
(47, 2, 1),
(15, 3, 1),
(31, 3, 1),
(47, 3, 1),
(16, 1, 1),
(32, 1, 1),
(48, 1, 1),
(16, 2, 1),
(32, 2, 1),
(48, 2, 1),
(16, 3, 1),
(32, 3, 1),
(48, 3, 1);


