﻿using maisfidelidade.cnc.fo.api.core.Configuration;
using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.RegularExpressions;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Usuarios
{
    public class PreCadastro : EntityBase, IAggregateRoot
    {
        public string CNPJ { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        public string ConfirmaSenha { get; set; }

        private readonly Regex regexNonDigits = new Regex(@"[^\d]+");
        private readonly Regex regexEmail = new Regex(@"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$");

        public bool IsValid()
        {
            if (CNPJIsValid() && EmailIsValid() &&
                PasswordIsValid() && PasswordConfirmIsValid() && ComparePassword())
                return true;

            return false;
        }

        public bool CNPJIsValid()
        {
            if (!string.IsNullOrEmpty(this.CNPJ))
            {
                this.CNPJ = regexNonDigits.Replace(this.CNPJ, "");

                if (IsCnpj())
                    return true;
            }

            return false;
        }

        public bool EmailIsValid()
        {
            if (!string.IsNullOrEmpty(this.Email))
            {
                if (regexEmail.IsMatch(this.Email))
                    return true;
            }

            return false;
        }

        public bool PasswordIsValid()
        {
            if (!string.IsNullOrEmpty(this.Senha))
            {
                if (this.Senha.Length == 6)
                    return true;
            }

            return false;
        }

        public bool PasswordConfirmIsValid()
        {
            if (!string.IsNullOrEmpty(this.ConfirmaSenha))
            {
                if (this.ConfirmaSenha.Length == 6)
                    return true;
            }

            return false;
        }

        public bool ComparePassword()
        {
            return string.Equals(this.Senha, this.ConfirmaSenha);
        }

        public bool IsCnpj()
        {
            var multiplicador1 = new int[12] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            var multiplicador2 = new int[13] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int soma;
            int resto;
            string digito;
            string tempCnpj;
            this.CNPJ = this.CNPJ.Trim();
            this.CNPJ = this.CNPJ.Replace(".", "").Replace("-", "").Replace("/", "");
            if (this.CNPJ.Length != 14)
                return false;
            tempCnpj = this.CNPJ.Substring(0, 12);
            soma = 0;
            for (int i = 0; i < 12; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador1[i];
            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = resto.ToString();
            tempCnpj = tempCnpj + digito;
            soma = 0;
            for (int i = 0; i < 13; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador2[i];
            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = digito + resto.ToString();
            return this.CNPJ.EndsWith(digito);
        }

        public string GerarToken(PreCadastroEnum status)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(ConfigurationManager.AppSettings["JWT:Key"]);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(JwtRegisteredClaimNames.UniqueName, this.Email),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        new Claim("Status", status.ToString())
                    }),
                Expires = DateTime.UtcNow.AddMinutes(5),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
    }
}
