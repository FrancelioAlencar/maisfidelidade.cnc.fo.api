﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.RepositoryFramework;
using System;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Usuarios
{
    public interface ILogAcessoRepositorio : IRepositorio<LogAcesso>
    {
        void SalvaRegistro(int idUsuario, int idPerfil);

        DateTime ObterUltimoAcesso(int idUsuario);
    }
}