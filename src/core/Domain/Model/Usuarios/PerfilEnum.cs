﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Usuarios
{
    public enum PerfilEnum
    {
        Administrador = 1,
        GC = 2,
        GR = 3,
        Lojista = 4
    }
}
