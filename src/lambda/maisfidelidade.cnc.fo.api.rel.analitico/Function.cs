using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using maisfidelidade.cnc.fo.api.core.Common.AWS;
using maisfidelidade.cnc.fo.api.core.Common.DependencyInjection;
using maisfidelidade.cnc.fo.api.core.Domain.Model;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Empresas;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Relatorios;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Usuarios;
using maisfidelidade.cnc.fo.api.core.Log;
using Newtonsoft.Json;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace maisfidelidade.cnc.fo.api.rel.analitico
{
    public class Function : FunctionBase
    {
        private readonly IAnaliticoRepositorio analiticoRepositorio;

        public Function()
        {
            Logger.Log($"Resolve Inje��o de Dependencia");

            var dependencyResolver = new DependencyResolver();

            analiticoRepositorio = dependencyResolver.ObterServico<IAnaliticoRepositorio>();
        }

        public APIGatewayProxyResponse FunctionHandler(APIGatewayProxyRequest request, ILambdaContext context)
        {
            try
            {
                Logger.Log($"Valida token");
                var token = ValidaToken(request);

                Logger.Log($"Deserializa body {request.Body} para Filtro");
                var requestBody = new Filtro();
                if (request.Body != null)
                    requestBody = JsonConvert.DeserializeObject<Filtro>(request.Body);

                if (requestBody.Exportacao)
                {
                    Logger.Log($"Busca informa��es para exporta��o");
                    IList<Analitico> listaExportar = null;

                    if (token.Perfil.Id.Equals((int)PerfilEnum.Lojista))
                        listaExportar = analiticoRepositorio.ObterLojistaExportacao(token.IdEmpresa, requestBody);
                    else
                        listaExportar = analiticoRepositorio.ObterComercialExportacao(token.IdUsuario, requestBody, token.Perfil.Id);

                    LogMessage(context, "Solicita��o de processamento bem-sucedida.");

                    return CreateResponse(new
                    {
                        success = true,
                        data = listaExportar
                    });
                }

                Logger.Log($"Obtem quantidade de registros e p�ginas");
                Paginacao paginacao = null;
                
                if (token.Perfil.Id.Equals((int)PerfilEnum.Lojista))
                    paginacao = analiticoRepositorio.ObterPaginacaoLojista(requestBody, token.IdEmpresa);
                else
                    paginacao = analiticoRepositorio.ObterPaginacao(requestBody, token.IdUsuario, token.Perfil.Id);

                if (requestBody.Pagina == 0)
                    requestBody.Pagina = 1;

                if (requestBody.Pagina < 1 || requestBody.Pagina > paginacao.QuantidadePaginas)
                {
                    Logger.Log($"A p�gina solicitada n�o foi localizada");
                    return CreateResponse(new
                    {
                        success = false,
                        message = $"A p�gina que voc� solicitou n�o foi localizada."
                    });
                }

                requestBody.Offset = (requestBody.Pagina * 10) - 10;

                IList<Analitico> lista = null;

                if (token.Perfil.Id.Equals((int)PerfilEnum.Lojista))
                {
                    Logger.Log($"Busca informa��es do relat�rio lojista");
                    lista = analiticoRepositorio.ObterLojista(token.IdEmpresa, requestBody);
                }
                else
                {
                    Logger.Log($"Busca informa��es do relat�rio comercial");
                    lista = analiticoRepositorio.ObterComercial(token.IdUsuario, requestBody, token.Perfil.Id);
                }

                if (requestBody.MarketShare != null)
                    lista.Select(x => x.MarketShare.Contains(requestBody.MarketShare));

                if (requestBody.IPS != null)
                    lista.Select(x => x.IPS.Contains(requestBody.IPS));

                LogMessage(context, "Solicita��o de processamento bem-sucedida.");

                return CreateResponse(new
                {
                    success = true,
                    current_page = requestBody.Pagina,
                    total_pages = paginacao.QuantidadePaginas,
                    total_records = paginacao.TotalRegistros,
                    data = lista
                });
            }
            catch (Exception _exception)
            {
                LogMessage(context, string.Format("Falha na solicita��o de processamento - {0}", _exception.Message));
                Logger.Log($"Error ao buscar relat�rio anal�tico: error ::{_exception.Message} - StackTrace:: {_exception.StackTrace} ");
                _exception.Data.Add("LambdaContext", JsonConvert.SerializeObject(context));
                return CreateResponse(new
                {
                    success = false,
                    message = $"Ocorreu um erro ao tentar processar sua solicita��o.<br/>Tente novamente em instantes. Se o erro persistir, fale com {PessoaContato()}."
                });
            }
        }
    }
}