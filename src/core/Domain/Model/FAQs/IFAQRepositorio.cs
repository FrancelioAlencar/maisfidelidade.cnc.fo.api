﻿using System.Collections;
using System.Collections.Generic;
using maisfidelidade.cnc.fo.api.core.Domain.Model.FAQs;
using maisfidelidade.cnc.fo.api.core.Infrastructure;
using maisfidelidade.cnc.fo.api.core.Infrastructure.RepositoryFramework;

namespace maisfidelidade.cnc.fo.api.core.Domain
{
    public interface IFAQRepositorio : IRepositorio<FAQ>
    {
        IList<FAQ> BuscarFAQ();
        Template BuscarTemplate();
    }
}