﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Mensagens
{
    public enum EmailEnum
    {
        RecuperarSenha = 1,
        SenhaAlterada = 2
    }
}