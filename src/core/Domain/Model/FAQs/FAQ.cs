﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using Newtonsoft.Json;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.FAQs
{
    /// <summary>
    ///
    /// </summary>
    public class FAQ : EntityBase, IAggregateRoot
    {
        [JsonProperty("question")]
        public string Pergunta { get; set; }

        [JsonProperty("answer")]
        public string Resposta { get; set; }
    }
}