﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Relatorios
{
    public class FiltroDetalhe
    {
        [JsonProperty("managers")]
        public IList<FiltroGerente> Gerente { get; set; }

        [JsonProperty("segments")]
        public IList<FiltroSegmento> Segmento { get; set; }

        [JsonProperty("dates")]
        public IList<DateTime> Data { get; set; }

        [JsonProperty("group_names")]
        public IList<string> Grupo { get; set; }
    }

    public class FiltroGerente
    {
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Nome { get; set; }
    }

    public class FiltroSegmento
    {
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Nome { get; set; }
    }
}
