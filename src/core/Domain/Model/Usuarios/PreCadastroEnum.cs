﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Usuarios
{
    public enum PreCadastroEnum
    {
        Erro = 0,
        CNPJ = 1,
        Email = 2,
        Senha = 3,
        RealizaCadastro = 4
    }
}
