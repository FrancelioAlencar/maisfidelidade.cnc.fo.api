﻿using maisfidelidade.cnc.fo.api.core.Domain.Model.Empresas;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Usuarios
{
    public class Token
    {
        public int IdUsuario { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public Perfil Perfil { get; set; }
        public bool TrocaSenha { get; set; }

        public int IdEmpresa { get; set; }
        public IList<EmpresaResumido> Empresas { get; set; }

        public string UniqueName { get; set; }
        public string Jti { get; set; }
    }
}
