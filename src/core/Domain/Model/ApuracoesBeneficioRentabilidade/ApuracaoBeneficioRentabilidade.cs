﻿using maisfidelidade.cnc.fo.api.core.Common.AWS;
using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using Newtonsoft.Json;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.ApuracoesBeneficioRentabilidade
{
    /// <summary>
    ///
    /// </summary>
    public class ApuracaoBeneficioRentabilidade : EntityBase, IAggregateRoot
    {
        [JsonProperty("name")]
        public string Nome { get; set; }

        [JsonProperty("value")]
        public int Valor { get; set; }
    }
}