﻿using Amazon.Lambda.Core;
using maisfidelidade.cnc.fo.api.core.Configuration;
//using maisfidelidade.cnc.fo.api.importacao.core.DI;
//using maisfidelidade.cnc.fo.api.importacao.core.Domain.Model.Enums;
//using maisfidelidade.cnc.fo.api.importacao.core.Domain.Repositories.MongoDB;
//using maisfidelidade.cnc.fo.api.importacao.core.Log;
using MongoDB.Bson;
using System;
using System.Collections.Generic;

namespace maisfidelidade.cnc.fo.api.core.Log
{
	public class Logger
	//public class Logger : ILogger
	{
		//private readonly IMongoRepositorio<LogInfo> logMongoRepository;

		//public Logger(IMongoRepositorio<LogInfo> logMongoRepository)
		//{
		//	this.logMongoRepository = logMongoRepository;
		//}
		public static ILambdaContext _lambdaContext { get; set; }

		public static void Log(string message, params object[] args)
		{
			if (string.IsNullOrWhiteSpace(message)) message = "Empty";
			if (ConfigurationManager.AppSettings["Debug"].To<bool>(true))
			{
				if (_lambdaContext != null)
				{
					if (args == null || args.Length == 0)
						_lambdaContext.Logger.Log(message);
					else
						_lambdaContext.Logger.Log(string.Format(message, args));
				}
				else
				{
					if (args == null || args.Length == 0)
						Console.WriteLine(message);
					else
						Console.WriteLine(message, args);
				}
			}
		}

		public void Log(LogInfo logInfo)
		{

		}

		//public void Log(ObjectId? idArquivo, StatusAndamentoProcesso statusAndamentoProcesso, LogWatcher logWatcher, TipoLog tipoLog, string mensagem)
		//{
		//	logWatcher.Parar();

		//	var logInfo = new LogInfo
		//	{
		//		IdArquivo = idArquivo,
		//		StatusAndamentoProcesso = statusAndamentoProcesso,
		//		DataInicio = logWatcher.Inicio,
		//		DataFim = logWatcher.Termino,
		//		TempoDecorrido = logWatcher.TempoDecorrido,
		//		Tipo = tipoLog,
		//		Mensagem = mensagem,
		//		StackTrace = string.Empty,
		//	};

		//	logMongoRepository.Salvar(logInfo);
		//}

		public void LogException(Exception exception, LogWatcher logWatcher)
		{
			logWatcher.Parar();

			Log(exception.ToString());
		}

		public static void LogForce(string message, params object[] args)
		{
			if (string.IsNullOrWhiteSpace(message)) message = "Empty";
			if (_lambdaContext != null)
			{
				if (args == null || args.Length == 0)
					_lambdaContext.Logger.Log(message);
				else
					_lambdaContext.Logger.Log(string.Format(message, args));
			}
			else
			{
				if (args == null || args.Length == 0)
					Console.WriteLine(message, args);
				else
					Console.WriteLine(message);
			}
		}

		//public void Log(ObjectId? idArquivo, StatusAndamentoProcesso statusAndamentoProcesso, LogWatcher logWatcher, TipoLog tipoLog, string mensagem, IList<LogDetalhe> detalhes)
		//{
		//	logWatcher.Parar();

		//	var logInfo = new LogInfo
		//	{
		//		IdArquivo = idArquivo,
		//		StatusAndamentoProcesso = statusAndamentoProcesso,
		//		DataInicio = logWatcher.Inicio,
		//		DataFim = logWatcher.Termino,
		//		TempoDecorrido = logWatcher.TempoDecorrido,
		//		Tipo = tipoLog,
		//		Mensagem = mensagem,
		//		StackTrace = string.Empty,
		//		Detalhes = detalhes
		//	};

		//	logMongoRepository.Salvar(logInfo);
		//}
	}
}
