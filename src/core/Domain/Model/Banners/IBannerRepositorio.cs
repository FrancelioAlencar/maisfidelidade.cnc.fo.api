﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.RepositoryFramework;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Banners
{
    public interface IBannerRepositorio : IRepositorio<Banner>
    {
        IList<Banner> Obter(int idEmpresa);
    }
}
