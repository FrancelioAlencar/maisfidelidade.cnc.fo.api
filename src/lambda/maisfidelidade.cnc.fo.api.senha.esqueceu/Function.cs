using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Amazon;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using maisfidelidade.cnc.fo.api.core.Common.AWS;
using maisfidelidade.cnc.fo.api.core.Common.DependencyInjection;
using maisfidelidade.cnc.fo.api.core.Configuration;
using maisfidelidade.cnc.fo.api.core.Domain.Model;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Empresas;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Mensagens;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Usuarios;
using maisfidelidade.cnc.fo.api.core.Log;
using Newtonsoft.Json;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace maisfidelidade.cnc.fo.api.senha.esqueceu
{
    public class Function : FunctionBase
    {
        private readonly IUsuarioRepositorio usuarioRepositorio;
        private readonly IEmailTemplateRepositorio emailTemplateRepositorio;
        private readonly ILogSenhaRepositorio logSenhaRepositorio;
        private readonly IEmpresaRepositorio empresaRepositorio;

        public Function()
        {
            Logger.Log($"Resolve Inje��o de Dependencia");

            var dependencyResolver = new DependencyResolver();

            usuarioRepositorio = dependencyResolver.ObterServico<IUsuarioRepositorio>();
            emailTemplateRepositorio = dependencyResolver.ObterServico<IEmailTemplateRepositorio>();
            logSenhaRepositorio = dependencyResolver.ObterServico<ILogSenhaRepositorio>();
            empresaRepositorio = dependencyResolver.ObterServico<IEmpresaRepositorio>();
        }

        public async Task<APIGatewayProxyResponse> FunctionHandler(APIGatewayProxyRequest request, ILambdaContext context)
        {
            try
            {
                Logger.Log($"Deserializa {request.Body} para Usuario");
                var requestBody = JsonConvert.DeserializeObject<Usuario>(request.Body);

                if (string.IsNullOrEmpty(requestBody.Email))
                {
                    return CreateResponse(new
                    {
                        success = false,
                        message = $"O endere�o de email precisa ser preenchido."
                    });
                }

                Logger.Log($"Busca informa��es do usu�rio");
                Usuario usuario = new Usuario();
                usuario = usuarioRepositorio.ObterPorEmail(requestBody.Email);

                if (usuario == null)
                {
                    Logger.Log($"Usu�rio {requestBody.Email} veio nulo");
                    throw new Exception($"Usu�rio {requestBody.Email} n�o localizado");
                }

                Logger.Log($"Seta id_usuario: {usuario.Id}");

                if (!usuario.Ativo)
                {
                    Logger.Log($"Usu�rio {requestBody.Email} esta inativo");
                    throw new Exception($"Usu�rio {requestBody.Id} esta inativo");
                }

                Logger.Log($"Obtem empresa por Id {usuario.IdEmpresa}");
                Empresa empresa = empresaRepositorio.ObterPorId(usuario.IdEmpresa);

                if (empresa == null)
                {
                    Logger.Log($"Empresa {usuario.IdEmpresa} veio nulo");
                    throw new Exception($"Empresa {usuario.IdEmpresa} n�o localizado");
                }

                if (!empresa.Ativo)
                {
                    Logger.Log($"Empresa {usuario.IdEmpresa} esta inativa");
                    throw new Exception($"Empresa {usuario.IdEmpresa} esta inativa");
                }

                Logger.Log($"Prepara nova senha");
                var senhaAntiga = usuario.Senha;
                var novaSenha = usuario.GerarNovaSenha();
                var newPasswordHash = usuario.Encrypt(novaSenha);
                usuario.Senha = newPasswordHash;
                usuario.TrocarSenha = true;

                Logger.Log("Troca senha");
                usuarioRepositorio.TrocaSenha(usuario);

                Logger.Log($"Salva registro de log");
                logSenhaRepositorio.SalvaRegistro(new LogSenha(usuario.Id, senhaAntiga, true));

                //Logger.Log($"Prepara mensagem disparo de e-mail");
                //var mensagem = new Mensagem();
                //mensagem.EmailDestinatario = usuario.Email;

                //Logger.Log($"Busca template de email para recupera��o de senha: {(int)EmailEnum.RecuperarSenha}");
                //var configuracaoEmail = emailTemplateRepositorio.ObterPorId((int)EmailEnum.RecuperarSenha);
                //mensagem.Assunto = configuracaoEmail.Titulo;
                //mensagem.Corpo = string.Format(configuracaoEmail.Mensagem, novaSenha);

                //Logger.Log($"Envia email de recuperar senha");
                //await EnviarEmailAsync(mensagem);

                LogMessage(context, "Solicita��o de processamento bem-sucedida.");

                return CreateResponse(new { success = true });
            }
            catch (Exception _exception)
            {
                LogMessage(context, string.Format("Falha na solicita��o de processamento - {0}", _exception.Message));
                Logger.Log($"Error ao recuperar senha: error ::{_exception.Message} - StackTrace:: {_exception.StackTrace} ");
                _exception.Data.Add("LambdaContext", JsonConvert.SerializeObject(context));
                return CreateResponse(new
                {
                    success = false,
                    message = $"Ocorreu um erro ao tentar processar sua solicita��o.<br/>Tente novamente em instantes. Se o erro persistir, fale com {PessoaContato()}."
                });
            }
        }

        public async Task<bool> EnviarEmailAsync(Mensagem mensagem)
        {
            try
            {
                Logger.Log($"BORA DE EMAIL");
                var destinatario = new Destination(new List<string> { mensagem.EmailDestinatario });
                var assunto = new Content(mensagem.Assunto);
                var corpo = new Body(new Content(mensagem.Corpo));

                Logger.Log($"Monta mensagem");
                var mensagemSES = new Message(assunto, corpo);

                Logger.Log($"Cria requisi��o com o source {ConfigurationManager.AppSettings["EnvioEmail:emailRemetente"]}");
                var requisicaoEnvio = new SendEmailRequest(
                    source: ConfigurationManager.AppSettings["EnvioEmail:emailRemetente"],
                    destination: destinatario,
                    message: mensagemSES);

                SendEmailResponse resposta;
                Logger.Log($"instancia cliente na regi�o {ConfigurationManager.AppSettings["EnvioEmail:regiao"]}");
                using (var client = new AmazonSimpleEmailServiceClient(RegionEndpoint.GetBySystemName(ConfigurationManager.AppSettings["EnvioEmail:regiao"])))
                {
                    Logger.Log($"cliente send email async {client}");
                    resposta = await client.SendEmailAsync(requisicaoEnvio);
                }

                Logger.Log($"retorno email {resposta.HttpStatusCode}");
                if (resposta.HttpStatusCode != System.Net.HttpStatusCode.OK)
                    return false;

                return true;
            }
            catch (Exception error)
            {
                Logger.Log($"EnviarEmailAsync {error.Message}");
                throw new Exception($"EnviarEmailAsync {error.Message}");
            }
        }
    }
}