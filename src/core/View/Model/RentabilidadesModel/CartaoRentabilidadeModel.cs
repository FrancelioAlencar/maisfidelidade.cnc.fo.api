﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace maisfidelidade.cnc.fo.api.core.View.Model.RentabilidadesModel
{
    public class CartaoRentabilidadeModel
    {
        [JsonProperty("name")]
        public string Nome { get; set; }
        
        [JsonProperty("value")]
        public object Valor { get; set; }
        
        [JsonProperty("type")]
        public int Tipo { get; set; }

    }

}