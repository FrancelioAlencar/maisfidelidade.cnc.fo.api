﻿using System.Collections;
using System.Collections.Generic;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Arquivos;
using maisfidelidade.cnc.fo.api.core.Infrastructure;
using maisfidelidade.cnc.fo.api.core.Infrastructure.RepositoryFramework;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Arquivos
{
    public interface ArquivoRepositorio : IRepositorio<Arquivo>
    {
		IList<Arquivo> BuscarArquivo();
    }
}
