﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Mensagens
{
    public class EmailTemplate : EntityBase, IAggregateRoot
    {
        public string Titulo { get; set; }
        public string Mensagem { get; set; }
        public bool Ativo { get; set; }
        public DateTime Alteracao { get; set; }
    }
}
