﻿using maisfidelidade.cnc.fo.api.core.Domain.Model.Mensagens;
using maisfidelidade.cnc.fo.api.core.Log;
using System;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Infrastructure.Repositories
{
    class EmailTemplateRepositorio : MySqlRepositorioBase<EmailTemplate>, IEmailTemplateRepositorio
    {
        public EmailTemplateRepositorio()
        {

        }

        public EmailTemplate ObterPorId(int id)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("SELECT id_configuracao_email AS Id, titulo AS Titulo, mensagem AS Mensagem, fl_ativo AS Ativo, dt_alteracao AS Alteracao ");
                query.Append(" FROM tb_configuracao_emails ");
                query.AppendFormat(" WHERE id_configuracao_email = {0}", id);

                Logger.Log($"EmailTemplateRepositorio > ObterPorId Query MySql: {query.ToString()}");

                return this.BuildEntityFromSql(query.ToString());
            }
            catch (Exception error)
            {
                throw new Exception($"EmailTemplateRepositorio > ObterPorId: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        protected override void BuildChildCallbacks()
        {

        }


        protected override void PersistDeletedItem(EmailTemplate item)
        {
            throw new NotImplementedException();
        }

        protected override void PersistNewItem(EmailTemplate item)
        {
            throw new NotImplementedException();
        }

        protected override void PersistUpdatedItem(EmailTemplate item)
        {
            throw new NotImplementedException();
        }
    }
}
