﻿using maisfidelidade.cnc.fo.api.core.Domain.Model.MatrizesIndicadoresRentabilidade;
using maisfidelidade.cnc.fo.api.core.Domain.Model.MatrizesBeneficiosRentabilidade;
using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Matrizes
{
    /// <summary>
    ///
    /// </summary>
    public class Matriz : EntityBase, IAggregateRoot
    {
        public Matriz()
        {
            MatrizIndicadorRentabilidade = new List<MatrizIndicadorRentabilidade>();
            MatrizBeneficioRentabilidade = new List<MatrizBeneficioRentabilidade>();
        }

        [JsonProperty("matrixId")]
        public int IdMatriz { get; set; }

        [JsonProperty("idVerticalZone")]
        public int IdFaixaVertical { get; set; }

        [JsonProperty("verticalZone")]
        public string FaixaVertical { get; set; }

        [JsonProperty("verticalPercentFrom")]
        public string PorcentagemDeVertical { get; set; }

        [JsonProperty("verticalPercentTo")]
        public string PorcentagemAteVertical { get; set; }

        [JsonProperty("idHorizontalZone")]
        public int IdFaixaHorizontal { get; set; }

        [JsonProperty("horizontalZone")]
        public string FaixaHorizontal { get; set; }

        [JsonProperty("horizontalPercentFrom")]
        public string PorcentagemDeHorizontal { get; set; }

        [JsonProperty("horizontalPercentTo")]
        public string PorcentagemAteHorizontal { get; set; }

        [JsonProperty("category")]
        public string Categoria { get; set; }

        [JsonProperty("value")]
        public decimal Valor { get; set; }

        [JsonProperty("position")]
        public int Posicao { get; set; }

        [JsonProperty("indexMatrix")]
        public IList<MatrizIndicadorRentabilidade> MatrizIndicadorRentabilidade { get; set; }

        [JsonProperty("benefitsMatrix")]
        public IList<MatrizBeneficioRentabilidade> MatrizBeneficioRentabilidade { get; set; }
    }
}