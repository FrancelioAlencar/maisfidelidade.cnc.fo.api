﻿using maisfidelidade.cnc.fo.api.core.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using maisfidelidade.cnc.fo.api.core.Log;
using Amazon.SQS.Model;
using maisfidelidade.cnc.fo.api.core.Domain.Model.ApuracoesBeneficioRentabilidade;

namespace maisfidelidade.cnc.fo.api.core.Infrastructure.Repositories
{
	public class ApuracaoBeneficioRentabilidadeRepositorio : MySqlRepositorioBase<ApuracaoBeneficioRentabilidade>, IApuracaoBeneficioRentabilidadeRepositorio
	{
		public ApuracaoBeneficioRentabilidadeRepositorio()
		{

		}
		public IList<ApuracaoBeneficioRentabilidade> BuscarApuracaoBeneficioRentabilidade(int idEmpresa)
		{
			var builder = new StringBuilder();
			builder.Append("SELECT br.nome as Nome, ");
			builder.Append(" abr.valor as Valor ");
			builder.Append(" FROM tb_beneficio_rentabilidade br ");
			builder.Append(" INNER JOIN tb_apuracao_beneficio_rentabilidade abr ");
			builder.Append(" ON br.id_beneficio_rentabilidade = abr.id_beneficio_rentabilidade ");
			builder.Append(" INNER JOIN tb_apuracao AS A ON A.id_apuracao = abr.id_apuracao");
			builder.Append($" WHERE id_empresa = {idEmpresa}");
			builder.Append(" ORDER BY ");
			builder.Append(" abr.id_apuracao_beneficio_rentabilidade ASC ");

			Logger.Log($"Busca Matriz: {builder.ToString()}");

			return this.BuildEntitiesFromSql(builder.ToString());
		}
		protected override void BuildChildCallbacks()
		{

		}

		protected override void PersistDeletedItem(ApuracaoBeneficioRentabilidade item)
		{
			throw new NotImplementedException();
		}

		protected override void PersistNewItem(ApuracaoBeneficioRentabilidade item)
		{
			throw new NotImplementedException();
		}

		protected override void PersistUpdatedItem(ApuracaoBeneficioRentabilidade item)
		{
			throw new NotImplementedException();
		}
	}
}
