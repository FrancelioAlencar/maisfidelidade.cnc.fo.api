﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace maisfidelidade.cnc.fo.api.core.View.Model.RentabilidadesModel
{
    public class TotalRentabilidadeModel
    {
        [JsonProperty("max")]
        public decimal Maximo { get; set; }

        [JsonProperty("accomplished")]
        public decimal Realizado { get; set; }
    }

}