using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using Newtonsoft.Json;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Apuracoes
{
    /// <summary>
    ///
    /// </summary>
    public class Apuracao : EntityBase, IAggregateRoot
    {
        [JsonProperty("cnpj")]
        public string Cnpj { get; set; }

        [JsonProperty("segmentId")]
        public int IdSegmento { get; set; }

        [JsonProperty("santanderFinancing")]
        public decimal FinanciadoSantander { get; set; }

        [JsonProperty("storeFinancing")]
        public decimal FinanciadoLoja { get; set; }

        [JsonProperty("contractsAmount")]
        public int QtdContrato { get; set; }

        [JsonProperty("lendersAmount")]
        public int QtdPrestamista { get; set; }

        [JsonProperty("carsAmount")]
        public int QtdAuto { get; set; }

        [JsonProperty("bothAmount")]
        public int QtdAmbos { get; set; }
    }
}