﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace maisfidelidade.cnc.fo.api.core.View.Model.RentabilidadesModel
{
    public class ValorRentabilidadeModel
    {
        [JsonProperty("name")]
        public string Nome { get; set; }

        [JsonProperty("max")]
        public decimal Maximo { get; set; }

        [JsonProperty("accomplished")]
        public decimal Realizado { get; set; }

        [JsonProperty("type")]
        public int Tipo { get; set; }
    }

}