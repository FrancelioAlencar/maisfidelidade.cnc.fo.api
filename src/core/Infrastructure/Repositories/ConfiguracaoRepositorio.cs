﻿using maisfidelidade.cnc.fo.api.core.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace maisfidelidade.cnc.fo.api.core.Infrastructure.Repositories
{
    public class ConfiguracaoRepositorio : MySqlRepositorioBase<Configuracao>, IConfiguracaoRepositorio
    {
        public ConfiguracaoRepositorio()
        {

        }

        public IList<Configuracao> ObterHubConfig()
        {
            var builder = new StringBuilder();
            builder.Append("select chave AS Tipo, valor AS Valor from autos_tb_configuracao where chave in ('hub_numero_tentativas', 'hub_tempo_limite_ms');");

            return this.ExecuteQuery<Configuracao>(builder.ToString());
        }

        protected override void BuildChildCallbacks()
        {

        }

        protected override void PersistDeletedItem(Configuracao item)
        {
            throw new NotImplementedException();
        }

        protected override void PersistNewItem(Configuracao item)
        {
            throw new NotImplementedException();
        }

        protected override void PersistUpdatedItem(Configuracao item)
        {
            throw new NotImplementedException();
        }
    }
}
