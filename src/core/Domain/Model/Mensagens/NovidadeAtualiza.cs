﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Mensagens
{
    public class NovidadeAtualiza
    {
        // Este id novidade é a pk da tb_novidade_usuario (id_novidade_usuario)
        public int IdNovidade { get; set; }
        public bool Lida { get; set; }
        public bool Pin { get; set; }
    }
}
