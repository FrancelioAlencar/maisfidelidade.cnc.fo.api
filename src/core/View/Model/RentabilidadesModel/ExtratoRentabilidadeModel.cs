﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace maisfidelidade.cnc.fo.api.core.View.Model.RentabilidadesModel
{
    public class ExtratoRentabilidadeModel
    {
        public ExtratoRentabilidadeModel()
        {
            Valores = new List<ValorRentabilidadeModel>();
            Total = new TotalRentabilidadeModel();
            Beneficios = new List<BeneficioRentabilidadeModel>();  
        }

        [JsonProperty("values")]
        public List<ValorRentabilidadeModel> Valores { get; set; }
        
        [JsonProperty("total")]
        public TotalRentabilidadeModel Total { get; set; }
        
        [JsonProperty("benefits")]
        public List<BeneficioRentabilidadeModel> Beneficios { get; set; }

    }

}