using System;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using maisfidelidade.cnc.fo.api.core.Common.AWS;
using maisfidelidade.cnc.fo.api.core.Common.DependencyInjection;
using maisfidelidade.cnc.fo.api.core.Domain.Model;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Empresas;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Mensagens;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Usuarios;
using maisfidelidade.cnc.fo.api.core.Log;
using Newtonsoft.Json;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace maisfidelidade.cnc.fo.api.mensagens
{
    public class Function : FunctionBase
    {
        private readonly INovidadeRepositorio novidadeRepositorio;

        public Function()
        {
            Logger.Log($"Resolve Inje��o de Dependencia");

            var dependencyResolver = new DependencyResolver();

            novidadeRepositorio = dependencyResolver.ObterServico<INovidadeRepositorio>();
        }

        public APIGatewayProxyResponse FunctionHandler(APIGatewayProxyRequest request, ILambdaContext context)
        {
            try
            {
                Logger.Log("Valida token");
                var token = ValidaToken(request);

                Logger.Log($"Busca mensagens do usu�rio: {token.IdUsuario}");
                var lista = novidadeRepositorio.Obter(token.IdUsuario);

                LogMessage(context, "Solicita��o de processamento bem-sucedida.");

                return CreateResponse(new { success = true, data = lista });
            }
            catch (Exception _exception)
            {
                LogMessage(context, string.Format("Falha na solicita��o de processamento - {0}", _exception.Message));
                Logger.Log($"Error ao carregar mensagens: error ::{_exception.Message} - StackTrace:: {_exception.StackTrace} ");
                _exception.Data.Add("LambdaContext", JsonConvert.SerializeObject(context));
                return CreateResponse(new
                {
                    success = false,
                    message = $"Ocorreu um erro ao tentar processar sua solicita��o.<br/>Tente novamente em instantes. Se o erro persistir, fale com {PessoaContato()}."
                });
            }
        }
    }
}