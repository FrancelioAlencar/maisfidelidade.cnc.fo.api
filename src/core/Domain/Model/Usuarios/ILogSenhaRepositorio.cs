﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.RepositoryFramework;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Usuarios
{
    public interface ILogSenhaRepositorio : IRepositorio<LogSenha>
    {
        void SalvaRegistro(LogSenha log);
    }
}