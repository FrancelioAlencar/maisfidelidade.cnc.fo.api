﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using Newtonsoft.Json;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Arquivos
{
    /// <summary>
    ///
    /// </summary>
    public class Arquivo : EntityBase, IAggregateRoot
    {
        [JsonProperty("name")]
        public string Nome { get; set; }

        [JsonProperty("type")]
        public int Tipo { get; set; }

        [JsonProperty("lastProcessed")]
        public bool UltimoProcessado { get; set; }

        [JsonProperty("linesAmount")]
        public int TotalLinhas { get; set; }

        [JsonProperty("active")]
        public bool Ativo { get; set; }
    }
}