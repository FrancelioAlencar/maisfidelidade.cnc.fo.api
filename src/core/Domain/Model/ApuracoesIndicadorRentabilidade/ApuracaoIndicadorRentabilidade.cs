﻿using maisfidelidade.cnc.fo.api.core.Common.AWS;
using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using Newtonsoft.Json;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.ApuracoesIndicadorRentabilidade
{
    /// <summary>
    ///
    /// </summary>
    public class ApuracaoIndicadorRentabilidade : EntityBase, IAggregateRoot
    {
        [JsonProperty("id")]
        public int IdIndicadorRentabilidade { get; set; }

        [JsonProperty("idPai")]
        public int? IdIndicadorRentabilidadePai { get; set; }

        [JsonProperty("name")]
        public string Nome { get; set; }

        [JsonProperty("max")]
        public decimal Maximo { get; set; }

        [JsonProperty("accomplished")]
        public decimal Realizado { get; set; }

        [JsonProperty("type")]
        public int Tipo { get; set; }
    }
}