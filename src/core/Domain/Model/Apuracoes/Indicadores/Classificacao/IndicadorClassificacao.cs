﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Apuracoes.Indicadores.Classificacao
{
    public class IndicadorClassificacao
    {
        public int Id { get; set; }
        public string Valor { get; set; }
    }
}
