﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace maisfidelidade.cnc.fo.api.core.View.Model.SimuladoresModel
{
    public class ExtratoSimuladorModel
    {
        [JsonProperty("plus")]
        public string Plus { get; set; }

        [JsonProperty("retorno")]
        public string Retorno { get; set; }

        [JsonProperty("marketing")]
        public bool Marketing { get; set; }

        [JsonProperty("gravasme")]
        public bool Gravasme { get; set; }

        [JsonProperty("credito")]
        public bool Credito { get; set; }
    }
}