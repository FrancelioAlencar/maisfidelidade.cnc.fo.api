﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Indicadores.Performance
{
    public class IndicadorPerformanceRetorno
    {
        [JsonProperty("indicator")]
        public IndicadorPerformanceEnum Indicador { get; set; }

        [JsonProperty("name")]
        public string Nome { get; set; }

        [JsonProperty("description")]
        public string Descricao { get; set; }

        [JsonProperty("tooltip")]
        public string Tooltip { get; set; }

        [JsonProperty("value")]
        public string Valor { get; set; }

        [JsonProperty("indicators")]
        public IList<IndicadorPerformanceDado> Dados { get; set; }
    }
}
