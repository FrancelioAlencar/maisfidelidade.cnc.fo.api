using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using maisfidelidade.cnc.fo.api.core.Common.AWS;
using maisfidelidade.cnc.fo.api.core.Common.DependencyInjection;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Usuarios;
using System;
using Newtonsoft.Json;
using maisfidelidade.cnc.fo.api.core.Domain.Model;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Empresas;
using maisfidelidade.cnc.fo.api.core.Log;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace maisfidelidade.cnc.fo.api.cliente.precadastro
{
    public class Function : FunctionBase
    {
        private readonly IUsuarioRepositorio usuarioRepositorio;
        private readonly IEmpresaRepositorio empresaRepositorio;

        public Function()
        {
            Logger.Log($"Resolve Inje��o de Dependencia");

            var dependencyResolver = new DependencyResolver();

            usuarioRepositorio = dependencyResolver.ObterServico<IUsuarioRepositorio>();
            empresaRepositorio = dependencyResolver.ObterServico<IEmpresaRepositorio>();
        }

        public APIGatewayProxyResponse FunctionHandler(APIGatewayProxyRequest request, ILambdaContext context)
        {
            try
            {
                Logger.Log($"Deserializa {request.Body} para PreCadastro");
                var requestBody = JsonConvert.DeserializeObject<PreCadastro>(request.Body);

                if (requestBody == null)
                {
                    Logger.Log("Os dados informados n�o s�o v�lidos.");
                    throw new Exception($"Os dados informados n�o s�o v�lidos: {requestBody}");
                }

                //Logger.Log($"Checa dados da requisi��o para saber qual o tipo de valida��o");
                //var tipoVerificacao = TipoVerificacao(requestBody);

                //if (tipoVerificacao.Equals(PreCadastroEnum.Erro))
                //{
                //    Logger.Log("Os dados informados n�o s�o v�lidos.");
                //    return CreateResponse(new ResultadoRequisicao(false, "Os dados informados n�o s�o v�lidos.", requestBody));
                //}

                Logger.Log($"Valida os dados recebidos");
                if (!ValidaDados(requestBody, out string erro, out Empresa empresa, out Usuario usuario))
                {
                    Logger.Log(erro);
                    return CreateResponse(new
                    {
                        success = false,
                        message = erro
                    });
                }

                Logger.Log($"Realiza cadastro do email {requestBody.Email} na empresa {empresa.Id}");
                Usuario novoUsuario = new Usuario();
                novoUsuario.Ativo = true;
                novoUsuario.Email = requestBody.Email;
                Logger.Log("Prepara senha");
                novoUsuario.Senha = novoUsuario.Encrypt(requestBody.Senha);
                novoUsuario.TrocarSenha = false;
                novoUsuario.IdEmpresa = empresa.Id;

                Logger.Log("Cadastra usu�rio");
                novoUsuario = usuarioRepositorio.Cadastro(novoUsuario);

                if (novoUsuario.Id > 0)
                {
                    Logger.Log($"Associa perfil de lojista ({(int)PerfilEnum.Lojista} no usu�rio {novoUsuario.Id}");
                    usuarioRepositorio.AssociaPerfil(novoUsuario, PerfilEnum.Lojista);

                    Logger.Log($"Busca o usu�rio {novoUsuario.Id} para gera��o do token");
                    novoUsuario = usuarioRepositorio.ObterPorId(novoUsuario.Id);

                    LogMessage(context, "Solicita��o de processamento bem-sucedida.");

                    Logger.Log("Gera token de login");
                    var token = novoUsuario.GerarToken();

                    Logger.Log("Retorna token e objeto do usu�rio cadastrado");
                    return CreateResponse(new { success = true, Token = token });
                }
                else
                {
                    LogMessage(context, "Solicita��o de processamento com falha.");
                    Logger.Log("Problemas ao tentar cadastrar o usu�rio");
                    throw new Exception($"Problemas ao tentar cadastrar o usu�rio");
                }
            }
            catch (Exception _exception)
            {
                LogMessage(context, string.Format("Falha na solicita��o de processamento - {0}", _exception.Message));
                Logger.Log($"Error no pr�-cadastro: error ::{_exception.Message} - StackTrace:: {_exception.StackTrace} ");
                _exception.Data.Add("LambdaContext", JsonConvert.SerializeObject(context));
                return CreateResponse(new
                {
                    success = false,
                    message = $"Ocorreu um erro ao tentar processar sua solicita��o.<br/>Tente novamente em instantes. Se o erro persistir, fale com {PessoaContato()}."
                });
            }
        }

        public PreCadastroEnum TipoVerificacao(PreCadastro dados)
        {
            try
            {
                if (dados.ConfirmaSenha != null && dados.Senha != null && dados.Email != null && dados.CNPJ != null)
                    return PreCadastroEnum.RealizaCadastro;

                if (dados.Senha != null && dados.Email != null && dados.CNPJ != null)
                    return PreCadastroEnum.Senha;

                if (dados.Email != null && dados.CNPJ != null)
                    return PreCadastroEnum.Email;

                if (dados.CNPJ != null)
                    return PreCadastroEnum.CNPJ;

                return PreCadastroEnum.Erro;
            }
            catch (Exception error)
            {
                throw new Exception($"TipoVerificacao: {error.Message}");
            }
        }

        public bool ValidaDados(PreCadastro dados, out string erro, out Empresa empresa, out Usuario usuario)
        {
            try
            {
                erro = null;
                empresa = null;
                usuario = null;

                //if (tipo.Equals(PreCadastroEnum.CNPJ))
                //{
                //    Logger.Log($"Verifica se CNPJ informado � v�lido: {dados.CNPJ}");
                //    if (!dados.CNPJIsValid())
                //        erro = "CNPJ inv�lido.";
                //    else
                //    {
                //        Logger.Log($"Busca empresa por CNPJ: {dados.CNPJ}");
                //        empresa = empresaRepositorio.ObterPorCNPJ(dados.CNPJ);

                //        if (empresa == null)
                //            erro = "CNPJ n�o localizado.";
                //        else if (!empresa.Ativo)
                //            erro = "Opera��o n�o permitida. Contate seu GR!";
                //        else if (string.IsNullOrEmpty(empresa.Email))
                //            erro = "Opera��o n�o permitida. Contate seu GR!";
                //        else
                //            return true;
                //    }
                //}
                //else if (tipo.Equals(PreCadastroEnum.Email))
                //{
                //    Logger.Log($"Verifica se Email informado � v�lido: {dados.Email}");
                //    if (!dados.EmailIsValid())
                //        erro = "Email inv�lido.";
                //    else
                //    {
                //        Logger.Log($"Busca empresa por CNPJ: {dados.CNPJ}");
                //        empresa = empresaRepositorio.ObterPorCNPJ(dados.CNPJ);

                //        if (empresa == null)
                //            erro = "CNPJ n�o localizado.";
                //        else if (!empresa.Ativo)
                //            erro = "Opera��o n�o permitida. Contate seu GR!";
                //        else if (string.IsNullOrEmpty(empresa.Email))
                //            erro = "Opera��o n�o permitida. Contate seu GR!";
                //        else if (!empresa.Email.Equals(dados.Email))
                //            erro = "Opera��o n�o permitida. Contate seu GR!";
                //        else
                //        {
                //            Logger.Log($"Verifica se este email j� est� na base de dados: {dados.Email}");
                //            usuario = usuarioRepositorio.ObterPorEmail(dados.Email);

                //            if (usuario != null)
                //                erro = "E-mail j� cadastrado.";
                //            else
                //            {
                //                Logger.Log($"Busca usu�rio por id_empresa: {empresa.Id}");
                //                Usuario usuarioEmpresa = usuarioRepositorio.ObterPorIdEmpresa(empresa.Id);

                //                if (usuarioEmpresa != null)
                //                {
                //                    Logger.Log($"Verifica se email informado � o mesmo que consta para esta empresa na base: {usuarioEmpresa.Email}");
                //                    if (!usuarioEmpresa.Email.Equals(dados.Email))
                //                    {
                //                        if (usuarioEmpresa.Ativo)
                //                            erro = "j� existe um email cadastrado.";
                //                    }
                //                    else
                //                        erro = "Usu�rio j� cadastrado.";
                //                }
                //                else
                //                    return true;
                //            }
                //        }
                //    }
                //}
                //else if (tipo.Equals(PreCadastroEnum.Senha))
                //{
                Logger.Log($"Verifica se a senha � v�lida");
                if (!dados.PasswordIsValid())
                    erro = "Senha inv�lida.";
                else
                {
                    Logger.Log($"Busca empresa por CNPJ: {dados.CNPJ}");
                    empresa = empresaRepositorio.ObterPorCNPJ(dados.CNPJ);

                    if (empresa == null)
                        erro = "CNPJ n�o localizado.";
                    else if (!empresa.Ativo)
                        erro = "Opera��o n�o permitida. Contate seu GR!";
                    else if (string.IsNullOrEmpty(empresa.Email))
                        erro = "Opera��o n�o permitida. Contate seu GR!";
                    else if (!empresa.Email.Equals(dados.Email))
                        erro = "Opera��o n�o permitida. Contate seu GR!";
                    else
                    {
                        Logger.Log($"Verifica se este email j� est� na base de dados: {dados.Email}");
                        usuario = usuarioRepositorio.ObterPorEmail(dados.Email);

                        if (usuario != null)
                            erro = "E-mail j� cadastrado.";
                        else
                        {
                            Logger.Log($"Busca usu�rio por id_empresa: {empresa.Id}");
                            Usuario usuarioEmpresa = usuarioRepositorio.ObterPorIdEmpresa(empresa.Id);

                            if (usuarioEmpresa != null)
                            {
                                Logger.Log($"Verifica se email informado � o mesmo que consta para esta empresa na base: {usuarioEmpresa.Email}");
                                if (!usuarioEmpresa.Email.Equals(dados.Email))
                                {
                                    if (usuarioEmpresa.Ativo)
                                        erro = "j� existe um email cadastrado.";
                                }
                                else
                                    erro = "Usu�rio j� cadastrado.";
                            }
                            else
                                return true;
                        }
                    }
                }
                //}
                //else if (tipo.Equals(PreCadastroEnum.RealizaCadastro))
                //{
                //    Logger.Log("Verifica se a confirma��o da senha � v�lida");
                //    if (!dados.PasswordConfirmIsValid())
                //    {
                //        erro = "Senha inv�lida.";
                //        return false;
                //    }

                //    Logger.Log("Compara a senha e a confirma��o da senha");
                //    if (!dados.ComparePassword())
                //        erro = "As senhas n�o conferem.";
                //    else
                //    {
                //        Logger.Log($"Busca empresa por CNPJ: {dados.CNPJ}");
                //        empresa = empresaRepositorio.ObterPorCNPJ(dados.CNPJ);

                //        if (empresa == null)
                //            erro = "CNPJ n�o localizad.";
                //        else if (!empresa.Ativo)
                //            erro = "Opera��o n�o permitida. Contate seu GR!";
                //        else if (string.IsNullOrEmpty(empresa.Email))
                //            erro = "Opera��o n�o permitida. Contate seu GR!";
                //        else if (!empresa.Email.Equals(dados.Email))
                //            erro = "Opera��o n�o permitida. Contate seu GR!";
                //        else
                //        {
                //            Logger.Log($"Verifica se este email j� est� na base de dados: {dados.Email}");
                //            usuario = usuarioRepositorio.ObterPorEmail(dados.Email);

                //            if (usuario != null)
                //                erro = "E-mail j� cadastrado.";
                //            else
                //            {
                //                Logger.Log($"Busca usu�rio por id_empresa: {empresa.Id}");
                //                Usuario usuarioEmpresa = usuarioRepositorio.ObterPorIdEmpresa(empresa.Id);

                //                if (usuarioEmpresa != null)
                //                {
                //                    Logger.Log($"Verifica se email informado � o mesmo que consta para esta empresa na base: {usuarioEmpresa.Email}");
                //                    if (!usuarioEmpresa.Email.Equals(dados.Email))
                //                    {
                //                        if (usuarioEmpresa.Ativo)
                //                            erro = "j� existe um email cadastrado.";
                //                    }
                //                    else
                //                        erro = "Usu�rio j� cadastrado.";
                //                }
                //                else
                //                    return true;
                //            }
                //        }
                //    }
                //}

                return false;
            }
            catch (Exception error)
            {
                throw new Exception($"ValidaDados: {error.Message}");
            }
        }
    }
}
