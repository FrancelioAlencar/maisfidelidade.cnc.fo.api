﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model
{
    public class ResultadoRequisicao
    {
        public ResultadoRequisicao(bool sucesso, string mensagem = null, object objeto = null)
        {
            this.Mensagem = mensagem;
            this.Objeto = objeto;
            this.Sucesso = sucesso;

        }
        public string Mensagem { get; set; }
        public object Objeto { get; set; }
        public bool Sucesso { get; set; }
    }
}
