﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Gerentes
{
    public class Gerente : EntityBase, IAggregateRoot
    {
        public int Matricula { get; set; }
        public int Tipo { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
    }
}
