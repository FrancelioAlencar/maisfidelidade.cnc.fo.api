﻿using MongoDB.Bson.Serialization.Attributes;

namespace maisfidelidade.cnc.fo.api.core.Log
{
    public class LogDetalhe
    {
        [BsonElement("id_coluna_arquivo")]
        public int IdColunaArquivo { get; set; }

        [BsonElement("index")]
        public int Index { get; set; }

        [BsonElement("cnpj")]
        public string Cnpj { get; set; }

        [BsonElement("mensagem")]
        public string Mensagem { get; set; }
    }
}
