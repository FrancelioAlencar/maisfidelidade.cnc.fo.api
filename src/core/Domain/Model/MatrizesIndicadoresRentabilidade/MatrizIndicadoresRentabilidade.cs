﻿using maisfidelidade.cnc.fo.api.core.Common.AWS;
using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using Newtonsoft.Json;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.MatrizesIndicadoresRentabilidade

{
    /// <summary>
    ///
    /// </summary>
    public class MatrizIndicadorRentabilidade : EntityBase
    {
        [JsonProperty("matrixId")]
        public int IdMatriz { get; set; }

        [JsonProperty("matrix")]
        public string Matriz { get; set; }

        [JsonProperty("benefit")]
        public string IndicadorRentabilidade { get; set; }

        [JsonProperty("value")]
        public string Valor { get; set; }
    }
}