﻿using maisfidelidade.cnc.fo.api.core.Domain.Model.Usuarios;
using maisfidelidade.cnc.fo.api.core.Log;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Infrastructure.Repositories
{
    public class LogAcessoRepositorio : MySqlRepositorioBase<LogAcesso>, ILogAcessoRepositorio
    {
        public LogAcessoRepositorio()
        {

        }

        public DateTime ObterUltimoAcesso(int idUsuario)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("SELECT MAX(dt_inclusao)");
                query.Append(" FROM tb_log_acesso");
                query.AppendFormat(" WHERE id_usuario = {0}", idUsuario);

                Logger.Log($"LogAcessoRepositorio > SalvaRegistro Query MySql: {query.ToString()}");

                return this.ExecuteQueryFirst<DateTime>(query.ToString());
            }
            catch (Exception error)
            {
                throw new Exception($"LogAcessoRepositorio > ObterUltimoAcesso: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        public void SalvaRegistro(int idUsuario, int idPerfil)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("INSERT INTO tb_log_acesso (id_usuario, id_perfil) VALUES (");
                query.AppendFormat("{0}, {1});", idUsuario, idPerfil);

                Logger.Log($"LogAcessoRepositorio > SalvaRegistro Query MySql: {query.ToString()}");

                this.ExecuteQuery(query.ToString());
            }
            catch (Exception error)
            {
                throw new Exception($"LogAcessoRepositorio > SalvaRegistro: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        protected override void BuildChildCallbacks()
        {
            
        }

        protected override void PersistDeletedItem(LogAcesso item)
        {
            throw new NotImplementedException();
        }

        protected override void PersistNewItem(LogAcesso item)
        {
            throw new NotImplementedException();
        }

        protected override void PersistUpdatedItem(LogAcesso item)
        {
            throw new NotImplementedException();
        }
    }
}
