﻿using maisfidelidade.cnc.fo.api.core.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using maisfidelidade.cnc.fo.api.core.Domain.Model.FAQs;
using Amazon.SQS.Model;

namespace maisfidelidade.cnc.fo.api.core.Infrastructure.Repositories
{
	public class FAQRepositorio : MySqlRepositorioBase<FAQ>, IFAQRepositorio
	{
		public FAQRepositorio()
		{

		}
		public IList<FAQ> BuscarFAQ()
		{
			var faq = new FAQ();

			var builder = new StringBuilder();
			builder.Append("SELECT pergunta AS Pergunta ");
			builder.Append(", resposta AS Resposta ");
			builder.Append("from tb_faq ");
			builder.Append("Order by id_faq asc");

			return this.BuildEntitiesFromSql(builder.ToString());
		}

		public Template BuscarTemplate()
		{
			var comoFunciona = new Template();

			var builder = new StringBuilder();
			builder.Append("SELECT template AS Conteudo ");
			builder.Append("from tb_template ");
			builder.Append("Order by id_template asc ");
			return this.ExecuteQueryFirst<Template>(builder.ToString());
		}

		protected override void BuildChildCallbacks()
		{

		}

		protected override void PersistDeletedItem(FAQ item)
		{
			throw new NotImplementedException();
		}

		protected override void PersistNewItem(FAQ item)
		{
			throw new NotImplementedException();
		}

		protected override void PersistUpdatedItem(FAQ item)
		{
			throw new NotImplementedException();
		}
	}
}
