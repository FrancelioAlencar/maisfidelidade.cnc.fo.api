using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using maisfidelidade.cnc.fo.api.core.Common.AWS;
using maisfidelidade.cnc.fo.api.core.Common.DependencyInjection;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Usuarios;
using System;
using Newtonsoft.Json;
using maisfidelidade.cnc.fo.api.core.Domain.Model;
using maisfidelidade.cnc.fo.api.core.Log;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace maisfidelidade.cnc.fo.api.email.valida
{
	public class Function : FunctionBase
	{
		private readonly IUsuarioRepositorio usuarioRepositorio;

		public Function()
		{
			Logger.Log($"Resolve Inje��o de Dependencia");
			var dependencyResolver = new DependencyResolver();

			usuarioRepositorio = dependencyResolver.ObterServico<IUsuarioRepositorio>();
		}

		public APIGatewayProxyResponse FunctionHandler(APIGatewayProxyRequest request, ILambdaContext context)
		{
			try
			{
				Logger.Log($"Deserializa {request.Body} para Usuario");
				var requestBody = JsonConvert.DeserializeObject<Usuario>(request.Body);

				if (string.IsNullOrEmpty(requestBody.Email))
				{
					Logger.Log($"Os dados informados n�o s�o v�lidos. Email: {requestBody.Email}");
					return CreateResponse(new
					{
						success = false,
						message = $"O endere�o de e-mail deve ser preenchido"
					});
				}

				Logger.Log($"Busca informa��es do usu�rio {requestBody.Email}");
				Usuario usuario = usuarioRepositorio.ObterPorEmail(requestBody.Email);

				if (usuario == null)
				{
					Logger.Log($"Usu�rio {requestBody.Email} veio nulo");
					throw new Exception($"Usu�rio {requestBody.Email} n�o localizado.");
				}

				Logger.Log($"Seta id_usuario: {usuario.Id}");

				if (!usuario.Ativo)
				{
					Logger.Log($"Usu�rio {requestBody.Email} esta inativo");
					throw new Exception($"Usu�rio {requestBody.Email} esta inativo");
				}

				LogMessage(context, "Solicita��o de processamento bem-sucedida.");

				return CreateResponse(new
				{
					success = true
				});
			}
			catch (Exception _exception)
			{
				LogMessage(context, string.Format("Falha na solicita��o de processamento - {0}", _exception.Message));
				Logger.Log($"Error ao validar email: error ::{_exception.Message} - StackTrace:: {_exception.StackTrace} ");
				_exception.Data.Add("LambdaContext", JsonConvert.SerializeObject(context));
				return CreateResponse(new
				{
					success = false,
					message = $"Ocorreu um erro ao tentar processar sua solicita��o.<br/>Tente novamente em instantes. Se o erro persistir, fale com {PessoaContato()}."
				});
			}
		}
	}
}
