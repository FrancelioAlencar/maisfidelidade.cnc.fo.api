﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace maisfidelidade.cnc.fo.api.core.View.Model.FAQsModel
{
    public class FAQModel
    {
        public FAQModel()
        {
            Questionario = new List<QuestionarioFAQModel>();
        }

        [JsonProperty("data")]
        public List<QuestionarioFAQModel> Questionario { get; set; }
    }
}