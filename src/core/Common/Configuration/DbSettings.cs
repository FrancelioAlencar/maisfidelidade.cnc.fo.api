﻿using maisfidelidade.cnc.fo.api.core.Log;

namespace maisfidelidade.cnc.fo.api.core.Configuration
{
	public class DbSettings
	{
		public string Host { get; set; }
		public string Username { get; set; }
		public string Password { get; set; }
		public string Port { get; set; }
		public string Database { get; set; }
		public string Collection { get; set; }
		public string Protocol { get; set; }
		public string Options { get; set; }

		public DbSettings(string prefix, bool passwordBase64 = false, bool protocolBase64 = false)
		{
			Host = ConfigurationManager.AppSettings[prefix + "_host"];
			Port = ConfigurationManager.AppSettings[prefix + "_port"];
			Username = ConfigurationManager.AppSettings[prefix + "_username"];
			Password = ConfigurationManager.AppSettings.Decrypt(prefix + "_password", passwordBase64);
			Database = ConfigurationManager.AppSettings[prefix + "_database"];
			Collection = ConfigurationManager.AppSettings[prefix + "_collection"];
			Protocol = ConfigurationManager.AppSettings[prefix + "_protocol"];
			Options = ConfigurationManager.AppSettings[prefix + "_options"];
			if (protocolBase64) Protocol = Protocol.DecodeBase64();
			Logger.Log($"prefix: {prefix}, host: {Host}");
		}

		public string ConnectionString()
		{
			return $"Server={Host};Database={Database};Uid={Username};Pwd={Password};Convert Zero Datetime=True;Allow Zero Datetime=true;";
		}
	}
}
