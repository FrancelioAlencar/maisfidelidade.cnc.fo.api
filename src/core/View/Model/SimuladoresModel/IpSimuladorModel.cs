﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace maisfidelidade.cnc.fo.api.core.View.Model.SimuladoresModel
{
    public class IpSimuladorModel
    {
        [JsonProperty("qtContratos")]
        public int? QuantidadeContratos { get; set; }

        [JsonProperty("qtPrestamista")]
        public int? QuantidadePrestamista { get; set; }

        [JsonProperty("qtAuto")]
        public int? QuantidadeAuto { get; set; }

        [JsonProperty("qtAmbos")]
        public int? QuantidadeAmbos { get; set; }

        [JsonProperty("position")]
        public int? Posicao { get; set; }
    }
}