﻿using System.Collections;
using System.Collections.Generic;
using maisfidelidade.cnc.fo.api.core.Domain.Model.ApuracoesIndicadorRentabilidade;
using maisfidelidade.cnc.fo.api.core.Infrastructure;
using maisfidelidade.cnc.fo.api.core.Infrastructure.RepositoryFramework;

namespace maisfidelidade.cnc.fo.api.core.Domain
{
    public interface IApuracaoIndicadorRentabilidadeRepositorio : IRepositorio<ApuracaoIndicadorRentabilidade>
    {
        IList<ApuracaoIndicadorRentabilidade> BuscarApuracaoIndicadorRentabilidade(int idEmpresa);
    }
}