﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Mensagens
{
    public class Mensagem
    {
        public string Assunto { get; set; }
        public string Corpo { get; set; }
        public string EmailDestinatario { get; set; }
    }
}
