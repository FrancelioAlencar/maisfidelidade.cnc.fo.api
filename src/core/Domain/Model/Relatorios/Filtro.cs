﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Relatorios
{
    public class Filtro
    {
        public int IdGerente { get; set; }
        public int IdSegmento { get; set; }
        public DateTime? Data { get; set; }
        public string Grupo { get; set; }
        public int Pagina { get; set;  }
        public int Offset { get; set; }
        public bool Exportacao { get; set; }
        public string CNPJ { get; set; }
        public string NomeFantasia { get; set; }
        public string Producao { get; set; }
        public string MarketShare { get; set; }
        public string IPS { get; set; }
        public string QtdContratos { get; set; }
        public string QtdConvertidos { get; set; }
        public string QtdPrestamistas { get; set; }
        public string QtdAuto { get; set; }
        public string QtdPotencial { get; set; }
    }
}
