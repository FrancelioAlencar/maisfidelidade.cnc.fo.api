﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;

namespace maisfidelidade.cnc.fo.api.core.Infrastructure.RepositoryFramework
{
    /// <summary>
    ///
    /// </summary>
    /// <typeparam name="TAggregateRoot"></typeparam>
    public interface IRepositorio<TAggregateRoot>
        where TAggregateRoot : IAggregateRoot
    {

    }
}
