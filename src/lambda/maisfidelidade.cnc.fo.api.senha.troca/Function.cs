using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Amazon;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using maisfidelidade.cnc.fo.api.core.Common.AWS;
using maisfidelidade.cnc.fo.api.core.Common.DependencyInjection;
using maisfidelidade.cnc.fo.api.core.Configuration;
using maisfidelidade.cnc.fo.api.core.Domain.Model;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Mensagens;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Usuarios;
using maisfidelidade.cnc.fo.api.core.Log;
using Newtonsoft.Json;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace maisfidelidade.cnc.fo.api.senha.troca
{
    public class Function : FunctionBase
    {
        private readonly IUsuarioRepositorio usuarioRepositorio;
        private readonly IEmailTemplateRepositorio emailTemplateRepositorio;
        private readonly ILogSenhaRepositorio logSenhaRepositorio;

        public Function()
        {
            Logger.Log($"Resolve Inje��o de Dependencia");

            var dependencyResolver = new DependencyResolver();

            usuarioRepositorio = dependencyResolver.ObterServico<IUsuarioRepositorio>();
            emailTemplateRepositorio = dependencyResolver.ObterServico<IEmailTemplateRepositorio>();
            logSenhaRepositorio = dependencyResolver.ObterServico<ILogSenhaRepositorio>();
        }

        public async Task<APIGatewayProxyResponse> FunctionHandler(APIGatewayProxyRequest request, ILambdaContext context)
        {
            try
            {
                Logger.Log($"Valida token");
                var token = ValidaToken(request);

                Logger.Log($"Deserializa {request.Body} para Usuario");
                var requestBody = JsonConvert.DeserializeObject<Usuario>(request.Body);

                if (string.IsNullOrEmpty(requestBody.Senha))
                {
                    return CreateResponse(new
                    {
                        success = false,
                        message = $"A nova senha precisa ser informada."
                    });
                }

                Logger.Log($"Busca informa��es do usu�rio {token.IdUsuario}");
                Usuario usuario = new Usuario();
                usuario = usuarioRepositorio.ObterPorId(token.IdUsuario);

                //if (usuario == null)
                //{
                //	Logger.Log($"Usu�rio {requestBody.Email} veio nulo");
                //	return CreateResponse(new { success = false, message = "Usu�rio n�o localizado." });
                //}

                //if (!usuario.Ativo)
                //{
                //	Logger.Log($"Usu�rio {requestBody.Email} esta inativo");
                //	return CreateResponse(new { success = false, message = "Usu�rio inativo." });
                //}

                if (requestBody.Senha.Length != 6)
                {
                    Logger.Log($"Senha com o length diferente de 6 ({requestBody.Senha.Length})");
                    return CreateResponse(new
                    {
                        success = false,
                        message = $"A nova senha precisa ter 6 caracteres."
                    });
                }

                if (!usuario.TrocarSenha)
                {
                    Logger.Log($"Usu�rio n�o flagado para troca de senha {usuario.Id}");
                    return CreateResponse(new
                    {
                        success = false,
                        message = $"Opera��o n�o autorizada."
                    });
                }

                Logger.Log("Prepara senha");
                var senhaAntiga = usuario.Senha;
                var novaSenha = requestBody.Senha;
                var newPasswordHash = usuario.Encrypt(novaSenha);
                usuario.Senha = newPasswordHash;
                usuario.TrocarSenha = false;

                Logger.Log("Troca senha");
                usuarioRepositorio.TrocaSenha(usuario);

                Logger.Log($"Salva registro de log");
                logSenhaRepositorio.SalvaRegistro(new LogSenha(usuario.Id, senhaAntiga, false));

                //Logger.Log($"Prepara mensagem disparo de e-mail");
                //var mensagem = new Mensagem();
                //mensagem.EmailDestinatario = usuario.Email;

                //Logger.Log($"Busca template de email de senha alterada: {(int)EmailEnum.SenhaAlterada}");
                //var configuracaoEmail = emailTemplateRepositorio.ObterPorId((int)EmailEnum.SenhaAlterada);
                //mensagem.Assunto = configuracaoEmail.Titulo;
                //mensagem.Corpo = configuracaoEmail.Mensagem;

                //Logger.Log($"Envia email alertando troca de senha");
                //await EnviarEmailAsync(mensagem);

                LogMessage(context, "Solicita��o de processamento bem-sucedida.");

                return CreateResponse(new { success = true });
            }
            catch (Exception _exception)
            {
                LogMessage(context, string.Format("Falha na solicita��o de processamento - {0}", _exception.Message));
                Logger.Log($"Error ao trocar senha: error ::{_exception.Message} - StackTrace:: {_exception.StackTrace} ");
                _exception.Data.Add("LambdaContext", JsonConvert.SerializeObject(context));
                return CreateResponse(new
                {
                    success = false,
                    message = $"Ocorreu um erro ao tentar processar sua solicita��o.<br/>Tente novamente em instantes. Se o erro persistir, fale com {PessoaContato()}."
                });
            }
        }

        public async Task<bool> EnviarEmailAsync(Mensagem mensagem)
        {
            var destinatario = new Destination(new List<string> { mensagem.EmailDestinatario });
            var assunto = new Content(mensagem.Assunto);
            var corpo = new Body(new Content(mensagem.Corpo));

            var mensagemSES = new Message(assunto, corpo);

            var requisicaoEnvio = new SendEmailRequest(
                source: ConfigurationManager.AppSettings["EnvioEmail:emailRemetente"],
                destination: destinatario,
                message: mensagemSES);

            SendEmailResponse resposta;
            using (var client = new AmazonSimpleEmailServiceClient(RegionEndpoint.GetBySystemName(ConfigurationManager.AppSettings["EnvioEmail:regiao"])))
            {
                resposta = await client.SendEmailAsync(requisicaoEnvio);
            }

            if (resposta.HttpStatusCode != System.Net.HttpStatusCode.OK)
                return false;

            return true;
        }
    }
}