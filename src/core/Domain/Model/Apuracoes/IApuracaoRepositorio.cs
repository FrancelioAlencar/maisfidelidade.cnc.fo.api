﻿using System.Collections;
using System.Collections.Generic;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Apuracoes;
using maisfidelidade.cnc.fo.api.core.Infrastructure;
using maisfidelidade.cnc.fo.api.core.Infrastructure.RepositoryFramework;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Apuracoes
{
    public interface IApuracaoRepositorio : IRepositorio<Apuracao>
    {
        Apuracao BuscarApuracaoSemanal(int idUsuario);
    }
}
