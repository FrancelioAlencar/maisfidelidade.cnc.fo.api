﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Indicadores.Performance
{
    public enum TipoIndicadorPerformanceEnum
    {
        ComPercentual = 1,
        SemPèrcentual = 2,
    }
}
