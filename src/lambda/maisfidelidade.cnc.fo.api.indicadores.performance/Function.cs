using System;
using System.Collections.Generic;
using System.Linq;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using maisfidelidade.cnc.fo.api.core.Common.AWS;
using maisfidelidade.cnc.fo.api.core.Common.DependencyInjection;
using maisfidelidade.cnc.fo.api.core.Domain.Model;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Empresas;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Indicadores.Performance;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Usuarios;
using maisfidelidade.cnc.fo.api.core.Log;
using Newtonsoft.Json;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace maisfidelidade.cnc.fo.api.indicadores.performance
{
    public class Function : FunctionBase
    { 
        private readonly IUsuarioRepositorio usuarioRepositorio;
        private readonly IEmpresaRepositorio empresaRepositorio;
        private readonly IIndicadorPerformanceRepositorio indicadorPerformanceRepositorio;
        public Function()
        {
            Logger.Log($"Resolve Injeção de Dependencia");

            var dependencyResolver = new DependencyResolver();

            usuarioRepositorio = dependencyResolver.ObterServico<IUsuarioRepositorio>();
            empresaRepositorio = dependencyResolver.ObterServico<IEmpresaRepositorio>();
            indicadorPerformanceRepositorio = dependencyResolver.ObterServico<IIndicadorPerformanceRepositorio>();
        }

        public APIGatewayProxyResponse FunctionHandler(APIGatewayProxyRequest request, ILambdaContext context)
        {
            try
            {
                Logger.Log("Valida token");
                var token = ValidaToken(request);

                Logger.Log($"Busca lista de indicadores de performance pela empresa: {token.IdEmpresa}");
                IList<IndicadorPerformance> listaIndicadores = indicadorPerformanceRepositorio.ObterPorEmpresa(token.IdEmpresa);
                
                if (!listaIndicadores.Any())
                    return CreateResponse(new ResultadoRequisicao(false, "Nenhum indicador localizado."));

                return CreateResponse(new { success = true, data = ObterRetorno(listaIndicadores) });
            }
            catch (Exception _exception)
            {
                LogMessage(context, string.Format("Falha na solicitação de processamento - {0}", _exception.Message));
                Logger.Log($"Error ao carregar os indicadores de performance: error ::{_exception.Message} - StackTrace:: {_exception.StackTrace} ");
                _exception.Data.Add("LambdaContext", JsonConvert.SerializeObject(context));
                return CreateResponse(new
                {
                    success = false,
                    message = $"Ocorreu um erro ao tentar processar sua solicitação.<br/>Tente novamente em instantes. Se o erro persistir, fale com {PessoaContato()}."
                });
            }
        }

        public IList<IndicadorPerformanceRetorno> ObterRetorno(IList<IndicadorPerformance> lista)
        {
            IList<IndicadorPerformanceRetorno> retorno = new List<IndicadorPerformanceRetorno>();

            foreach (var item in lista.GroupBy(x => x.IdEntidade))
            {
                IndicadorPerformanceRetorno indicador = new IndicadorPerformanceRetorno();
                indicador.Indicador = (IndicadorPerformanceEnum)int.Parse(item.Key.ToString());
                indicador.Dados = new List<IndicadorPerformanceDado>();
                
                foreach (var info in item)
                {
                    indicador.Nome = info.Indicador;
                    indicador.Descricao = info.Descricao;
                    indicador.Tooltip = info.Tooltip;
                    indicador.Valor = info.Valor;
                    indicador.Dados.Add(new IndicadorPerformanceDado
                    {
                        Dado = info.Dado,
                        Valor = info.ValorDado
                    });
                }

                retorno.Add(indicador);
            }

            return retorno;
        }
    }
}
