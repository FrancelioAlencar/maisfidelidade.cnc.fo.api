﻿using Newtonsoft.Json;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Indicadores.Performance
{
    public class IndicadorPerformanceDado
    {
        [JsonProperty("info")]
        public string Dado { get; set; }
        
        [JsonProperty("value")] 
        public string Valor { get; set; }
    }
}
