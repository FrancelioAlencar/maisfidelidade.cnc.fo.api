﻿using maisfidelidade.cnc.fo.api.core.Domain.Model.Gerentes;
using maisfidelidade.cnc.fo.api.core.Log;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Infrastructure.Repositories
{
    public class GerenteRepositorio : MySqlRepositorioBase<Gerente>, IGerenteRepositorio
    {
        public GerenteRepositorio()
        {

        }

        public Gerente ObterPorEmail(string email)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("SELECT id_gerente AS Id, matricula AS Matricula, id_tipo_gerente AS Tipo, nome AS Nome, email AS Email, telefone AS Telefone, ativo AS Ativo, dt_inclusao AS DataInclusao, dt_alteracao AS DataAlteracao ");
                query.Append(" FROM tb_gerente ");
                query.AppendFormat(" WHERE email = '{0}'", email);

                Logger.Log($"GerenteRepositorio > ObterPorEmail Query MySql: {query.ToString()}");

                return this.BuildEntityFromSql(query.ToString());
            }
            catch (Exception error)
            {
                throw new Exception($"GerenteRepositorio > ObterPorEmail: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        protected override void BuildChildCallbacks()
        {

        }

        protected override void PersistDeletedItem(Gerente item)
        {
            throw new NotImplementedException();
        }

        protected override void PersistNewItem(Gerente item)
        {
            throw new NotImplementedException();
        }

        protected override void PersistUpdatedItem(Gerente item)
        {
            throw new NotImplementedException();
        }
    }
}
