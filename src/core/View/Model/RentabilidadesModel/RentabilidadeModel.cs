﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace maisfidelidade.cnc.fo.api.core.View.Model.RentabilidadesModel
{
    public class RentabilidadeModel
    {
        public RentabilidadeModel()
        {
            Dados = new DadosRentabilidadeModel();
        }

        [JsonProperty("data")]
        public DadosRentabilidadeModel Dados { get; set; }
    }
}