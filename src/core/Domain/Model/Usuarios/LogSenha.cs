﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Usuarios
{
    public class LogSenha : EntityBase, IAggregateRoot
    {
        public LogSenha(int idUsuario, string hashSenha, bool emailRecuperarSenha)
        {
            this.IdUsuario = idUsuario;
            this.HashSenha = hashSenha;
            this.EmailRecuperarSenha = emailRecuperarSenha;
        }

        public int IdUsuario { get; set; }
        public string HashSenha{ get; set; }
        public bool EmailRecuperarSenha { get; set; }
    }
}
