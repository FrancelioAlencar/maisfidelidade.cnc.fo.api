﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace maisfidelidade.cnc.fo.api.core.View.Model.SimuladoresModel
{
    public class ParticipacaoMercadoSimuladorModel
    {
        [JsonProperty("santander")]
        public decimal? Santander { get; set; }

        [JsonProperty("store")]
        public decimal? Loja { get; set; }

        [JsonProperty("position")]
        public int? Posicao { get; set; }

    }
}