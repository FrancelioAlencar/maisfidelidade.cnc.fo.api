﻿using maisfidelidade.cnc.fo.api.core.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using maisfidelidade.cnc.fo.api.core.Log;
using Amazon.SQS.Model;
using maisfidelidade.cnc.fo.api.core.Domain.Model.ApuracoesIndicadorRentabilidade;

namespace maisfidelidade.cnc.fo.api.core.Infrastructure.Repositories
{
	public class ApuracaoIndicadorRentabilidadeRepositorio : MySqlRepositorioBase<ApuracaoIndicadorRentabilidade>, IApuracaoIndicadorRentabilidadeRepositorio
	{
		public ApuracaoIndicadorRentabilidadeRepositorio()
		{

		}
		public IList<ApuracaoIndicadorRentabilidade> BuscarApuracaoIndicadorRentabilidade(int idEmpresa)
		{
			var builder = new StringBuilder();
			builder.Append("SELECT ir.id_indicador_rentabilidade AS IdIndicadorRentabilidade, ");
			builder.Append(" ir.id_indicador_rentabilidade_pai AS IdIndicadorRentabilidadePai, "); 
			builder.Append(" ir.nome AS Nome, ");
			builder.Append(" air.maximo AS Maximo, ");
			builder.Append(" air.realizado AS Realizado, ");
			builder.Append(" ir.id_indicador_rentabilidade_pai AS Tipo ");
			builder.Append(" FROM tb_indicador_rentabilidade ir ");
			builder.Append(" INNER JOIN tb_apuracao_indicador_rentabilidade air ");
			builder.Append(" ON ir.id_indicador_rentabilidade = air.id_indicador_rentabilidade ");
			builder.Append(" INNER JOIN tb_apuracao AS A ON A.id_apuracao = air.id_apuracao");
			builder.Append($" WHERE id_empresa = {idEmpresa}");
			builder.Append(" ORDER BY ");
			builder.Append(" air.id_apuracao_indicador_rentabilidade ASC ");

			Logger.Log($"Busca Matriz: {builder.ToString()}");

			return this.BuildEntitiesFromSql(builder.ToString());
		}

		protected override void BuildChildCallbacks()
		{

		}

		protected override void PersistDeletedItem(ApuracaoIndicadorRentabilidade item)
		{
			throw new NotImplementedException();
		}

		protected override void PersistNewItem(ApuracaoIndicadorRentabilidade item)
		{
			throw new NotImplementedException();
		}

		protected override void PersistUpdatedItem(ApuracaoIndicadorRentabilidade item)
		{
			throw new NotImplementedException();
		}
	}
}
