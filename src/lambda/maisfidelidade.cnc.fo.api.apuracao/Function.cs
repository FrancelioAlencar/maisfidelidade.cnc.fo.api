using System;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using maisfidelidade.cnc.fo.api.core.Common.AWS;
using maisfidelidade.cnc.fo.api.core.Common.DependencyInjection;
using maisfidelidade.cnc.fo.api.core.Domain.Model;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Apuracoes;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Empresas;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Mensagens;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Usuarios;
using maisfidelidade.cnc.fo.api.core.Log;
using Newtonsoft.Json;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace maisfidelidade.cnc.fo.api.apuracao
{
    public class Function : FunctionBase
    {
        private readonly IApuracaoRepositorio apuracaoRepositorio;

        public Function()
        {
            Logger.Log($"Resolve Inje��o de Dependencia");

            var dependencyResolver = new DependencyResolver();

            apuracaoRepositorio = dependencyResolver.ObterServico<IApuracaoRepositorio>();
        }

        public APIGatewayProxyResponse FunctionHandler(APIGatewayProxyRequest request, ILambdaContext context)
        {
            try
            {
                Logger.Log("Valida token");
                var token = ValidaToken(request);

                Logger.Log($"Obtem as apuracao da empresa {token.IdEmpresa}");
                ApuracaoDashboard apuracao = apuracaoRepositorio.ObterDashboard(token.IdEmpresa);

                if (apuracao == null)
                {
                    Logger.Log($"Nenhuma apura��o localizada para a empresa {token.IdEmpresa}");
                    return CreateResponse(new
                    {
                        success = false,
                        message = $"Nenhuma apura��o localizada."
                    });
                }

                var retorno = new
                {
                    last_month = new
                    {
                        month = apuracao.DataAnterior.Month,
                        plus = apuracao.PlusAnterior,
                        auto = apuracao.AutoAnterior,
                        prestamista = apuracao.PrestamistaAnterior
                    },
                    current_month = new
                    {
                        month = apuracao.DataAtual.Month,
                        plus = apuracao.PlusAtual,
                        auto = apuracao.AutoAtual,
                        prestamista = apuracao.PrestamistaAtual
                    },
                    next_month = new
                    {
                        month = apuracao.DataFuturo.Month,
                        plus = apuracao.PlusFuturo,
                        auto = apuracao.AutoFuturo,
                        prestamista = apuracao.PrestamistaFuturo
                    },
                };

                LogMessage(context, "Solicita��o de processamento bem-sucedida.");

                return CreateResponse(new { success = true, data = retorno });
            }
            catch (Exception _exception)
            {
                LogMessage(context, string.Format("Falha na solicita��o de processamento - {0}", _exception.Message));
                Logger.Log($"Error ao carregar apura��o: error ::{_exception.Message} - StackTrace:: {_exception.StackTrace} ");
                _exception.Data.Add("LambdaContext", JsonConvert.SerializeObject(context));
                return CreateResponse(new
                {
                    success = false,
                    message = $"Ocorreu um erro ao tentar processar sua solicita��o.<br/>Tente novamente em instantes. Se o erro persistir, fale com {PessoaContato()}."
                });
            }
        }
    }
}