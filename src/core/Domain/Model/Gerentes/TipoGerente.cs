﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Gerentes
{
    public class TipoGerente : EntityBase, IAggregateRoot
    {
        public string Nome { get; set; }
        public string Sigla { get; set; }
        public GerenteEnum Tipo { get; set; }
    }
}
