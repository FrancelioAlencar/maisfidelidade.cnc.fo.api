﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.RepositoryFramework;
using System.Collections.Generic;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Indicadores.Performance
{
    public interface IIndicadorPerformanceRepositorio : IRepositorio<IndicadorPerformance>
    {
        IList<IndicadorPerformance> ObterPorEmpresa(int idEmpresa);
    }
}
