INSERT INTO autos_tb_configuracao (chave, valor)
VALUES ('categoria_bronze_pontos_de_padrao', '0'),
	('categoria_bronze_pontos_ate_padrao', '199'),
    ('categoria_silver_pontos_de_padrao', '200'),
	('categoria_silver_pontos_ate_padrao', '499'),
    ('categoria_gold_pontos_de_padrao', '500'),
	('categoria_gold_pontos_ate_padrao', '849'),
    ('categoria_platinum_pontos_de_padrao', '850'),
	('categoria_platinum_pontos_ate_padrao', '99999');
    
INSERT INTO autos_tb_configuracao (chave, valor)
VALUES ('criterio_market_share_perc_padrao', '0'),
	('criterio_pagamento_cc_perc_padrao', '0'),
    ('criterio_ip_seguro_perc_padrao', '0'),
	('criterio_maquina_getnet_perc_padrao', '0'),
    ('criterio_indicacao_cc_pf_perc_padrao', '0'),
	('criterio_aceite_de_leads_perc_padrao', '0'),
    ('criterio_pagamento_webmotors_perc_padrao', '0'),
	('criterio_estoque_potencial_perc_padrao', '0');
    
INSERT INTO autos_tb_configuracao (chave, valor)
VALUES ('criterio_market_share_ptos_padrao', '0'),
	('criterio_pagamento_cc_ptos_padrao', '0'),
    ('criterio_ip_seguro_ptos_padrao', '0'),
	('criterio_maquina_getnet_ptos_padrao', '0'),
    ('criterio_indicacao_cc_pf_ptos_padrao', '0'),
	('criterio_aceite_de_leads_ptos_padrao', '0'),
    ('criterio_pagamento_webmotors_ptos_padrao', '0'),
	('criterio_estoque_potencial_ptos_padrao', '0');    
    
INSERT INTO autos_tb_configuracao (chave, valor)
VALUES ('criterio_market_share_perc_max_padrao', '100'),
	('criterio_pagamento_cc_perc_max_padrao', '100'),
    ('criterio_ip_seguro_perc_max_padrao', '100'),
	('criterio_maquina_getnet_perc_max_padrao', '0'),
    ('criterio_indicacao_cc_pf_perc_max_padrao', '0'),
	('criterio_aceite_de_leads_perc_max_padrao', '100'),
    ('criterio_pagamento_webmotors_perc_max_padrao', '0'),
	('criterio_estoque_potencial_perc_max_padrao', '100');

INSERT INTO autos_tb_configuracao (chave, valor)
VALUES ('criterio_market_share_ptos_max_padrao', '500'),
	('criterio_pagamento_cc_ptos_max_padrao', '100'),
    ('criterio_ip_seguro_ptos_max_padrao', '200'),
	('criterio_maquina_getnet_ptos_max_padrao', '50'),
    ('criterio_indicacao_cc_pf_ptos_max_padrao', '5'),
	('criterio_aceite_de_leads_ptos_max_padrao', '100'),
    ('criterio_pagamento_webmotors_ptos_max_padrao', '50'),
	('criterio_estoque_potencial_ptos_max_padrao', '100');
