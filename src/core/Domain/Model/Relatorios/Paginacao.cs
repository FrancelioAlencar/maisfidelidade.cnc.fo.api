﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Relatorios
{
    public class Paginacao
    {
        public int TotalRegistros { get; set; }
        public int QuantidadePaginas { get; set; }
    }
}
