﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Apuracoes.Indicadores.Classificacao
{
    public enum IndicadorClassificacaoEnum
    {
        MarketShare = 1,
        IPS = 2
    }
}
