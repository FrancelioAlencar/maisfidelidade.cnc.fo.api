﻿using maisfidelidade.cnc.fo.api.core.Domain.Model.Usuarios;
using maisfidelidade.cnc.fo.api.core.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Infrastructure.Repositories
{
    public class UsuarioRepositorio : MySqlRepositorioBase<Usuario>, IUsuarioRepositorio
    {
        public UsuarioRepositorio()
        {

        }

        public void AssociaPerfil(Usuario usuario, PerfilEnum perfil)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("INSERT INTO tb_usuario_perfil (id_usuario, id_perfil) VALUES (");
                query.AppendFormat(" {0}, {1});", usuario.Id, (int)perfil);

                Logger.Log($"UsuarioRepositorio > AssociaPerfil Query MySql: {query.ToString()}");

                this.ExecuteQuery(query.ToString());
            }
            catch (Exception error)
            {
                throw new Exception($"UsuarioRepositorio > AssociaPerfil: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        public void AtualizaIdEmpresa(int idUsuario, int idEmpresa)
        {
            try
            {
                var query = new StringBuilder();

                query.AppendFormat("UPDATE tb_usuario SET id_empresa = {0} ", idEmpresa);
                query.AppendFormat(" WHERE id_usuario = {0}", idUsuario);

                Logger.Log($"UsuarioRepositorio > AtualizaIdEmpresa Query MySql: {query.ToString()}");

                this.ExecuteQuery(query.ToString());
            }
            catch (Exception error)
            {
                throw new Exception($"UsuarioRepositorio > AtualizaIdEmpresa: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        public Usuario Cadastro(Usuario usuario)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("INSERT INTO tb_usuario (nome, email, hash_senha, id_empresa, dt_cadastro, dt_alteracao, fl_trocar_senha, fl_ativo) VALUES (");
                query.AppendFormat(" '{0}', '{1}', '{2}', {3}, NOW(), NOW(), {4}, {5});", usuario.Nome, usuario.Email, usuario.Senha, usuario.IdEmpresa, usuario.TrocarSenha, usuario.Ativo);

                Logger.Log($"UsuarioRepositorio > Cadastro Query MySql: {query.ToString()}");

                this.ExecuteQuery(query.ToString());

                return ObterPorEmail(usuario.Email);
            }
            catch (Exception error)
            {
                throw new Exception($"UsuarioRepositorio > Cadastro: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        public Usuario ObterPorEmail(string email)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("SELECT id_usuario AS Id, nome AS Nome, email AS Email, hash_senha AS Senha, fl_ativo AS Ativo, fl_trocar_senha AS TrocarSenha, id_empresa AS IdEmpresa ");
                query.Append(" FROM tb_usuario ");
                query.AppendFormat(" WHERE email = '{0}'", email);

                Logger.Log($"UsuarioRepositorio > ObterPorEmail Query MySql: {query.ToString()}");

                return this.BuildEntityFromSql(query.ToString());
            }
            catch (Exception error)
            {
                throw new Exception($"UsuarioRepositorio > ObterPorEmail: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        public Usuario ObterPorId(int id)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("SELECT id_usuario AS Id, nome AS Nome, email AS Email, hash_senha AS Senha, fl_ativo AS Ativo, fl_trocar_senha AS TrocarSenha, id_empresa AS IdEmpresa ");
                query.Append(" FROM tb_usuario ");
                query.AppendFormat(" WHERE id_usuario = {0}", id);

                Logger.Log($"UsuarioRepositorio > ObterPorId Query MySql: {query.ToString()}");

                return this.BuildEntityFromSql(query.ToString());
            }
            catch (Exception error)
            {
                throw new Exception($"UsuarioRepositorio > ObterPorId: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        public Usuario ObterPorIdEmpresa(int id)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("SELECT id_usuario AS Id, nome AS Nome, email AS Email, hash_senha AS Senha, fl_ativo AS Ativo, fl_trocar_senha AS TrocarSenha, id_empresa AS IdEmpresa ");
                query.Append(" FROM tb_usuario ");
                query.AppendFormat(" WHERE id_empresa = {0}", id);

                Logger.Log($"UsuarioRepositorio > ObterPorIdEmpresa Query MySql: {query.ToString()}");

                return this.BuildEntityFromSql(query.ToString());
            }
            catch (Exception error)
            {
                throw new Exception($"UsuarioRepositorio > ObterPorIdEmpresa: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        public void TrocaSenha(Usuario usuario)
        {
            try
            {
                var query = new StringBuilder();

                query.AppendFormat("UPDATE tb_usuario SET hash_senha = '{0}', fl_trocar_senha = {1}, dt_alteracao = NOW() ", usuario.Senha, usuario.TrocarSenha);
                query.AppendFormat(" WHERE id_usuario = {0}", usuario.Id);

                Logger.Log($"UsuarioRepositorio > TrocaSenha Query MySql: {query.ToString()}");

                this.ExecuteQuery(query.ToString());
            }
            catch (Exception error)
            {
                throw new Exception($"UsuarioRepositorio > TrocaSenha: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        protected override void BuildChildCallbacks()
        {
            this.ChildCallbacks.Add("perfil",
                delegate (IList<Usuario> usuarios, object childKeyName)
                {
                    this.AppendPerfil(usuarios);
                });
        }

        protected override void PersistDeletedItem(Usuario item)
        {
            throw new NotImplementedException();
        }

        protected override void PersistNewItem(Usuario item)
        {
            throw new NotImplementedException();
        }

        protected override void PersistUpdatedItem(Usuario item)
        {
            throw new NotImplementedException();
        }

        private void AppendPerfil(IList<Usuario> usuarios)
        {
            try
            {
                if (!usuarios.Any())
                    return;

                var usuario = usuarios.FirstOrDefault();

                var query = new StringBuilder();
                query.Append(" SELECT B.id_perfil AS Id, B.nome AS Nome");
                query.Append(" FROM tb_usuario_perfil AS A");
                query.Append(" LEFT JOIN tb_perfil AS B ON B.id_perfil = A.id_perfil");
                query.AppendFormat(" WHERE A.id_usuario = {0} AND B.fl_ativo = 1", usuario.Id);

                Logger.Log($"UsuarioRepositorio > AppendPerfil Query MySql: {query.ToString()}");

                usuario.Perfil = this.ExecuteQueryFirst<Perfil>(query.ToString());

                if (usuario.Perfil != null)
                    this.AppendPermissao(usuario.Perfil);

                usuarios[0] = usuario;
            }
            catch (Exception error)
            {
                throw new Exception($"UsuarioRepositorio > AppendPerfil: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        private void AppendPermissao(Perfil perfil)
        {
            try
            {
                var query = new StringBuilder();
                query.Append(" SELECT B.id_permissao AS Id, B.nome AS Nome ");
                query.Append(" FROM tb_perfil_permissao AS A");
                query.Append(" LEFT JOIN tb_permissao AS B ON B.id_permissao = A.id_permissao");
                query.AppendFormat(" WHERE A.id_perfil = {0} AND B.fl_ativo = 1;", perfil.Id);

                Logger.Log($"UsuarioRepositorio > AppendPermissao Query MySql: {query.ToString()}");

                perfil.Permissoes = this.ExecuteQuery<Permissao>(query.ToString());
            }
            catch (Exception error)
            {
                throw new Exception($"UsuarioRepositorio > AppendPermissao: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }
    }
}
