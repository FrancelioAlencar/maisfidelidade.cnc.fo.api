﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain
{
    public class Configuracao : EntityBase, IAggregateRoot
    {
        public string Tipo { get; set; }
        public string Valor { get; set; }
    }
}
