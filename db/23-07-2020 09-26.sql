-- --------------------------------------------------------
-- Servidor:                     dbmaisfidelidade-2020-dev.c4thiyk7foml.us-east-1.rds.amazonaws.com
-- Versão do servidor:           5.7.22-log - Source distribution
-- OS do Servidor:               Linux
-- HeidiSQL Versão:              11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura para tabela db_concessionaria.tb_apuracao
DROP TABLE IF EXISTS `tb_apuracao`;
CREATE TABLE IF NOT EXISTS `tb_apuracao` (
  `id_apuracao` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_arquivo` int(10) unsigned NOT NULL,
  `id_empresa` int(10) unsigned NOT NULL,
  `id_segmento` int(10) unsigned NOT NULL,
  `id_categoria` int(10) unsigned NOT NULL,
  `financiamento_santander` decimal(10,2) NOT NULL,
  `financiamento_loja` decimal(10,2) NOT NULL,
  `qtd_contratos` int(10) unsigned NOT NULL,
  `qtd_prestamista` int(10) unsigned NOT NULL,
  `qtd_auto` int(10) unsigned NOT NULL,
  `qtd_ambos` int(10) unsigned NOT NULL,
  `dt_inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dt_alteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_apuracao`),
  KEY `fk_arquivo__apuracao` (`id_arquivo`),
  KEY `fk_empresa__apuracao` (`id_empresa`),
  KEY `fk_categoria__apuracao` (`id_categoria`),
  KEY `fk_segmento__apuracao` (`id_segmento`),
  CONSTRAINT `fk_arquivo__apuracao` FOREIGN KEY (`id_arquivo`) REFERENCES `tb_arquivo` (`id_arquivo`),
  CONSTRAINT `fk_categoria__apuracao` FOREIGN KEY (`id_categoria`) REFERENCES `tb_categoria` (`id_categoria`),
  CONSTRAINT `fk_empresa__apuracao` FOREIGN KEY (`id_empresa`) REFERENCES `tb_empresa` (`id_empresa`),
  CONSTRAINT `fk_segmento__apuracao` FOREIGN KEY (`id_segmento`) REFERENCES `tb_segmento` (`id_segmento`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_apuracao: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_apuracao` DISABLE KEYS */;
INSERT INTO `tb_apuracao` (`id_apuracao`, `id_arquivo`, `id_empresa`, `id_segmento`, `id_categoria`, `financiamento_santander`, `financiamento_loja`, `qtd_contratos`, `qtd_prestamista`, `qtd_auto`, `qtd_ambos`, `dt_inclusao`, `dt_alteracao`) VALUES
	(1, 1, 3, 2, 3, 500000.00, 500000.00, 300, 300, 300, 300, '2020-07-21 12:48:41', '2020-07-21 12:48:41');
/*!40000 ALTER TABLE `tb_apuracao` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_apuracao_indicador_performance
DROP TABLE IF EXISTS `tb_apuracao_indicador_performance`;
CREATE TABLE IF NOT EXISTS `tb_apuracao_indicador_performance` (
  `id_apuracao_indicador_performance` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_apuracao` int(10) NOT NULL,
  `id_indicador` int(11) NOT NULL,
  `valor` varchar(100) NOT NULL,
  PRIMARY KEY (`id_apuracao_indicador_performance`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_apuracao_indicador_performance: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_apuracao_indicador_performance` DISABLE KEYS */;
INSERT INTO `tb_apuracao_indicador_performance` (`id_apuracao_indicador_performance`, `id_apuracao`, `id_indicador`, `valor`) VALUES
	(1, 1, 1, ''),
	(2, 1, 2, '50%'),
	(3, 1, 3, '50%');
/*!40000 ALTER TABLE `tb_apuracao_indicador_performance` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_apuracao_indicador_performance_criterios
DROP TABLE IF EXISTS `tb_apuracao_indicador_performance_criterios`;
CREATE TABLE IF NOT EXISTS `tb_apuracao_indicador_performance_criterios` (
  `id_apuracao_indicador_performance_criterio` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_apuracao_indicador` int(11) NOT NULL,
  `id_criterio` int(11) NOT NULL,
  `valor` varchar(100) NOT NULL,
  PRIMARY KEY (`id_apuracao_indicador_performance_criterio`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_apuracao_indicador_performance_criterios: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_apuracao_indicador_performance_criterios` DISABLE KEYS */;
INSERT INTO `tb_apuracao_indicador_performance_criterios` (`id_apuracao_indicador_performance_criterio`, `id_apuracao_indicador`, `id_criterio`, `valor`) VALUES
	(1, 1, 1, 'R$ 0000,00'),
	(2, 1, 2, 'R$ 0000,00'),
	(3, 2, 3, '8'),
	(4, 2, 4, '4'),
	(5, 3, 5, '8'),
	(6, 3, 6, '4');
/*!40000 ALTER TABLE `tb_apuracao_indicador_performance_criterios` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_arquivo
DROP TABLE IF EXISTS `tb_arquivo`;
CREATE TABLE IF NOT EXISTS `tb_arquivo` (
  `id_arquivo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_arquivo_stage` char(24) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `tipo` tinyint(1) NOT NULL,
  `dt_referencia` datetime NOT NULL,
  `dt_previsao_atualizacao` datetime NOT NULL,
  `dt_importacao` datetime NOT NULL,
  `dt_publicacao` datetime NOT NULL,
  `ultimo_processado` bit(1) NOT NULL DEFAULT b'0',
  `total_linhas` int(10) unsigned NOT NULL DEFAULT '0',
  `ativo` bit(1) NOT NULL DEFAULT b'1',
  `dt_inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dt_alteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_arquivo`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_arquivo: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_arquivo` DISABLE KEYS */;
INSERT INTO `tb_arquivo` (`id_arquivo`, `id_arquivo_stage`, `nome`, `tipo`, `dt_referencia`, `dt_previsao_atualizacao`, `dt_importacao`, `dt_publicacao`, `ultimo_processado`, `total_linhas`, `ativo`, `dt_inclusao`, `dt_alteracao`) VALUES
	(1, 'ARQUIVO_TESTE', 'ARQUIVO_TESTE', 1, '2020-07-21 12:24:41', '2020-07-21 12:24:41', '2020-07-21 12:24:41', '2020-07-21 12:24:41', b'1', 1, b'1', '2020-07-21 12:24:41', '2020-07-21 12:26:38');
/*!40000 ALTER TABLE `tb_arquivo` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_beneficio
DROP TABLE IF EXISTS `tb_beneficio`;
CREATE TABLE IF NOT EXISTS `tb_beneficio` (
  `id_beneficio` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_entidade` int(10) unsigned NOT NULL,
  `nome` varchar(50) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `tooltip` varchar(100) NOT NULL,
  `ativo` bit(1) NOT NULL DEFAULT b'1',
  `dt_inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dt_alteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_beneficio`),
  UNIQUE KEY `uq_id_entidade_beneficio` (`id_entidade`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_beneficio: ~9 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_beneficio` DISABLE KEYS */;
INSERT INTO `tb_beneficio` (`id_beneficio`, `id_entidade`, `nome`, `descricao`, `tooltip`, `ativo`, `dt_inclusao`, `dt_alteracao`) VALUES
	(1, 1, 'Campanha de Marketing', '', '', b'1', '2020-07-14 13:53:10', '2020-07-14 13:53:10'),
	(2, 2, 'Comissão Plus', '', '', b'1', '2020-07-14 13:53:10', '2020-07-14 13:53:10'),
	(3, 3, 'Retorno', '', '', b'1', '2020-07-14 13:53:10', '2020-07-14 13:53:10'),
	(4, 4, 'Comissão Prestamista', '', '', b'1', '2020-07-14 13:53:10', '2020-07-14 13:53:10'),
	(5, 5, 'Comissão Seguro Auto', '', '', b'1', '2020-07-14 13:53:10', '2020-07-14 13:53:10'),
	(6, 6, 'Pagamento Sem Gravame', '', '', b'1', '2020-07-14 13:53:10', '2020-07-14 13:53:10'),
	(7, 7, 'Pagamento Sem Documento', '', '', b'1', '2020-07-14 13:53:10', '2020-07-14 13:53:10'),
	(8, 8, 'Floorplan Usados', '', '', b'1', '2020-07-14 13:53:10', '2020-07-14 13:53:10'),
	(9, 9, 'Mesa de Crédito (Buyer)', '', '', b'1', '2020-07-14 13:53:10', '2020-07-14 13:53:10');
/*!40000 ALTER TABLE `tb_beneficio` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_categoria
DROP TABLE IF EXISTS `tb_categoria`;
CREATE TABLE IF NOT EXISTS `tb_categoria` (
  `id_categoria` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `ativo` bit(1) NOT NULL DEFAULT b'1',
  `dt_inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dt_alteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_categoria: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_categoria` DISABLE KEYS */;
INSERT INTO `tb_categoria` (`id_categoria`, `nome`, `descricao`, `ativo`, `dt_inclusao`, `dt_alteracao`) VALUES
	(1, 'Bronze', '', b'1', '2020-07-17 20:14:01', '2020-07-17 20:14:01'),
	(2, 'Silver', '', b'1', '2020-07-17 20:14:01', '2020-07-17 20:14:01'),
	(3, 'Gold', '', b'1', '2020-07-17 20:14:01', '2020-07-17 20:14:01'),
	(4, 'Platinum', '', b'1', '2020-07-17 20:14:01', '2020-07-17 20:14:01');
/*!40000 ALTER TABLE `tb_categoria` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_configuracao_emails
DROP TABLE IF EXISTS `tb_configuracao_emails`;
CREATE TABLE IF NOT EXISTS `tb_configuracao_emails` (
  `id_configuracao_email` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(150) NOT NULL DEFAULT '',
  `mensagem` varchar(5000) NOT NULL DEFAULT '',
  `fl_ativo` bit(1) NOT NULL DEFAULT b'0',
  `dt_alteracao` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_configuracao_email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_configuracao_emails: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_configuracao_emails` DISABLE KEYS */;
INSERT INTO `tb_configuracao_emails` (`id_configuracao_email`, `titulo`, `mensagem`, `fl_ativo`, `dt_alteracao`) VALUES
	(1, 'Sua senha de acesso para acessar o portal +Fidelidade Concessionários', 'Sua senha para acessar o sistema +Fidelidade Concessionários no portal concessionarios-santanderfinanc.com.br é: {0}', b'1', '2020-07-14 14:06:00'),
	(2, 'Sua senha de acesso para acessar o portal +Fidelidade Concessionários', 'Sua senha foi alterada', b'1', '2020-07-14 15:49:00');
/*!40000 ALTER TABLE `tb_configuracao_emails` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_criterio
DROP TABLE IF EXISTS `tb_criterio`;
CREATE TABLE IF NOT EXISTS `tb_criterio` (
  `id_criterio` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_entidade` int(10) unsigned NOT NULL,
  `nome` varchar(50) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `tooltip` varchar(100) NOT NULL,
  `ativo` bit(1) NOT NULL DEFAULT b'1',
  `dt_inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dt_alteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_criterio`),
  UNIQUE KEY `uq_id_entidade_criterio` (`id_entidade`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_criterio: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_criterio` DISABLE KEYS */;
INSERT INTO `tb_criterio` (`id_criterio`, `id_entidade`, `nome`, `descricao`, `tooltip`, `ativo`, `dt_inclusao`, `dt_alteracao`) VALUES
	(1, 1, 'Market Share', '', '', b'1', '2020-07-14 13:53:55', '2020-07-14 13:53:55'),
	(2, 2, 'IP Seguros', '', '', b'1', '2020-07-14 13:53:55', '2020-07-14 13:53:55');
/*!40000 ALTER TABLE `tb_criterio` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_empresa
DROP TABLE IF EXISTS `tb_empresa`;
CREATE TABLE IF NOT EXISTS `tb_empresa` (
  `id_empresa` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cnpj` char(14) NOT NULL,
  `nome_fantasia` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `matricula_gr` int(10) unsigned NOT NULL,
  `matricula_gc` int(10) unsigned NOT NULL,
  `ativo` bit(1) NOT NULL DEFAULT b'1',
  `dt_inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dt_alteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_empresa`),
  KEY `fk_tb_gerente__empresa___matricula_gr` (`matricula_gr`),
  KEY `fk_tb_gerente__empresa___matricula_gc` (`matricula_gc`),
  CONSTRAINT `fk_tb_gerente__empresa___matricula_gc` FOREIGN KEY (`matricula_gc`) REFERENCES `tb_gerente` (`matricula`),
  CONSTRAINT `fk_tb_gerente__empresa___matricula_gr` FOREIGN KEY (`matricula_gr`) REFERENCES `tb_gerente` (`matricula`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_empresa: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_empresa` DISABLE KEYS */;
INSERT INTO `tb_empresa` (`id_empresa`, `cnpj`, `nome_fantasia`, `email`, `matricula_gr`, `matricula_gc`, `ativo`, `dt_inclusao`, `dt_alteracao`) VALUES
	(1, '56613864000133', 'EMPRESA DO TESTER', 'empresa@teste.com', 123456, 654321, b'1', '2020-07-16 14:29:29', '2020-07-17 17:24:01'),
	(2, '39001354000196', 'EMPRESA XPG', 'empresa1@teste.com', 123456, 654321, b'1', '2020-07-16 17:30:26', '2020-07-17 17:24:03'),
	(3, '62692710000187', 'EMPRESA DAVI', 'davi.caetano@opah.com.br', 701971, 654321, b'1', '2020-07-16 17:35:28', '2020-07-17 17:24:09'),
	(5, '88738865000109', 'SEM EMAIL', '', 701971, 654321, b'1', '2020-07-17 14:26:45', '2020-07-17 14:26:46'),
	(6, '82816054000194', 'INATIVA', 'davi@opah.com', 123456, 654321, b'0', '2020-07-17 14:27:18', '2020-07-17 17:27:19');
/*!40000 ALTER TABLE `tb_empresa` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_faixa
DROP TABLE IF EXISTS `tb_faixa`;
CREATE TABLE IF NOT EXISTS `tb_faixa` (
  `id_faixa` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_criterio` int(10) DEFAULT NULL,
  `id_segmento` int(10) DEFAULT NULL,
  `nome` varchar(45) NOT NULL,
  `percentual_de` decimal(10,2) DEFAULT NULL,
  `percentual_ate` decimal(10,2) DEFAULT NULL,
  `dt_inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dt_alteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_faixa`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_faixa: ~24 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_faixa` DISABLE KEYS */;
INSERT INTO `tb_faixa` (`id_faixa`, `id_criterio`, `id_segmento`, `nome`, `percentual_de`, `percentual_ate`, `dt_inclusao`, `dt_alteracao`) VALUES
	(1, 1, 2, 'FXS1-BD', 0.00, 5.00, '2020-07-15 16:06:38', '2020-07-15 16:06:38'),
	(2, 1, 2, 'FXS2-BD', 6.00, 10.00, '2020-07-15 16:06:42', '2020-07-15 16:06:42'),
	(3, 1, 2, 'FXS3-BD', 11.00, 35.00, '2020-07-15 16:06:45', '2020-07-15 16:06:45'),
	(4, 1, 2, 'FXS4-BD', 36.00, 100.00, '2020-07-15 16:06:48', '2020-07-15 16:06:48'),
	(5, 1, 3, 'FXS1-ID', 0.00, 10.00, '2020-07-15 16:06:52', '2020-07-15 16:06:52'),
	(6, 1, 3, 'FXS2-ID', 11.00, 20.00, '2020-07-15 16:06:55', '2020-07-15 16:06:55'),
	(7, 1, 3, 'FXS3-ID', 21.00, 50.00, '2020-07-15 16:06:58', '2020-07-15 16:06:58'),
	(8, 1, 3, 'FXS4-ID', 51.00, 100.00, '2020-07-15 16:07:01', '2020-07-15 16:07:01'),
	(9, 1, 4, 'FXS1-RV', 0.00, 10.00, '2020-07-15 16:07:05', '2020-07-15 16:07:05'),
	(10, 1, 4, 'FXS2-RV', 11.00, 20.00, '2020-07-15 16:07:08', '2020-07-15 16:07:08'),
	(11, 1, 4, 'FXS3-RV', 21.00, 50.00, '2020-07-15 16:07:11', '2020-07-15 16:07:11'),
	(12, 1, 4, 'FXS4-RV', 51.00, 100.00, '2020-07-15 16:07:13', '2020-07-15 16:07:13'),
	(13, 2, 2, 'FXI1-BD', 0.00, 20.00, '2020-07-15 16:07:16', '2020-07-15 16:07:16'),
	(14, 2, 2, 'FXI2-BD', 21.00, 35.00, '2020-07-15 16:07:19', '2020-07-15 16:07:19'),
	(15, 2, 2, 'FXI3-BD', 36.00, 50.00, '2020-07-15 16:07:22', '2020-07-15 16:07:22'),
	(16, 2, 2, 'FXI4-BD', 51.00, 100.00, '2020-07-15 16:07:24', '2020-07-15 16:07:24'),
	(17, 2, 3, 'FXI1-ID', 0.00, 20.00, '2020-07-15 16:07:27', '2020-07-15 16:07:27'),
	(18, 2, 3, 'FXI2-ID', 21.00, 35.00, '2020-07-15 16:07:29', '2020-07-15 16:07:29'),
	(19, 2, 3, 'FXI3-ID', 36.00, 50.00, '2020-07-15 16:07:32', '2020-07-15 16:07:32'),
	(20, 2, 3, 'FXI4-ID', 51.00, 100.00, '2020-07-15 16:07:35', '2020-07-15 16:07:35'),
	(21, 2, 4, 'FXI1-RV', 0.00, 25.00, '2020-07-15 16:07:38', '2020-07-15 16:07:38'),
	(22, 2, 4, 'FXI2-RV', 26.00, 50.00, '2020-07-15 16:07:41', '2020-07-15 16:07:41'),
	(23, 2, 4, 'FXI3-RV', 51.00, 75.00, '2020-07-15 16:07:44', '2020-07-15 16:07:44'),
	(24, 2, 4, 'FXI4-RV', 76.00, 100.00, '2020-07-15 16:07:46', '2020-07-15 16:07:46');
/*!40000 ALTER TABLE `tb_faixa` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_faq
DROP TABLE IF EXISTS `tb_faq`;
CREATE TABLE IF NOT EXISTS `tb_faq` (
  `id_faq` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_entidade` int(10) unsigned DEFAULT NULL,
  `pergunta` varchar(500) DEFAULT NULL,
  `resposta` varchar(500) DEFAULT NULL,
  `dt_inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dt_alteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_faq`),
  UNIQUE KEY `uq_criterio` (`id_entidade`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_faq: ~10 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_faq` DISABLE KEYS */;
INSERT INTO `tb_faq` (`id_faq`, `id_entidade`, `pergunta`, `resposta`, `dt_inclusao`, `dt_alteracao`) VALUES
	(11, 1, 'O que é o programa?', 'O +Fidelidade é um programa que tem como objetivo ofertas aos parceiros os melhores benefícios de acordo com a matriz de fidelização, desafios e classificação.', '2020-07-14 17:22:53', '2020-07-14 17:22:53'),
	(12, 2, 'Como funciona a Matriz de Fidelização?', 'De acordo com a performance combinada de Market Share e IPS, o grupo é posicionado na Matriz para definir sua classificação mensal e a comissão média.', '2020-07-14 17:23:02', '2020-07-14 17:23:02'),
	(13, 3, 'O que é a Classificação?', 'A classificação define se a loja é fidelização conosco, respectivamente, Platinum, Gold, Silver ou Bronze. Assim, quanto melhor a classificação, melhor os benefícios e possibilidade de participar dos desafios.', '2020-07-14 17:23:06', '2020-07-14 17:23:06'),
	(14, 4, 'Como apurar o indicador de Market Share?', 'O percentual de Market Share que compõe a Matriz de Fidelização é definida pelas vendas com gravame no Santander sobre as vendas com gravame na Concessionária, conforme referência Cetip - B3.', '2020-07-14 17:23:18', '2020-07-14 17:23:18'),
	(15, 5, 'Como apurar o indicador de IPS?', 'O Índice de Penetração de Seguros (IPS) é a quantidade de contratos com seguro(s) sobre contratos elegíveis aos produtos com multiplicador (x2) caso o contrato tenha ambos os seguros (Auto e prestamista).', '2020-07-14 17:23:24', '2020-07-14 17:23:24'),
	(16, 6, 'Como será a disponibilização da comissão +fidelidade?', 'Será disponibilizada em até D+15 após o fechamento mensal do programa. O cadastro será retroativo, ou seja, percentual de comissão com referência a produção do mês anterior.', '2020-07-14 17:23:28', '2020-07-14 17:23:28'),
	(17, 7, 'Como será a disponibilização das comissões de seguros?', 'Será disponibilizada em D+5 após o fechamento mensal do programa. O cadastro valerá para o mês vigente, ou seja, não retroativo, percentual de comissão com referência a produção do mês atual.', '2020-07-14 17:23:33', '2020-07-14 17:23:33'),
	(18, 8, 'Qual a periodicidade de atualização do programa +fidelidade?', 'A atualização acontecerá as quintas com referência do apurado da semana anterior.', '2020-07-14 17:23:37', '2020-07-14 17:23:37'),
	(19, 9, 'Quais outros benefícios fazem parte do programa +fidelidade?', 'Parceiros Platinum ou Gold tem a opção dos benefícios de pagamento com pendência de gravame e pendência de documento (CRV).', '2020-07-14 17:23:41', '2020-07-14 17:23:41'),
	(20, 10, 'O que é a Mesa de Crédito?', 'A Mesa de Crédito é um benefício do Platinum para negociar crédito, taxa e prazo da proposta.', '2020-07-14 17:23:44', '2020-07-14 17:23:44');
/*!40000 ALTER TABLE `tb_faq` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_gerente
DROP TABLE IF EXISTS `tb_gerente`;
CREATE TABLE IF NOT EXISTS `tb_gerente` (
  `id_gerente` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `matricula` int(10) unsigned NOT NULL,
  `id_tipo_gerente` int(10) unsigned NOT NULL,
  `nome` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `telefone` varchar(11) NOT NULL,
  `ativo` bit(1) NOT NULL DEFAULT b'1',
  `dt_inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dt_alteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_gerente`),
  UNIQUE KEY `uq_matricula_gerente` (`matricula`),
  KEY `fk_tb_tipo_gerente__gerente` (`id_tipo_gerente`),
  CONSTRAINT `fk_tb_tipo_gerente__gerente` FOREIGN KEY (`id_tipo_gerente`) REFERENCES `tb_tipo_gerente` (`id_tipo_gerente`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_gerente: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_gerente` DISABLE KEYS */;
INSERT INTO `tb_gerente` (`id_gerente`, `matricula`, `id_tipo_gerente`, `nome`, `email`, `telefone`, `ativo`, `dt_inclusao`, `dt_alteracao`) VALUES
	(1, 701971, 1, 'ANA PAULA BASTELLI BELON', 'ABELON@SANTANDER.COM.BR', '13991748190', b'1', '2020-07-16 12:31:17', '2020-07-16 12:31:17'),
	(2, 729367, 2, 'RHAFAEL TONIER MATOS', 'RHAFAEL.MATOS@SANTANDER.COM.BR', '1390904455', b'1', '2020-07-16 12:31:17', '2020-07-16 12:31:17'),
	(3, 123456, 1, 'EU GERENTE RELACIONAMENTO', 'gerente@relacionamento.com', '', b'1', '2020-07-16 17:28:03', '2020-07-16 17:28:03'),
	(4, 654321, 2, 'EU GERENTE COMERCIAL', 'gerente@comercial.com', '', b'1', '2020-07-16 17:28:33', '2020-07-16 17:28:33');
/*!40000 ALTER TABLE `tb_gerente` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_indicador_performance
DROP TABLE IF EXISTS `tb_indicador_performance`;
CREATE TABLE IF NOT EXISTS `tb_indicador_performance` (
  `id_indicador_performance` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_entidade` int(10) unsigned DEFAULT NULL,
  `nome` varchar(50) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `tooltip` varchar(100) NOT NULL,
  `ativo` bit(1) NOT NULL DEFAULT b'1',
  `dt_inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dt_alteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_tipo_indicador` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_indicador_performance`),
  UNIQUE KEY `uq_indicador_performance` (`id_entidade`),
  KEY `FK_tb_indicador_performance_tb_tipo_indicador_performance` (`id_tipo_indicador`),
  CONSTRAINT `FK_tb_indicador_performance_tb_tipo_indicador_performance` FOREIGN KEY (`id_tipo_indicador`) REFERENCES `tb_tipo_indicador_performance` (`id_tipo_indicador_performance`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_indicador_performance: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_indicador_performance` DISABLE KEYS */;
INSERT INTO `tb_indicador_performance` (`id_indicador_performance`, `id_entidade`, `nome`, `descricao`, `tooltip`, `ativo`, `dt_inclusao`, `dt_alteracao`, `id_tipo_indicador`) VALUES
	(1, 10, 'Financiamento', '', '', b'1', '2020-07-22 11:24:00', '2020-07-22 11:24:03', 2),
	(2, 20, 'Seguro Auto', '', '', b'1', '2020-07-22 11:24:23', '2020-07-22 11:24:23', 1),
	(3, 30, 'Seguro Prestamista', '', '', b'1', '2020-07-22 11:24:40', '2020-07-22 11:24:40', 1);
/*!40000 ALTER TABLE `tb_indicador_performance` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_indicador_performance_criterios
DROP TABLE IF EXISTS `tb_indicador_performance_criterios`;
CREATE TABLE IF NOT EXISTS `tb_indicador_performance_criterios` (
  `id_indicador_performance_criterios` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_indicador` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `ativo` bit(1) NOT NULL DEFAULT b'1',
  `dt_inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dt_alteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_indicador_performance_criterios`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_indicador_performance_criterios: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_indicador_performance_criterios` DISABLE KEYS */;
INSERT INTO `tb_indicador_performance_criterios` (`id_indicador_performance_criterios`, `id_indicador`, `nome`, `ativo`, `dt_inclusao`, `dt_alteracao`) VALUES
	(1, 1, 'Santander', b'1', '2020-07-22 11:32:06', '2020-07-22 11:32:06'),
	(2, 1, 'Concessionárias', b'1', '2020-07-22 11:32:21', '2020-07-22 11:32:21'),
	(3, 2, 'Contratos elegíveis', b'1', '2020-07-22 11:32:34', '2020-07-22 11:32:34'),
	(4, 2, 'Contratos convertidos', b'1', '2020-07-22 11:32:44', '2020-07-22 11:32:51'),
	(5, 3, 'Contratos elegíveis', b'1', '2020-07-22 11:33:11', '2020-07-22 11:33:11'),
	(6, 3, 'Contratos converttidos', b'1', '2020-07-22 11:33:22', '2020-07-22 11:33:22');
/*!40000 ALTER TABLE `tb_indicador_performance_criterios` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_log_acesso
DROP TABLE IF EXISTS `tb_log_acesso`;
CREATE TABLE IF NOT EXISTS `tb_log_acesso` (
  `id_log_acesso` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_usuario` int(10) unsigned NOT NULL,
  `id_perfil` int(10) unsigned NOT NULL,
  `dt_inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_log_acesso`) USING BTREE,
  KEY `fk_id_usuario` (`id_usuario`),
  KEY `fk_id_perfil` (`id_perfil`),
  CONSTRAINT `fk_id_perfil` FOREIGN KEY (`id_perfil`) REFERENCES `tb_perfil` (`id_perfil`),
  CONSTRAINT `fk_id_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `tb_usuario` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_log_acesso: ~8 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_log_acesso` DISABLE KEYS */;
INSERT INTO `tb_log_acesso` (`id_log_acesso`, `id_usuario`, `id_perfil`, `dt_inclusao`) VALUES
	(1, 14, 4, '2020-07-20 16:46:40'),
	(2, 14, 4, '2020-07-20 16:49:57'),
	(3, 14, 4, '2020-07-20 16:49:57'),
	(4, 9, 2, '2020-07-20 16:49:57'),
	(5, 8, 2, '2020-07-20 16:49:57'),
	(6, 7, 3, '2020-07-20 16:49:57'),
	(7, 7, 3, '2020-07-20 16:49:57'),
	(8, 14, 4, '2020-07-21 12:58:43');
/*!40000 ALTER TABLE `tb_log_acesso` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_log_senha
DROP TABLE IF EXISTS `tb_log_senha`;
CREATE TABLE IF NOT EXISTS `tb_log_senha` (
  `id_log_senha` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_usuario` int(10) unsigned NOT NULL,
  `hash_senha` varchar(150) NOT NULL,
  `dt_inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fl_recuperar_senha` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id_log_senha`),
  KEY `fk_id_usuario1` (`id_usuario`),
  CONSTRAINT `fk_id_usuario1` FOREIGN KEY (`id_usuario`) REFERENCES `tb_usuario` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_log_senha: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_log_senha` DISABLE KEYS */;
INSERT INTO `tb_log_senha` (`id_log_senha`, `id_usuario`, `hash_senha`, `dt_inclusao`, `fl_recuperar_senha`) VALUES
	(1, 14, 'ZDA25LU5lllEYvwm9nR3vg==', '2020-07-20 17:19:23', b'1');
/*!40000 ALTER TABLE `tb_log_senha` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_matriz
DROP TABLE IF EXISTS `tb_matriz`;
CREATE TABLE IF NOT EXISTS `tb_matriz` (
  `id_matriz` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_faixa_vertical` int(10) unsigned NOT NULL,
  `id_faixa_horizontal` int(10) unsigned NOT NULL,
  `id_categoria` int(10) unsigned NOT NULL,
  `dt_inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dt_alteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_matriz`),
  KEY `fk_faixahorizontal__matriz` (`id_faixa_horizontal`),
  KEY `fk_faixavertical__matriz` (`id_faixa_vertical`),
  KEY `fk_categoria__matriz` (`id_categoria`),
  CONSTRAINT `fk_categoria__matriz` FOREIGN KEY (`id_categoria`) REFERENCES `tb_categoria` (`id_categoria`),
  CONSTRAINT `fk_faixahorizontal__matriz` FOREIGN KEY (`id_faixa_horizontal`) REFERENCES `tb_faixa` (`id_faixa`),
  CONSTRAINT `fk_faixavertical__matriz` FOREIGN KEY (`id_faixa_vertical`) REFERENCES `tb_faixa` (`id_faixa`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_matriz: ~48 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_matriz` DISABLE KEYS */;
INSERT INTO `tb_matriz` (`id_matriz`, `id_faixa_vertical`, `id_faixa_horizontal`, `id_categoria`, `dt_inclusao`, `dt_alteracao`) VALUES
	(1, 1, 13, 1, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(2, 2, 13, 2, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(3, 3, 13, 2, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(4, 4, 13, 2, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(5, 1, 14, 1, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(6, 2, 14, 2, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(7, 3, 14, 3, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(8, 4, 14, 3, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(9, 1, 15, 1, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(10, 2, 15, 2, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(11, 3, 15, 3, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(12, 4, 15, 4, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(13, 1, 16, 1, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(14, 2, 16, 2, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(15, 3, 16, 4, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(16, 4, 16, 4, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(17, 5, 17, 1, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(18, 6, 17, 2, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(19, 7, 17, 2, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(20, 8, 17, 2, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(21, 5, 18, 1, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(22, 6, 18, 2, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(23, 7, 18, 3, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(24, 8, 18, 3, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(25, 5, 19, 1, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(26, 6, 19, 2, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(27, 7, 19, 3, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(28, 8, 19, 4, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(29, 5, 20, 1, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(30, 6, 20, 2, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(31, 7, 20, 4, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(32, 8, 20, 4, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(33, 9, 21, 1, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(34, 10, 21, 2, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(35, 11, 21, 2, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(36, 12, 21, 2, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(37, 9, 22, 1, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(38, 10, 22, 2, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(39, 11, 22, 3, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(40, 12, 22, 3, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(41, 9, 23, 1, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(42, 10, 23, 2, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(43, 11, 23, 3, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(44, 12, 23, 4, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(45, 9, 24, 1, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(46, 10, 24, 2, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(47, 11, 24, 4, '2020-07-20 22:56:49', '2020-07-20 22:56:49'),
	(48, 12, 24, 4, '2020-07-20 22:56:49', '2020-07-20 22:56:49');
/*!40000 ALTER TABLE `tb_matriz` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_matriz_beneficio
DROP TABLE IF EXISTS `tb_matriz_beneficio`;
CREATE TABLE IF NOT EXISTS `tb_matriz_beneficio` (
  `id_matriz_beneficio` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_matriz` int(10) unsigned NOT NULL,
  `id_beneficio` int(10) unsigned NOT NULL,
  `valor` varchar(50) NOT NULL,
  `dt_inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dt_alteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_matriz_beneficio`),
  KEY `fk_matrizbeneficio__matriz` (`id_matriz`),
  KEY `fk_matrizbeneficio__beneficio` (`id_beneficio`),
  CONSTRAINT `fk_matrizbeneficio__beneficio` FOREIGN KEY (`id_beneficio`) REFERENCES `tb_beneficio` (`id_beneficio`),
  CONSTRAINT `fk_matrizbeneficio__matriz` FOREIGN KEY (`id_matriz`) REFERENCES `tb_matriz` (`id_matriz`)
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_matriz_beneficio: ~144 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_matriz_beneficio` DISABLE KEYS */;
INSERT INTO `tb_matriz_beneficio` (`id_matriz_beneficio`, `id_matriz`, `id_beneficio`, `valor`, `dt_inclusao`, `dt_alteracao`) VALUES
	(1, 1, 2, '0,017', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(2, 17, 2, '0,017', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(3, 33, 2, '0,017', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(4, 1, 4, '0,04', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(5, 17, 4, '0,04', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(6, 33, 4, '0,04', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(7, 1, 5, '0,04', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(8, 17, 5, '0,04', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(9, 33, 5, '0,04', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(10, 2, 2, '0,017', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(11, 18, 2, '0,017', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(12, 34, 2, '0,017', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(13, 2, 4, '0,05', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(14, 18, 4, '0,05', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(15, 34, 4, '0,05', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(16, 2, 5, '0,05', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(17, 18, 5, '0,05', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(18, 34, 5, '0,05', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(19, 3, 2, '0,017', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(20, 19, 2, '0,017', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(21, 35, 2, '0,017', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(22, 3, 4, '0,05', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(23, 19, 4, '0,05', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(24, 35, 4, '0,05', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(25, 3, 5, '0,05', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(26, 19, 5, '0,05', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(27, 35, 5, '0,05', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(28, 4, 2, '0,017', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(29, 20, 2, '0,017', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(30, 36, 2, '0,017', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(31, 4, 4, '0,06', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(32, 20, 4, '0,06', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(33, 36, 4, '0,06', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(34, 4, 5, '0,06', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(35, 20, 5, '0,06', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(36, 36, 5, '0,06', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(37, 5, 2, '0,019', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(38, 21, 2, '0,019', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(39, 37, 2, '0,019', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(40, 5, 4, '0,07', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(41, 21, 4, '0,07', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(42, 37, 4, '0,07', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(43, 5, 5, '0,07', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(44, 21, 5, '0,07', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(45, 37, 5, '0,07', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(46, 6, 2, '0,019', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(47, 22, 2, '0,019', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(48, 38, 2, '0,019', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(49, 6, 4, '0,08', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(50, 22, 4, '0,08', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(51, 38, 4, '0,08', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(52, 6, 5, '0,07', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(53, 22, 5, '0,07', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(54, 38, 5, '0,07', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(55, 7, 2, '0,020', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(56, 23, 2, '0,020', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(57, 39, 2, '0,020', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(58, 7, 4, '0,09', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(59, 23, 4, '0,09', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(60, 39, 4, '0,09', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(61, 7, 5, '0,08', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(62, 23, 5, '0,08', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(63, 39, 5, '0,08', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(64, 8, 2, '0,020', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(65, 24, 2, '0,020', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(66, 40, 2, '0,020', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(67, 8, 4, '0,1', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(68, 24, 4, '0,1', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(69, 40, 4, '0,1', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(70, 8, 5, '0,08', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(71, 24, 5, '0,08', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(72, 40, 5, '0,08', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(73, 9, 2, '0,021', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(74, 25, 2, '0,021', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(75, 41, 2, '0,021', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(76, 9, 4, '0,09', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(77, 25, 4, '0,09', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(78, 41, 4, '0,09', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(79, 9, 5, '0,08', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(80, 25, 5, '0,08', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(81, 41, 5, '0,08', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(82, 10, 2, '0,024', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(83, 26, 2, '0,024', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(84, 42, 2, '0,024', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(85, 10, 4, '0,14', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(86, 26, 4, '0,14', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(87, 42, 4, '0,14', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(88, 10, 5, '0,12', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(89, 26, 5, '0,12', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(90, 42, 5, '0,12', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(91, 11, 2, '0,024', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(92, 27, 2, '0,024', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(93, 43, 2, '0,024', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(94, 11, 4, '0,15', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(95, 27, 4, '0,15', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(96, 43, 4, '0,15', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(97, 11, 5, '0,13', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(98, 27, 5, '0,13', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(99, 43, 5, '0,13', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(100, 12, 2, '0,024', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(101, 28, 2, '0,024', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(102, 44, 2, '0,024', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(103, 12, 4, '0,28', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(104, 28, 4, '0,28', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(105, 44, 4, '0,28', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(106, 12, 5, '0,19', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(107, 28, 5, '0,19', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(108, 44, 5, '0,19', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(109, 13, 2, '0,020', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(110, 29, 2, '0,020', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(111, 45, 2, '0,020', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(112, 13, 4, '0,09', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(113, 29, 4, '0,09', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(114, 45, 4, '0,09', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(115, 13, 5, '0,09', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(116, 29, 5, '0,09', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(117, 45, 5, '0,09', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(118, 14, 2, '0,024', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(119, 30, 2, '0,024', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(120, 46, 2, '0,024', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(121, 14, 4, '0,16', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(122, 30, 4, '0,16', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(123, 46, 4, '0,16', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(124, 14, 5, '0,13', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(125, 30, 5, '0,13', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(126, 46, 5, '0,13', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(127, 15, 2, '0,024', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(128, 31, 2, '0,024', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(129, 47, 2, '0,024', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(130, 15, 4, '0,3', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(131, 31, 4, '0,3', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(132, 47, 4, '0,3', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(133, 15, 5, '0,19', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(134, 31, 5, '0,19', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(135, 47, 5, '0,19', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(136, 16, 2, '0,024', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(137, 32, 2, '0,024', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(138, 48, 2, '0,024', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(139, 16, 4, '0,35', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(140, 32, 4, '0,35', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(141, 48, 4, '0,35', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(142, 16, 5, '0,19', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(143, 32, 5, '0,19', '2020-07-22 13:14:39', '2020-07-22 13:14:39'),
	(144, 48, 5, '0,19', '2020-07-22 13:14:39', '2020-07-22 13:14:39');
/*!40000 ALTER TABLE `tb_matriz_beneficio` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_novidades
DROP TABLE IF EXISTS `tb_novidades`;
CREATE TABLE IF NOT EXISTS `tb_novidades` (
  `id_novidade` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(150) NOT NULL,
  `descricao` text NOT NULL,
  `dt_inicio` timestamp NULL DEFAULT NULL,
  `dt_fim` timestamp NULL DEFAULT NULL,
  `dt_inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dt_alteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_usuario_envio` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_novidade`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_novidades: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_novidades` DISABLE KEYS */;
INSERT INTO `tb_novidades` (`id_novidade`, `titulo`, `descricao`, `dt_inicio`, `dt_fim`, `dt_inclusao`, `dt_alteracao`, `id_usuario_envio`) VALUES
	(1, 'Boas Vindas', 'Era uma vez no México', '2020-07-23 08:07:16', '2020-08-23 08:07:18', '2020-07-23 08:07:23', '2020-07-23 08:07:24', NULL),
	(2, 'O que podemos fazer por você hoje?', 'Diga o que quiser que faremos', '2020-07-23 08:07:50', '2020-07-28 08:07:52', '2020-07-23 11:07:49', '2020-07-23 11:07:59', NULL),
	(3, 'Lorem Ipsum', '<p>O que foi?</p><p>Nada!</p>', '2020-07-23 08:08:26', '2020-09-23 08:08:27', '2020-07-23 11:08:34', '2020-07-23 11:08:39', NULL),
	(4, 'Santander Financ', 'Lançamento do novo produto', NULL, NULL, '2020-07-23 11:09:00', '2020-07-23 11:09:00', NULL);
/*!40000 ALTER TABLE `tb_novidades` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_novidade_usuario
DROP TABLE IF EXISTS `tb_novidade_usuario`;
CREATE TABLE IF NOT EXISTS `tb_novidade_usuario` (
  `id_novidade_usuario` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_novidade` int(11) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `dt_leitura` timestamp NULL DEFAULT NULL,
  `ordem_exibicao` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_novidade_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_novidade_usuario: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_novidade_usuario` DISABLE KEYS */;
INSERT INTO `tb_novidade_usuario` (`id_novidade_usuario`, `id_novidade`, `id_usuario`, `dt_leitura`, `ordem_exibicao`) VALUES
	(1, 1, 14, '2020-07-23 12:10:34', 2),
	(2, 2, 14, '2020-07-23 12:11:06', 1),
	(3, 3, 14, NULL, NULL),
	(4, 4, 14, NULL, NULL);
/*!40000 ALTER TABLE `tb_novidade_usuario` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_perfil
DROP TABLE IF EXISTS `tb_perfil`;
CREATE TABLE IF NOT EXISTS `tb_perfil` (
  `id_perfil` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `fl_ativo` bit(1) NOT NULL DEFAULT b'0',
  `dt_alteracao` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_perfil`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_perfil: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_perfil` DISABLE KEYS */;
INSERT INTO `tb_perfil` (`id_perfil`, `nome`, `fl_ativo`, `dt_alteracao`) VALUES
	(1, 'Administrador', b'1', '2020-07-06 11:50:00'),
	(2, 'GC', b'1', '2020-07-10 10:10:01'),
	(3, 'GR', b'1', '2020-07-10 10:10:01'),
	(4, 'Lojista', b'1', '2020-07-17 10:44:00');
/*!40000 ALTER TABLE `tb_perfil` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_perfil_permissao
DROP TABLE IF EXISTS `tb_perfil_permissao`;
CREATE TABLE IF NOT EXISTS `tb_perfil_permissao` (
  `id_perfil_permissao` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_perfil` int(11) NOT NULL DEFAULT '0',
  `id_permissao` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_perfil_permissao`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_perfil_permissao: ~20 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_perfil_permissao` DISABLE KEYS */;
INSERT INTO `tb_perfil_permissao` (`id_perfil_permissao`, `id_perfil`, `id_permissao`) VALUES
	(1, 1, 1),
	(2, 1, 6),
	(3, 2, 1),
	(4, 2, 2),
	(5, 2, 3),
	(6, 2, 4),
	(7, 2, 5),
	(8, 2, 6),
	(9, 3, 1),
	(10, 3, 2),
	(11, 3, 3),
	(12, 3, 4),
	(13, 3, 5),
	(14, 3, 6),
	(15, 4, 1),
	(16, 4, 2),
	(17, 4, 3),
	(18, 4, 4),
	(19, 4, 5),
	(20, 4, 6);
/*!40000 ALTER TABLE `tb_perfil_permissao` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_permissao
DROP TABLE IF EXISTS `tb_permissao`;
CREATE TABLE IF NOT EXISTS `tb_permissao` (
  `id_permissao` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `dt_alteracao` datetime NOT NULL,
  `fl_ativo` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id_permissao`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_permissao: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_permissao` DISABLE KEYS */;
INSERT INTO `tb_permissao` (`id_permissao`, `nome`, `dt_alteracao`, `fl_ativo`) VALUES
	(1, 'Home', '2020-07-07 14:55:45', b'1'),
	(2, 'Simulador', '2020-07-07 14:56:24', b'1'),
	(3, 'Classificação', '2020-07-20 07:44:00', b'1'),
	(4, 'Mensagens', '2020-07-20 07:44:46', b'1'),
	(5, 'Analítico', '2020-07-20 07:45:17', b'1'),
	(6, 'FAQ', '2020-07-20 07:46:05', b'1');
/*!40000 ALTER TABLE `tb_permissao` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_segmento
DROP TABLE IF EXISTS `tb_segmento`;
CREATE TABLE IF NOT EXISTS `tb_segmento` (
  `id_segmento` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_segmento_pai` int(10) unsigned DEFAULT NULL,
  `nome` varchar(45) NOT NULL,
  `codigo_santander` int(10) unsigned DEFAULT NULL,
  `ativo` tinyint(4) NOT NULL,
  `dt_inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dt_alteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_segmento`),
  KEY `fk_segmento_segmento1_idx` (`id_segmento_pai`),
  CONSTRAINT `fk_segmento_segmento1` FOREIGN KEY (`id_segmento_pai`) REFERENCES `tb_segmento` (`id_segmento`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_segmento: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_segmento` DISABLE KEYS */;
INSERT INTO `tb_segmento` (`id_segmento`, `id_segmento_pai`, `nome`, `codigo_santander`, `ativo`, `dt_inclusao`, `dt_alteracao`) VALUES
	(1, NULL, 'REVENDAS', NULL, 1, '2020-07-17 23:45:58', '2020-07-17 23:45:58'),
	(2, 1, 'BIG DEALERS', 149, 1, '2020-07-17 23:46:20', '2020-07-17 23:46:20'),
	(3, 1, 'INTERMEDIATE DEALERS', 141, 1, '2020-07-17 23:46:20', '2020-07-17 23:46:20'),
	(4, 1, 'REVENDAS VAREJO', 140, 1, '2020-07-17 23:46:20', '2020-07-17 23:46:20');
/*!40000 ALTER TABLE `tb_segmento` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_template
DROP TABLE IF EXISTS `tb_template`;
CREATE TABLE IF NOT EXISTS `tb_template` (
  `id_template` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_entidade` int(10) unsigned DEFAULT NULL,
  `template` varchar(45) DEFAULT NULL,
  `dt_inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dt_alteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_template`),
  UNIQUE KEY `uq_criterio` (`id_entidade`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_template: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_template` DISABLE KEYS */;
INSERT INTO `tb_template` (`id_template`, `id_entidade`, `template`, `dt_inclusao`, `dt_alteracao`) VALUES
	(1, 1, '<p class=\'mb16\'> {resposta} </p></div>', '2020-07-14 17:22:46', '2020-07-14 17:22:46');
/*!40000 ALTER TABLE `tb_template` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_tipo_gerente
DROP TABLE IF EXISTS `tb_tipo_gerente`;
CREATE TABLE IF NOT EXISTS `tb_tipo_gerente` (
  `id_tipo_gerente` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `sigla` varchar(5) NOT NULL,
  `dt_inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dt_alteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_tipo_gerente`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_tipo_gerente: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_tipo_gerente` DISABLE KEYS */;
INSERT INTO `tb_tipo_gerente` (`id_tipo_gerente`, `nome`, `sigla`, `dt_inclusao`, `dt_alteracao`) VALUES
	(1, 'Gerente de Relacionamento', 'GR', '2020-07-15 21:58:56', '2020-07-15 21:58:56'),
	(2, 'Gerente Comercial', 'GC', '2020-07-15 21:58:56', '2020-07-15 21:58:56');
/*!40000 ALTER TABLE `tb_tipo_gerente` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_tipo_indicador_performance
DROP TABLE IF EXISTS `tb_tipo_indicador_performance`;
CREATE TABLE IF NOT EXISTS `tb_tipo_indicador_performance` (
  `id_tipo_indicador_performance` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `ativo` bit(1) NOT NULL DEFAULT b'1',
  `dt_inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dt_alteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_tipo_indicador_performance`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_tipo_indicador_performance: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_tipo_indicador_performance` DISABLE KEYS */;
INSERT INTO `tb_tipo_indicador_performance` (`id_tipo_indicador_performance`, `nome`, `ativo`, `dt_inclusao`, `dt_alteracao`) VALUES
	(1, 'Com Percentual', b'1', '2020-07-22 11:23:09', '2020-07-22 11:23:09'),
	(2, 'Sem Percentual', b'1', '2020-07-22 11:23:19', '2020-07-22 11:23:19');
/*!40000 ALTER TABLE `tb_tipo_indicador_performance` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_usuario
DROP TABLE IF EXISTS `tb_usuario`;
CREATE TABLE IF NOT EXISTS `tb_usuario` (
  `id_usuario` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) NOT NULL,
  `email` varchar(250) NOT NULL,
  `hash_senha` varchar(250) NOT NULL,
  `id_empresa` int(11) DEFAULT NULL,
  `dt_cadastro` datetime NOT NULL,
  `dt_alteracao` datetime NOT NULL,
  `fl_trocar_senha` bit(1) NOT NULL DEFAULT b'0',
  `fl_ativo` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_usuario: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_usuario` DISABLE KEYS */;
INSERT INTO `tb_usuario` (`id_usuario`, `nome`, `email`, `hash_senha`, `id_empresa`, `dt_cadastro`, `dt_alteracao`, `fl_trocar_senha`, `fl_ativo`) VALUES
	(6, 'ANA PAULA BASTELLI BELON', 'ABELON@SANTANDER.COM.BR', 'NADA', NULL, '2020-07-16 17:40:21', '2020-07-16 17:40:21', b'0', b'1'),
	(7, 'EU GERENTE RELACIONAMENTO', 'gerente@relacionamento.com', 'NADA', NULL, '2020-07-16 17:40:21', '2020-07-16 17:40:21', b'0', b'1'),
	(8, 'EU GERENTE COMERCIAL', 'gerente@comercial.com', 'NADA', NULL, '2020-07-16 17:40:21', '2020-07-16 17:40:21', b'0', b'1'),
	(9, 'RHAFAEL TONIER MATOS', 'RHAFAEL.MATOS@SANTANDER.COM.BR', 'NADA', NULL, '2020-07-16 17:40:21', '2020-07-16 17:40:21', b'0', b'1'),
	(14, '', 'davi.caetano@opah.com.br', 'bUQoawGZAa8sn8jLMJac9Q==', 3, '2020-07-17 18:19:07', '2020-07-20 17:19:20', b'1', b'1');
/*!40000 ALTER TABLE `tb_usuario` ENABLE KEYS */;

-- Copiando estrutura para tabela db_concessionaria.tb_usuario_perfil
DROP TABLE IF EXISTS `tb_usuario_perfil`;
CREATE TABLE IF NOT EXISTS `tb_usuario_perfil` (
  `id_usuario_perfil` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `id_perfil` int(11) NOT NULL,
  PRIMARY KEY (`id_usuario_perfil`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_concessionaria.tb_usuario_perfil: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_usuario_perfil` DISABLE KEYS */;
INSERT INTO `tb_usuario_perfil` (`id_usuario_perfil`, `id_usuario`, `id_perfil`) VALUES
	(2, 6, 3),
	(3, 7, 3),
	(4, 8, 2),
	(5, 9, 2),
	(8, 14, 4);
/*!40000 ALTER TABLE `tb_usuario_perfil` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
