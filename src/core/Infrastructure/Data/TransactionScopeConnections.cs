﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace maisfidelidade.cnc.fo.api.core.Infrastructure.Data
{
    public class TransactionScopeConnections
    {
        private static readonly Dictionary<Transaction, Dictionary<string, DatabaseConnectionWrapper>> transactionConnections = new Dictionary<Transaction, Dictionary<string, DatabaseConnectionWrapper>>();

        public static DatabaseConnectionWrapper ObterConexao(Database database)
        {
            Dictionary<string, DatabaseConnectionWrapper> dictionary;
            DatabaseConnectionWrapper wrapper;
            Transaction current = Transaction.Current;

            if (current == null)
            {
                return null;
            }

            lock (transactionConnections)
            {
                if (!transactionConnections.TryGetValue(current, out dictionary))
                {
                    dictionary = new Dictionary<string, DatabaseConnectionWrapper>();
                    transactionConnections.Add(current, dictionary);
                    current.TransactionCompleted += new TransactionCompletedEventHandler(OnTransactionCompleted);
                }
            }

            lock (dictionary)
            {
                if (!dictionary.TryGetValue(database.ConnectionString, out wrapper))
                {
                    wrapper = new DatabaseConnectionWrapper(database.GetNewOpenConnection());
                    dictionary.Add(database.ConnectionString, wrapper);
                }

                wrapper.AddRef();
            }

            return wrapper;
        }

        private static void OnTransactionCompleted(object sender, TransactionEventArgs e)
        {
            Dictionary<string, DatabaseConnectionWrapper> dictionary;

            lock (transactionConnections)
            {
                if (!transactionConnections.TryGetValue(e.Transaction, out dictionary))
                {
                    return;
                }

                transactionConnections.Remove(e.Transaction);
            }

            lock (dictionary)
            {
                foreach (DatabaseConnectionWrapper wrapper in dictionary.Values)
                {
                    wrapper.Dispose();
                }
            }
        }
    }
}
