﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Indicadores.Performance
{
    public class IndicadorPerformance : EntityBase, IAggregateRoot
    {
        public int IdEntidade { get; set; }
        public string Indicador { get; set; }
        public string Descricao { get; set; }
        public string Tooltip { get; set; }
        public string Valor { get; set; }
        public string Dado { get; set; }
        public string ValorDado { get; set; }
    }
}
