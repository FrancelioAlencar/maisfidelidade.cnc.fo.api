﻿using maisfidelidade.cnc.fo.api.core.Common.Utility;
using System;

namespace maisfidelidade.cnc.fo.api.core.Infrastructure.Data
{
    public static class DatabaseFactory
    {
        private static volatile Func<string, Database> createNamedDatabase;

        public static Database CreateDatabase(string name)
        {
            return GetCreateDatabase()(name);
        }

        public static void SetDatabases(Func<string, Database> createNamedDatabase, bool throwIfSet = true)
        {
            Guard.ArgumentNotNull(createNamedDatabase, nameof(createNamedDatabase));

            Func<string, Database> funcCreateNamedDatabase = DatabaseFactory.createNamedDatabase;

            //if (funcCreateNamedDatabase != null && throwIfSet)
            //{
            //    throw new InvalidOperationException("Resources.ExceptionDatabaseProviderFactoryAlreadySet");
            //}

            DatabaseFactory.createNamedDatabase = createNamedDatabase;
        }

        private static Func<string, Database> GetCreateDatabase()
        {
            Func<string, Database> createNamedDatabase = DatabaseFactory.createNamedDatabase;

            if (createNamedDatabase == null)
            {
                throw new InvalidOperationException("Resources.ExceptionDatabaseProviderFactoryNotSet");
            }

            return createNamedDatabase;
        }

    }
}
