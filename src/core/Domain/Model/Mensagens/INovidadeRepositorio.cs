﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.RepositoryFramework;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Mensagens
{
    public interface INovidadeRepositorio : IRepositorio<Novidade>
    {
        IList<Novidade> Obter(int idUsuario);
        void Lida(int idNovidadeUsuario, bool lida);
        void Pin(int idNovidadeUsuario, bool check, int idUsuario);
    }
}
