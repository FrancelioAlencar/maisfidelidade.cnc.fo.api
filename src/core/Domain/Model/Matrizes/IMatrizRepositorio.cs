﻿using System.Collections;
using System.Collections.Generic;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Matrizes;
using maisfidelidade.cnc.fo.api.core.Infrastructure;
using maisfidelidade.cnc.fo.api.core.Infrastructure.RepositoryFramework;

namespace maisfidelidade.cnc.fo.api.core.Domain
{
    public interface IMatrizRepositorio : IRepositorio<Matriz>
    {
        IList<Matriz> BuscarMatriz(int segmento);
    }
}