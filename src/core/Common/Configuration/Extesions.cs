﻿using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using maisfidelidade.cnc.fo.api.core.Log;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Reflection;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Configuration
{
	public static class Extensions
	{
		public static void SetEnvironment(this ILambdaContext context)
		{
            Logger.Log(context.InvokedFunctionArn);

            string functionArn = context != null && !string.IsNullOrWhiteSpace(context.InvokedFunctionArn) ? context.InvokedFunctionArn : "dev";
			if (functionArn.EndsWith("hml"))
				Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "hml");
			else if (functionArn.EndsWith("azl"))
				Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "azl");
			else if (functionArn.EndsWith("prd"))
				Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "prd");
			else
				Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "dev");

            Logger.Log("Ambiente: " + Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT"));
        }

		public static T To<T>(this object value, T defaultValue = default(T))
		{
			if (value == null) return defaultValue;
			try
			{
				if (typeof(T).IsEnum)
				{
					if (value is char || value is string)
					{
						string vbyte = value.ToString();
						if (value.ToString().Length == 1) vbyte = ((sbyte)char.Parse(value.ToString())).ToString();
						return (T)Enum.Parse(typeof(T), vbyte.ToString());
					}
					else
						return (T)Enum.Parse(typeof(T), value.ToString());
				}
				return (T)Convert.ChangeType(value, typeof(T));
			}
			catch { }
			return defaultValue;
		}

		public static APIGatewayProxyResponse CreateResponse(this object _object, HttpStatusCode _httpStatusCode = HttpStatusCode.OK)
		{
			return new APIGatewayProxyResponse()
			{
				StatusCode = (int)_httpStatusCode,
				Headers = new Dictionary<string, string> { { "Access-Control-Allow-Origin", "*" }, { "Access-Control-Allow-Headers", "*" } },
				Body = JsonConvert.SerializeObject(new { success = _httpStatusCode != HttpStatusCode.OK ? false : true, data = _object }, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, MissingMemberHandling = MissingMemberHandling.Ignore })
			};
		}

		public static string MySqlConnectionString(this DbSettings obj)
		{
			return string.Format("Server={0};Database={1};Uid={2};Pwd={3};Convert Zero Datetime=True;Allow Zero Datetime=true;", obj.Host, obj.Database, obj.Username, obj.Password);
		}

		public static string MsSqlServerConnectionString(this DbSettings obj)
		{
			return string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};Persist Security Info=False;",obj.Host, obj.Database, obj.Username, obj.Password);
		}
		public static string GetDetailedDescription(this Enum value)
		{
			FieldInfo fieldInfo = value.GetType().GetField(Enum.GetName(value.GetType(), value));
			DescriptionAttribute descriptionAttribute = fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false)[0] as DescriptionAttribute;
			return descriptionAttribute.Description;
		}

		public static string MongoConnectionString(this DbSettings obj)
		{
			if (!string.IsNullOrWhiteSpace(obj.Username) && !string.IsNullOrWhiteSpace(obj.Password))
				return string.Format("{0}://{1}:{2}@{3}{4}{5}/{6}{7}", obj.Protocol, obj.Username, obj.Password, obj.Host, string.IsNullOrWhiteSpace(obj.Port) ? string.Empty : ":", obj.Port, obj.Database, obj.Options);
			else
				return string.Format("{0}://{1}{2}{3}/{4}{5}", obj.Protocol, obj.Host, string.IsNullOrWhiteSpace(obj.Port) ? string.Empty : ":", obj.Port, obj.Database, obj.Options);
		}

		public static string DecodeBase64(this string val)
		{
			return Encoding.UTF8.GetString(Convert.FromBase64String(val));
		}

		public static string FormatStr(this string text, params object[] args)
		{
			return string.Format(text, args);
		}
	}
}
