using maisfidelidade.cnc.fo.api.core.Domain;
using maisfidelidade.cnc.fo.api.core.Infrastructure.Repositories;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Mensagens;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Usuarios;
using Microsoft.Extensions.DependencyInjection;
using System;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Empresas;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Indicadores.Performance;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Gerentes;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Banners;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Apuracoes;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Relatorios;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Classificacao;

namespace maisfidelidade.cnc.fo.api.core.Common.DependencyInjection
{
    public class DependencyResolver
    {
        private readonly IServiceProvider provedorServico;

        public DependencyResolver()
        {
            var serviceCollection = new ServiceCollection();

            AdicionarVinculos(serviceCollection);

            provedorServico = serviceCollection.BuildServiceProvider();
        }

        public T ObterServico<T>()
        {
            try
            {
                return provedorServico.GetService<T>();
            }
            catch  (Exception exception)
            {
                throw new Exception("", exception);
            }
        }

        public object ObterServico(Type servicoType)
        {
            try
            {
                return provedorServico.GetService(servicoType);
            }
            catch (Exception exception)
            {
                throw new Exception("", exception);
            }
        }
        
        private void AdicionarVinculos(IServiceCollection servicos)
        {
			servicos.AddTransient<IFAQRepositorio, FAQRepositorio>();
            servicos.AddTransient<IUsuarioRepositorio, UsuarioRepositorio>();
            servicos.AddTransient<IEmailTemplateRepositorio, EmailTemplateRepositorio>();
            servicos.AddTransient<IEmpresaRepositorio, EmpresaRepositorio>();
            servicos.AddTransient<ILogAcessoRepositorio, LogAcessoRepositorio>();
            servicos.AddTransient<ILogSenhaRepositorio, LogSenhaRepositorio>();
            servicos.AddTransient<IIndicadorPerformanceRepositorio, IndicadorPerformanceRepositorio>();
            servicos.AddTransient<IGerenteRepositorio, GerenteRepositorio>();
            servicos.AddTransient<INovidadeRepositorio, NovidadeRepositorio>();
            servicos.AddTransient<IBannerRepositorio, BannerRepositorio>();
            servicos.AddTransient<IConfiguracaoRepositorio, ConfiguracaoRepositorio>();
            servicos.AddTransient<IMatrizRepositorio, MatrizRepositorio>();
            servicos.AddTransient<IApuracaoRepositorio, ApuracaoRepositorio>();
            servicos.AddTransient<IAnaliticoRepositorio, AnaliticoRepositorio>();
            servicos.AddTransient<IApuracaoIndicadorClassificacaoRepositorio, ApuracaoIndicadorClassificacaoRepositorio>();
            servicos.AddTransient<IApuracaoBeneficioRentabilidadeRepositorio, ApuracaoBeneficioRentabilidadeRepositorio>();
            servicos.AddTransient<IApuracaoIndicadorRentabilidadeRepositorio, ApuracaoIndicadorRentabilidadeRepositorio>();
        }
    }
}
