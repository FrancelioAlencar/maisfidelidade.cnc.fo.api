﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Common.Utility
{
	public static class GetException
	{
		public static string GetExceptionDetails(this Exception exception)
		{
			var properties = exception.GetType()
									.GetProperties();
			var fields = new List<string>();
			foreach (PropertyInfo property in properties)
			{
				var value = property.GetValue(exception, null);
				fields.Add($"{property.Name} = {(value != null ? value.ToString() : String.Empty)}");
			}
			return String.Join("\n", fields.ToArray());
		}

	}
}