﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.DomainBase;
using maisfidelidade.cnc.fo.api.core.Infrastructure.Data;
using maisfidelidade.cnc.fo.api.core.Infrastructure.RepositoryFramework;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Infrastructure.Repositories
{
    public abstract class MySqlRepositorioBase<TAggregateRoot> : RepositorioBase<TAggregateRoot>
        where TAggregateRoot : IAggregateRoot
    {
        public delegate void AppendChildData(IList<TAggregateRoot> entityAggregate,
            object childEntityKeyValue);

        private readonly Database readDatabase;
        private readonly Database writeDatabase;
        private Dictionary<string, AppendChildData> childCallbacks;

        protected MySqlRepositorioBase()
        {
            this.readDatabase = DatabaseFactory.CreateDatabase("read");
            this.writeDatabase = DatabaseFactory.CreateDatabase("write");
            this.childCallbacks = new Dictionary<string, AppendChildData>();
            this.BuildChildCallbacks();
        }

        public MySqlConnection ObterNovaConexao()
        {
            var connection = new MySqlConnection //this.dbProviderFactory.CreateConnection();
            {
                ConnectionString = this.writeDatabase.ConnectionString
            };

            return connection;
        }
        
        #region Propriedades



        #endregion

        protected Dictionary<string, AppendChildData> ChildCallbacks
        {
            get { return this.childCallbacks; }
        }

        protected abstract void BuildChildCallbacks();
               
        #region Métodos Públicos

        public TAggregateRoot BuscarPor(int id)
        {
            StringBuilder builder = new StringBuilder();
            
            return this.BuildEntityFromSql(builder.ToString());
        }

        public IList<TAggregateRoot> BuscarTodos()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(";");

            return this.BuildEntitiesFromSql(builder.ToString());
        }



        #endregion

        #region Métodos Protegidos

        protected IList<TAggregateRoot> ExecuteQuery(string query)
        {
            return this.readDatabase.ExecuteQuery<TAggregateRoot>(CommandType.Text, query);
        }

        protected IList<TAggregateRoot> ExecuteQuery<TFirst, TSecond>(string query, Func<TFirst, TSecond, TAggregateRoot> map)
        {
            return this.readDatabase.ExecuteQuery<TFirst, TSecond, TAggregateRoot>(CommandType.Text, query, map);
        }

        protected IList<TAggregateRoot> ExecuteQuery<TFirst, TSecond, TThird>(string query, Func<TFirst, TSecond, TThird, TAggregateRoot> map)
        {
            return this.readDatabase.ExecuteQuery<TFirst, TSecond, TThird, TAggregateRoot>(CommandType.Text, query, map);
        }

        protected IList<TAggregateRoot> ExecuteQuery<TFirst, TSecond, TThird, TFourth>(string query, Func<TFirst, TSecond, TThird, TFourth, TAggregateRoot> map)
        {
            return this.readDatabase.ExecuteQuery<TFirst, TSecond, TThird, TFourth, TAggregateRoot>(CommandType.Text, query, map);
        }

        protected IList<TAggregateRoot> ExecuteQuery<TFirst, TSecond, TThird, TFourth, TFifth>(string query, Func<TFirst, TSecond, TThird, TFourth, TFifth, TAggregateRoot> map)
        {
            return this.readDatabase.ExecuteQuery<TFirst, TSecond, TThird, TFourth, TFifth, TAggregateRoot>(CommandType.Text, query, map);
        }

        protected IList<TEntity> ExecuteQuery<TEntity>(string query)
        {
            return this.readDatabase.ExecuteQuery<TEntity>(CommandType.Text, query);
        }

        protected IList<TEntity> ExecuteQuery<TFirst, TSecond, TEntity>(string query, Func<TFirst, TSecond, TEntity> map)
        {
            return this.readDatabase.ExecuteQuery<TFirst, TSecond, TEntity>(CommandType.Text, query, map);
        }

        protected IList<TEntity> ExecuteQuery<TFirst, TSecond, TThird, TEntity>(string query, Func<TFirst, TSecond, TThird, TEntity> map)
        {
            return this.readDatabase.ExecuteQuery<TFirst, TSecond, TThird, TEntity>(CommandType.Text, query, map);
        }

        protected IList<TEntity> ExecuteQuery<TFirst, TSecond, TThird, TFourth, TEntity>(string query, Func<TFirst, TSecond, TThird, TFourth, TEntity> map)
        {
            return this.readDatabase.ExecuteQuery<TFirst, TSecond, TThird, TFourth, TEntity>(CommandType.Text, query, map);
        }

        protected TAggregateRoot ExecuteQueryFirst(string query)
        {
            return this.readDatabase.ExecuteQueryFirst<TAggregateRoot>(CommandType.Text, query);
        }

        protected TEntity ExecuteQueryFirst<TEntity>(string query)
        {
            return this.readDatabase.ExecuteQueryFirst<TEntity>(CommandType.Text, query);
        }

        protected virtual void BuildEntity(TAggregateRoot entity)
        {
            if (this.childCallbacks != null && this.childCallbacks.Count > 0)
            {
                foreach (string childKeyName in this.childCallbacks.Keys)
                {
                    this.childCallbacks[childKeyName](new List<TAggregateRoot> { entity }, null);
                }
            }
        }

        protected virtual void BuildChildEntities(IList<TAggregateRoot> entities)
        {
            if (this.childCallbacks != null && this.childCallbacks.Count > 0)
            {
                foreach (string childKeyName in this.childCallbacks.Keys)
                {
                    this.childCallbacks[childKeyName](entities, null);
                }
            }
        }

        protected virtual TAggregateRoot BuildEntityFromSql(string sql)
        {
            var entity = this.ExecuteQueryFirst(sql);

            if (entity != null)
                BuildEntity(entity);

            return entity;
        }

        protected virtual IList<TAggregateRoot> BuildEntitiesFromSql(string sql)
        {
            var entities = this.ExecuteQuery(sql);

            if (entities.Count > 0)
                BuildChildEntities(entities);

            return entities;
        }

        #endregion
    }
}
