﻿using maisfidelidade.cnc.fo.api.core.Domain.Model.Apuracoes.Indicadores.Classificacao;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Relatorios;
using maisfidelidade.cnc.fo.api.core.Domain.Model.Usuarios;
using maisfidelidade.cnc.fo.api.core.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Infrastructure.Repositories
{
    public class AnaliticoRepositorio : MySqlRepositorioBase<Analitico>, IAnaliticoRepositorio
    {
        public AnaliticoRepositorio()
        {

        }

        public IList<Analitico> ObterComercial(int idUsuario, Filtro filtro, int idPerfil)
        {
            try
            {
                var query = new StringBuilder();

                if (idPerfil.Equals((int)PerfilEnum.GC))
                    query.Append("SELECT D.id_apuracao AS IdApuracao, F.id_gerente AS IdGerente, F.nome AS Gerente, C.id_segmento AS Segmento, C.id_empresa AS Id, C.cnpj AS CNPJ, C.nome_grupo AS Grupo, C.nome_fantasia AS NomeFantasia,");
                else if (idPerfil.Equals((int)PerfilEnum.GR))
                    query.Append("SELECT D.id_apuracao AS IdApuracao, C.id_segmento AS Segmento, C.id_empresa AS Id, C.cnpj AS CNPJ, C.nome_grupo AS Grupo, C.nome_fantasia AS NomeFantasia,");

                query.Append(" D.financiado_loja AS Producao, D.qtd_contratos AS QtdContratos, D.qtd_prestamista AS QtdPrestamistas, D.qtd_auto AS QtdAuto, qtd_convertidos AS QtdConvertidos, qtd_potencial AS QtdPotencial");

                if (idPerfil.Equals((int)PerfilEnum.Administrador))
                {
                    query.Append(" FROM tb_empresa AS C");

                    query.Append(" INNER JOIN tb_apuracao AS D ON D.id_empresa = C.id_empresa");
                    query.Append(" INNER JOIN tb_arquivo AS E ON E.id_arquivo = D.id_arquivo");
                    query.Append(" INNER JOIN tb_gerente AS F ON F.matricula = C.matricula_gc");

                    query.Append($" WHERE 1 = 1");
                }
                else
                {
                    query.Append(" FROM tb_usuario AS A");
                    query.Append(" INNER JOIN tb_gerente AS B ON B.email = A.email");

                    if (idPerfil.Equals((int)PerfilEnum.GC))
                        query.Append(" INNER JOIN tb_empresa AS C ON C.matricula_gc = B.matricula");
                    else if (idPerfil.Equals((int)PerfilEnum.GR))
                        query.Append(" INNER JOIN tb_empresa AS C ON C.matricula_gr = B.matricula");

                    query.Append(" INNER JOIN tb_apuracao AS D ON D.id_empresa = C.id_empresa");
                    query.Append(" INNER JOIN tb_arquivo AS E ON E.id_arquivo = D.id_arquivo");

                    if (idPerfil.Equals((int)PerfilEnum.GC))
                        query.Append(" INNER JOIN tb_gerente AS F ON F.matricula = C.matricula_gr");

                    query.Append($" WHERE A.id_usuario = {idUsuario}");
                }

                if (filtro.Data == null)
                    query.Append(" AND E.ultimo_processado = 1");
                else
                    query.Append($" AND E.dt_referencia = '{filtro.Data}')");

                if (filtro.Grupo != null)
                    query.Append($" AND C.nome_grupo = '{filtro.Grupo}'");

                if (filtro.IdGerente != 0)
                    query.Append($" AND F.id_gerente = {filtro.IdGerente}");

                if (filtro.IdSegmento != 0)
                    query.Append($" AND B.id_segmento = {filtro.IdSegmento}");

                if (filtro.CNPJ != null)
                    query.Append($" AND C.cnpj LIKE '%{filtro.CNPJ}%'");

                if (filtro.NomeFantasia != null)
                    query.Append($" AND C.nome_fantasia LIKE '%{filtro.NomeFantasia}%'");

                if (filtro.Producao != null)
                    query.Append($" AND D.financiado_loja LIKE '%{filtro.Producao}%'");

                if (filtro.QtdAuto != null)
                    query.Append($" AND D.qtd_auto LIKE '%{filtro.QtdAuto}%'");

                if (filtro.QtdContratos != null)
                    query.Append($" AND D.qtd_contratos LIKE '%{filtro.QtdContratos}%'");

                //if (filtro.QtdConvertidos != null)
                //    query.Append($" AND D.qtd_contratos LIKE '%{filtro.QtdConvertidos}%'");

                //if (filtro.QtdPotencial != null)
                //    query.Append($" AND D.qtd_contratos LIKE '%{filtro.QtdPotencial}%'");

                if (filtro.QtdPrestamistas != null)
                    query.Append($" AND D.qtd_prestamista LIKE '%{filtro.QtdPrestamistas}%'");

                query.Append(" ORDER BY C.nome_fantasia");
                query.Append(" LIMIT 10");
                query.Append($" OFFSET {filtro.Offset};");
                               
                Logger.Log($"AnaliticoRepositorio > ObterComercial Query MySql: {query.ToString()}");

                return this.BuildEntitiesFromSql(query.ToString());
            }
            catch (Exception error)
            {
                throw new Exception($"AnaliticoRepositorio > ObterComercial: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        public FiltroDetalhe ObterFiltros(int idPerfil)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("SELECT DISTINCT nome_grupo");
                query.Append(" FROM tb_empresa WHERE ativo = 1");

                Logger.Log($"AnaliticoRepositorio > ObterFiltros Query MySql: {query.ToString()}");

                return new FiltroDetalhe
                {
                    Grupo = this.ExecuteQuery<string>(query.ToString()),
                    Data = ObterFiltroDatas(),
                    Gerente = ObterFiltroGerente(idPerfil),
                    Segmento = ObterFiltroSegmento()
                };
            }
            catch (Exception error)
            {
                throw new Exception($"AnaliticoRepositorio > ObterFiltros: { error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        private IList<DateTime> ObterFiltroDatas()
        {
            try
            {
                var query = new StringBuilder();

                query.Append("SELECT DISTINCT dt_referencia");
                query.Append(" FROM tb_arquivo");

                Logger.Log($"AnaliticoRepositorio > ObterFiltroDatas Query MySql: {query.ToString()}");

                return this.ExecuteQuery<DateTime>(query.ToString());
            }
            catch (Exception error)
            {
                throw new Exception($"AnaliticoRepositorio > ObterFiltroDatas: { error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        private IList<FiltroGerente> ObterFiltroGerente(int idPerfil)
        {
            try
            {
                var query = new StringBuilder();

                var sigla = "";

                if (idPerfil.Equals((int)PerfilEnum.GC))
                    sigla = "GR";
                else if (idPerfil.Equals((int)PerfilEnum.Administrador))
                    sigla = "GC";

                if (sigla.Equals(""))
                    return null;

                query.Append("SELECT id_gerente AS Id, nome AS Nome");
                query.Append(" FROM tb_gerente");
                query.AppendFormat(" WHERE id_tipo_gerente = (SELECT id_tipo_gerente FROM tb_tipo_gerente WHERE sigla = '{0}') AND ativo = 1;", sigla);

                Logger.Log($"AnaliticoRepositorio > ObterFiltroGerente Query MySql: {query.ToString()}");

                return this.ExecuteQuery<FiltroGerente>(query.ToString());
            }
            catch (Exception error)
            {
                throw new Exception($"AnaliticoRepositorio > ObterFiltroGerente: { error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        private IList<FiltroSegmento> ObterFiltroSegmento()
        {
            try
            {
                var query = new StringBuilder();

                query.Append("SELECT DISTINCT id_segmento AS Id, nome AS Nome");
                query.Append(" FROM tb_segmento");
                query.Append(" WHERE ativo = 1;");

                Logger.Log($"AnaliticoRepositorio > ObterFiltroSegmento Query MySql: {query.ToString()}");

                return this.ExecuteQuery<FiltroSegmento>(query.ToString());
            }
            catch (Exception error)
            {
                throw new Exception($"AnaliticoRepositorio > ObterFiltroSegmento: { error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        public Paginacao ObterPaginacao(Filtro filtro, int idUsuario, int idPerfil)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("SELECT COUNT(*) AS TotalRegistros, CEIL(COUNT(*) / 10) AS QuantidadePaginas");
                if (idPerfil.Equals((int)PerfilEnum.Administrador))
                {
                    query.Append(" FROM tb_empresa AS C");

                    query.Append(" INNER JOIN tb_apuracao AS D ON D.id_empresa = C.id_empresa");
                    query.Append(" INNER JOIN tb_arquivo AS E ON E.id_arquivo = D.id_arquivo");
                    query.Append(" INNER JOIN tb_gerente AS F ON F.matricula = C.matricula_gc");

                    query.Append($" WHERE 1 = 1");
                }
                else
                {
                    query.Append(" FROM tb_usuario AS A");
                    query.Append(" INNER JOIN tb_gerente AS B ON B.email = A.email");

                    if (idPerfil.Equals((int)PerfilEnum.GC))
                        query.Append(" INNER JOIN tb_empresa AS C ON C.matricula_gc = B.matricula");
                    else if (idPerfil.Equals((int)PerfilEnum.GR))
                        query.Append(" INNER JOIN tb_empresa AS C ON C.matricula_gr = B.matricula");

                    query.Append(" INNER JOIN tb_apuracao AS D ON D.id_empresa = C.id_empresa");
                    query.Append(" INNER JOIN tb_arquivo AS E ON E.id_arquivo = D.id_arquivo");

                    if (idPerfil.Equals((int)PerfilEnum.GC))
                        query.Append(" INNER JOIN tb_gerente AS F ON F.matricula = C.matricula_gr");

                    query.Append($" WHERE A.id_usuario = {idUsuario}");
                }                

                if (filtro.Data == null)
                    query.Append(" AND E.ultimo_processado = 1");
                else
                    query.Append($" AND E.dt_referencia = '{filtro.Data}')");

                if (filtro.Grupo != null)
                    query.Append($" AND C.nome_grupo = '{filtro.Grupo}'");

                if (filtro.IdGerente != 0)
                    query.Append($" AND F.id_gerente = {filtro.IdGerente}");

                if (filtro.IdSegmento != 0)
                    query.Append($" AND B.id_segmento = {filtro.IdSegmento}");

                if (filtro.CNPJ != null)
                    query.Append($" AND C.cnpj LIKE '%{filtro.CNPJ}%'");

                if (filtro.NomeFantasia != null)
                    query.Append($" AND C.nome_fantasia LIKE '%{filtro.NomeFantasia}%'");

                if (filtro.Producao != null)
                    query.Append($" AND D.financiado_loja LIKE '%{filtro.Producao}%'");

                if (filtro.QtdAuto != null)
                    query.Append($" AND D.qtd_auto LIKE '%{filtro.QtdAuto}%'");

                if (filtro.QtdContratos != null)
                    query.Append($" AND D.qtd_contratos LIKE '%{filtro.QtdContratos}%'");

                //if (filtro.QtdConvertidos != null)
                //    query.Append($" AND D.qtd_contratos LIKE '%{filtro.QtdConvertidos}%'");

                //if (filtro.QtdPotencial != null)
                //    query.Append($" AND D.qtd_contratos LIKE '%{filtro.QtdPotencial}%'");

                if (filtro.QtdPrestamistas != null)
                    query.Append($" AND D.qtd_prestamista LIKE '%{filtro.QtdPrestamistas}%'");

                Logger.Log($"AnaliticoRepositorio > ObterPaginacao Query MySql: {query.ToString()}");

                return this.ExecuteQueryFirst<Paginacao>(query.ToString());
            }
            catch (Exception error)
            {
                throw new Exception($"AnaliticoRepositorio > ObterPaginacao: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        protected override void BuildChildCallbacks()
        {
            this.ChildCallbacks.Add("perfil",
                delegate (IList<Analitico> relatorio, object childKeyName)
                {
                    this.AppendIndicadoresClassificacao(relatorio);
                });
        }

        private void AppendIndicadoresClassificacao(IList<Analitico> relatorio)
        {
            try
            {
                if (!relatorio.Any())
                    return;

                foreach (Analitico item in relatorio)
                {
                    var query = new StringBuilder();
                    query.Append("SELECT id_indicador_classificacao AS Id, valor AS Valor");
                    query.Append(" FROM tb_apuracao_indicador_classificacao");
                    query.AppendFormat(" WHERE id_apuracao = {0}", item.IdApuracao);

                    Logger.Log($"AnaliticoRepositorio > AppendIndicadoresClassificacao Query MySql: {query.ToString()}");

                    var indicadorClassificacao = this.ExecuteQuery<IndicadorClassificacao>(query.ToString());

                    if (indicadorClassificacao.Any())
                    {
                        item.MarketShare = indicadorClassificacao.FirstOrDefault(x => x.Id.Equals((int)IndicadorClassificacaoEnum.MarketShare))?.Valor;
                        item.IPS = indicadorClassificacao.FirstOrDefault(x => x.Id.Equals((int)IndicadorClassificacaoEnum.IPS))?.Valor;
                    }
                }
            }
            catch (Exception error)
            {
                throw new Exception($"AnaliticoRepositorio > AppendIndicadoresClassificacao: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        protected override void PersistDeletedItem(Analitico item)
        {
            throw new NotImplementedException();
        }

        protected override void PersistNewItem(Analitico item)
        {
            throw new NotImplementedException();
        }

        protected override void PersistUpdatedItem(Analitico item)
        {
            throw new NotImplementedException();
        }

        public IList<Analitico> ObterComercialExportacao(int idUsuario, Filtro filtro, int idPerfil)
        {
            try
            {
                var query = new StringBuilder();

                if (idPerfil.Equals((int)PerfilEnum.GC))
                    query.Append("SELECT D.id_apuracao AS IdApuracao, F.id_gerente AS IdGerente, F.nome AS Gerente, C.id_segmento AS Segmento, C.id_empresa AS Id, C.cnpj AS CNPJ, C.nome_grupo AS Grupo, C.nome_fantasia AS NomeFantasia,");
                else if (idPerfil.Equals((int)PerfilEnum.GR))
                    query.Append("SELECT D.id_apuracao AS IdApuracao, C.id_segmento AS Segmento, C.id_empresa AS Id, C.cnpj AS CNPJ, C.nome_grupo AS Grupo, C.nome_fantasia AS NomeFantasia,");

                query.Append(" D.financiado_loja AS Producao, D.qtd_contratos AS QtdContratos, D.qtd_prestamista AS QtdPrestamistas, D.qtd_auto AS QtdAuto, qtd_convertidos AS QtdConvertidos, qtd_potencial AS QtdPotencial");

                if (idPerfil.Equals((int)PerfilEnum.Administrador))
                {
                    query.Append(" FROM tb_empresa AS C");

                    query.Append(" INNER JOIN tb_apuracao AS D ON D.id_empresa = C.id_empresa");
                    query.Append(" INNER JOIN tb_arquivo AS E ON E.id_arquivo = D.id_arquivo");
                    query.Append(" INNER JOIN tb_gerente AS F ON F.matricula = C.matricula_gc");

                    query.Append($" WHERE 1 = 1");
                }
                else
                {
                    query.Append(" FROM tb_usuario AS A");
                    query.Append(" INNER JOIN tb_gerente AS B ON B.email = A.email");

                    if (idPerfil.Equals((int)PerfilEnum.GC))
                        query.Append(" INNER JOIN tb_empresa AS C ON C.matricula_gc = B.matricula");
                    else if (idPerfil.Equals((int)PerfilEnum.GR))
                        query.Append(" INNER JOIN tb_empresa AS C ON C.matricula_gr = B.matricula");

                    query.Append(" INNER JOIN tb_apuracao AS D ON D.id_empresa = C.id_empresa");
                    query.Append(" INNER JOIN tb_arquivo AS E ON E.id_arquivo = D.id_arquivo");

                    if (idPerfil.Equals((int)PerfilEnum.GC))
                        query.Append(" INNER JOIN tb_gerente AS F ON F.matricula = C.matricula_gr");

                    query.Append($" WHERE A.id_usuario = {idUsuario}");
                }

                if (filtro.Data == null)
                    query.Append(" AND E.ultimo_processado = 1");
                else
                    query.Append($" AND E.dt_referencia = '{filtro.Data}')");

                if (filtro.Grupo != null)
                    query.Append($" AND C.nome_grupo = '{filtro.Grupo}'");

                if (filtro.IdGerente != 0)
                    query.Append($" AND F.id_gerente = {filtro.IdGerente}");

                if (filtro.IdSegmento != 0)
                    query.Append($" AND B.id_segmento = {filtro.IdSegmento}");

                if (filtro.CNPJ != null)
                    query.Append($" AND C.cnpj LIKE '%{filtro.CNPJ}%'");

                if (filtro.NomeFantasia != null)
                    query.Append($" AND C.nome_fantasia LIKE '%{filtro.NomeFantasia}%'");

                if (filtro.Producao != null)
                    query.Append($" AND D.financiado_loja LIKE '%{filtro.Producao}%'");

                if (filtro.QtdAuto != null)
                    query.Append($" AND D.qtd_auto LIKE '%{filtro.QtdAuto}%'");

                if (filtro.QtdContratos != null)
                    query.Append($" AND D.qtd_contratos LIKE '%{filtro.QtdContratos}%'");

                //if (filtro.QtdConvertidos != null)
                //    query.Append($" AND D.qtd_contratos LIKE '%{filtro.QtdConvertidos}%'");

                //if (filtro.QtdPotencial != null)
                //    query.Append($" AND D.qtd_contratos LIKE '%{filtro.QtdPotencial}%'");

                if (filtro.QtdPrestamistas != null)
                    query.Append($" AND D.qtd_prestamista LIKE '%{filtro.QtdPrestamistas}%'");

                query.Append(" ORDER BY C.nome_fantasia;");

                Logger.Log($"AnaliticoRepositorio > ObterComercialExportacao Query MySql: {query.ToString()}");

                return this.BuildEntitiesFromSql(query.ToString());
            }
            catch (Exception error)
            {
                throw new Exception($"AnaliticoRepositorio > ObterComercialExportacao: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        public IList<Analitico> ObterLojista(int idEmpresa, Filtro filtro)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("SELECT C.id_apuracao AS IdApuracao, B.id_segmento AS Segmento, B.id_empresa AS Id, B.cnpj AS CNPJ, B.nome_grupo AS Grupo, B.nome_fantasia AS NomeFantasia,");
                query.Append(" C.financiado_loja AS Producao, C.qtd_contratos AS QtdContratos, C.qtd_prestamista AS QtdPrestamistas, C.qtd_auto AS QtdAuto, qtd_convertidos AS QtdConvertidos, qtd_potencial AS QtdPotencial");
                query.Append(" FROM tb_empresa AS A");
                query.Append(" LEFT JOIN tb_empresa AS B ON B.nome_grupo = A.nome_grupo");
                query.Append(" INNER JOIN tb_apuracao AS C ON C.id_empresa = B.id_empresa");
                query.Append(" INNER JOIN tb_arquivo AS D ON D.id_arquivo = C.id_arquivo");
                query.Append($" WHERE A.id_empresa = {idEmpresa}");

                if (filtro.Data == null)
                    query.Append(" AND D.ultimo_processado = 1");
                else
                    query.Append($" AND D.dt_referencia = '{filtro.Data}')");

                if (filtro.IdSegmento != 0)
                    query.Append($" AND B.id_segmento = {filtro.IdSegmento}");

                if (filtro.CNPJ != null)
                    query.Append($" AND B.cnpj LIKE '%{filtro.CNPJ}%'");

                if (filtro.NomeFantasia != null)
                    query.Append($" AND B.nome_fantasia LIKE '%{filtro.NomeFantasia}%'");

                if (filtro.Producao != null)
                    query.Append($" AND C.financiado_loja LIKE '%{filtro.Producao}%'");

                if (filtro.QtdAuto != null)
                    query.Append($" AND C.qtd_auto LIKE '%{filtro.QtdAuto}%'");

                if (filtro.QtdContratos != null)
                    query.Append($" AND C.qtd_contratos LIKE '%{filtro.QtdContratos}%'");

                //if (filtro.QtdConvertidos != null)
                //    query.Append($" AND C.qtd_contratos LIKE '%{filtro.QtdConvertidos}%'");

                //if (filtro.QtdPotencial != null)
                //    query.Append($" AND C.qtd_contratos LIKE '%{filtro.QtdPotencial}%'");

                if (filtro.QtdPrestamistas != null)
                    query.Append($" AND C.qtd_prestamista LIKE '%{filtro.QtdPrestamistas}%'");

                query.Append(" ORDER BY B.nome_fantasia");
                query.Append(" LIMIT 10");
                query.Append($" OFFSET {filtro.Offset};");

                Logger.Log($"AnaliticoRepositorio > ObterLojista Query MySql: {query.ToString()}");

                return this.BuildEntitiesFromSql(query.ToString());
            }
            catch (Exception error)
            {
                throw new Exception($"AnaliticoRepositorio > ObterLojista: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        public IList<Analitico> ObterLojistaExportacao(int idEmpresa, Filtro filtro)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("SELECT C.id_apuracao AS IdApuracao, B.id_segmento AS Segmento, B.id_empresa AS Id, B.cnpj AS CNPJ, B.nome_grupo AS Grupo, B.nome_fantasia AS NomeFantasia,");
                query.Append(" C.financiado_loja AS Producao, C.qtd_contratos AS QtdContratos, C.qtd_prestamista AS QtdPrestamistas, C.qtd_auto AS QtdAuto, qtd_convertidos AS QtdConvertidos, qtd_potencial AS QtdPotencial");
                query.Append(" FROM tb_empresa AS A");
                query.Append(" LEFT JOIN tb_empresa AS B ON B.nome_grupo = A.nome_grupo");
                query.Append(" INNER JOIN tb_apuracao AS C ON C.id_empresa = B.id_empresa");
                query.Append(" INNER JOIN tb_arquivo AS D ON D.id_arquivo = C.id_arquivo");
                query.Append($" WHERE A.id_empresa = {idEmpresa}");

                if (filtro.Data == null)
                    query.Append(" AND D.ultimo_processado = 1");
                else
                    query.Append($" AND D.dt_referencia = '{filtro.Data}')");

                if (filtro.IdSegmento != 0)
                    query.Append($" AND B.id_segmento = {filtro.IdSegmento}");

                if (filtro.CNPJ != null)
                    query.Append($" AND B.cnpj LIKE '%{filtro.CNPJ}%'");

                if (filtro.NomeFantasia != null)
                    query.Append($" AND B.nome_fantasia LIKE '%{filtro.NomeFantasia}%'");

                if (filtro.Producao != null)
                    query.Append($" AND C.financiado_loja LIKE '%{filtro.Producao}%'");

                if (filtro.QtdAuto != null)
                    query.Append($" AND C.qtd_auto LIKE '%{filtro.QtdAuto}%'");

                if (filtro.QtdContratos != null)
                    query.Append($" AND C.qtd_contratos LIKE '%{filtro.QtdContratos}%'");

                //if (filtro.QtdConvertidos != null)
                //    query.Append($" AND C.qtd_contratos LIKE '%{filtro.QtdConvertidos}%'");

                //if (filtro.QtdPotencial != null)
                //    query.Append($" AND C.qtd_contratos LIKE '%{filtro.QtdPotencial}%'");

                if (filtro.QtdPrestamistas != null)
                    query.Append($" AND C.qtd_prestamista LIKE '%{filtro.QtdPrestamistas}%'");

                query.Append(" ORDER BY B.nome_fantasia;");

                Logger.Log($"AnaliticoRepositorio > ObterLojistaExportacao Query MySql: {query.ToString()}");

                return this.BuildEntitiesFromSql(query.ToString());
            }
            catch (Exception error)
            {
                throw new Exception($"AnaliticoRepositorio > ObterLojistaExportacao: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }

        public Paginacao ObterPaginacaoLojista(Filtro filtro, int idEmpresa)
        {
            try
            {
                var query = new StringBuilder();

                query.Append("SELECT COUNT(*) AS TotalRegistros, CEIL(COUNT(*) / 10) AS QuantidadePaginas");
                query.Append(" FROM tb_empresa AS A");
                query.Append(" LEFT JOIN tb_empresa AS B ON B.nome_grupo = A.nome_grupo");
                query.Append(" INNER JOIN tb_apuracao AS C ON C.id_empresa = B.id_empresa");
                query.Append(" INNER JOIN tb_arquivo AS D ON D.id_arquivo = C.id_arquivo");
                query.Append($" WHERE A.id_empresa = {idEmpresa}");

                if (filtro.Data == null)
                    query.Append(" AND D.ultimo_processado = 1");
                else
                    query.Append($" AND D.dt_referencia = '{filtro.Data}')");

                if (filtro.IdSegmento != 0)
                    query.Append($" AND B.id_segmento = {filtro.IdSegmento}");

                if (filtro.CNPJ != null)
                    query.Append($" AND B.cnpj LIKE '%{filtro.CNPJ}%'");

                if (filtro.NomeFantasia != null)
                    query.Append($" AND B.nome_fantasia LIKE '%{filtro.NomeFantasia}%'");

                if (filtro.Producao != null)
                    query.Append($" AND C.financiado_loja LIKE '%{filtro.Producao}%'");

                if (filtro.QtdAuto != null)
                    query.Append($" AND C.qtd_auto LIKE '%{filtro.QtdAuto}%'");

                if (filtro.QtdContratos != null)
                    query.Append($" AND C.qtd_contratos LIKE '%{filtro.QtdContratos}%'");

                //if (filtro.QtdConvertidos != null)
                //    query.Append($" AND C.qtd_contratos LIKE '%{filtro.QtdConvertidos}%'");

                //if (filtro.QtdPotencial != null)
                //    query.Append($" AND C.qtd_contratos LIKE '%{filtro.QtdPotencial}%'");

                if (filtro.QtdPrestamistas != null)
                    query.Append($" AND C.qtd_prestamista LIKE '%{filtro.QtdPrestamistas}%'");

                Logger.Log($"AnaliticoRepositorio > ObterPaginacaoLojista Query MySql: {query.ToString()}");

                return this.ExecuteQueryFirst<Paginacao>(query.ToString());
            }
            catch (Exception error)
            {
                throw new Exception($"AnaliticoRepositorio > ObterPaginacaoLojista: {error.Message}\n\nStack Trace: {error.StackTrace}", error);
            }
        }
    }
}
