﻿using maisfidelidade.cnc.fo.api.core.Infrastructure.RepositoryFramework;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.cnc.fo.api.core.Domain.Model.Relatorios
{
    public interface IAnaliticoRepositorio : IRepositorio<Analitico>
    {
        Paginacao ObterPaginacao(Filtro filtro, int idUsuario, int idPerfil);
        Paginacao ObterPaginacaoLojista(Filtro filtro, int idEmpresa);
        IList<Analitico> ObterComercial(int idUsuario, Filtro filtro, int idPerfil);
        IList<Analitico> ObterLojista(int idEmpresa, Filtro filtro);
        IList<Analitico> ObterComercialExportacao(int idUsuario, Filtro filtro, int idPerfil);
        IList<Analitico> ObterLojistaExportacao(int idEmpresa, Filtro filtro);
        FiltroDetalhe ObterFiltros(int idPerfil);
    }
}
